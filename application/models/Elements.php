<?php 
class Elements extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function galeria(){
		$this->db->order_by('orden','ASC');		
		$galeria = $this->db->get('galeria');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/galeria/'.$g->foto);
			if(strpos($g->titulo,'youtube')>-1 || strpos($g->titulo,'vimeo')>-1){
				$galeria->row($n)->enlace = $galeria->row($n)->titulo;
				$galeria->row($n)->titulo = 'Video de youtube';
			}else{
				$galeria->row($n)->enlace = $galeria->row($n)->foto;
			}
		}
		return $galeria;
	}

	function blog(){
		$this->db->order_by('fecha','DESC');		
		$galeria = $this->db->get('blog');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/blog/'.$g->foto);
			$galeria->row($n)->link = base_url('blog/'.toUrl($g->id.'-'.$g->titulo));
			$galeria->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$g->id))->num_rows();
			$galeria->row($n)->texto = cortar_palabras(strip_tags($g->texto),70).'...';
		}
		return $galeria;
	}

	function estados(){	
		$galeria = $this->db->get('estados');	
		return $galeria;
	}
}