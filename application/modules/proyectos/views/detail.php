<?php 
$page = $this->load->view('proyecto-detalle',array(),TRUE,'paginas');

$tags = '';
foreach(explode(',',$detail->tags) as $t){
$tags.='<div class="col-xs-6 col-sm-2">
                  <button type="button" class="btn btn-default btn-colo-cu general-hover"><span>'.$t.'</span></button>
               </div>';
}
$detail->tags = $tags;
$detail->banner = base_url('img/proyectos/'.$detail->banner);
$detail->estado = $this->db->get_where('estados',array('id'=>$detail->estados_id))->row()->nombre;
foreach($detail as $n=>$d){
	$page = str_replace('['.$n.']',$d,$page);
}

$this->db->select('universidades.*');
$this->db->join('universidades','universidades.id = proyectos_universidades.universidades_id');
$avalada = $this->db->get_where('proyectos_universidades',array('proyectos_id'=>$detail->id));
foreach($avalada->result() as $n=>$v){
	$avalada->row($n)->logo = base_url('img/avaladores/'.$v->logo);
}


$this->db->select('proyectos_vacantes_detalles.*, proyectos_vacantes.estancia_minima, proyectos_vacantes.desde, proyectos_vacantes.hasta, carreras.nombre as categoria');
$this->db->join('proyectos_vacantes','proyectos_vacantes.id = proyectos_vacantes_detalles.proyectos_vacantes_id');
$this->db->join('carreras','carreras.id = proyectos_vacantes_detalles.carreras_id','left');
$vacantes = $this->db->get_where('proyectos_vacantes_detalles',array('proyectos_id'=>$detail->id));


$page = str_replace('[vacantes]',$this->load->view('vacantes',array('vacantes'=>$vacantes),TRUE),$page);
if($vacantes->num_rows()>0){
$vacantetab = '';
}else{
	$vacantetab = 'style="display:none"';
}
$page = str_replace('[tabvacantes]',$vacantetab,$page);

$testimonios = $this->db->get_where('proyectos_testimonios',array('proyectos_id'=>$detail->id));
foreach($testimonios->result() as $n=>$v){
	$testimonios->row($n)->imagen = base_url('img/testimonios_crud/'.$v->imagen);
}

$cruds = array(
	'avalada'=>$avalada,
	'testimonios'=>$testimonios
);
$page = $this->querys->fillFields($page,$cruds);
$this->load->view('read',array('page'=>$page),FALSE,'paginas');
?>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBiTQp5QUWTeD0XQpNnv3kevaKrtokkpnA"></script>
<script>
	google.maps.event.addDomListener(window, 'load', init);
	function init(){
		var mapa = document.getElementById('mapaubicacion');
		var mapOptions = {
            zoom: 8,
            center: new google.maps.LatLng<?= empty($detail->lat)?$detail->ubicacion_mapa:'('.$detail->lat.','.$detail->lon.')' ?>,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };        
        var map = new google.maps.Map(mapa,mapOptions);                            
        var marker = new google.maps.Marker({
	        position: new google.maps.LatLng<?= empty($detail->lat)?$detail->ubicacion_mapa:'('.$detail->lat.','.$detail->lon.')' ?>,
	        map: map,
	        icon:'<?= base_url() ?>img/marker-map.png',
	        title: '<?= $detail->nombre_ubicacion ?>'
    	});
	}
</script>