<?php foreach($this->db->get_where('areas_laborales')->result() as $c): ?>
<!-- Conteiner1 C.sociales-->
 <section id="marzen<?= $c->id ?>" class="tab-panel col-xs-12 col-sm-12 padding-30-new">

    <div class="col-xs-12 col-sm-12">
       <h2 class="title-menu-2 text-gray"><?= $c->nombre ?></h2>
       <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
    </div>


    <?php foreach($this->db->get_where('departamentos',array('areas_laborales_id'=>$c->id))->result() as $cc): ?>
	    <div class="col-xs-12 col-sm-12 padding-row-new">
	       <div class="col-xs-3 col-sm-1">
	          <img src="<?= base_url('img/areas_laborales/'.$cc->imagen) ?>" alt="Video Yo nomada" class="img-responsive center-block img-circle img-new-tab2">
	       </div>
	       <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
	          <?= $cc->nombre ?>
	       </div>
	    </div>

	    <div class="col-xs-12 col-sm-12 padding-40-tab">
	    	<?php foreach($this->db->get_where('carreras',array('departamentos_id'=>$cc->id))->result() as $a): ?>
		       <div class="col-xs-6 col-sm-1 padding0 new-padding-responsive">
		           <a href="<?= base_url('proyectos') ?>?carreras_id=<?= $a->id ?>" title="Ver proyectos con esta área"> <p class="text-area-labo"><?= $a->nombre ?></p></a>
		       </div>
		       <div class="col-xs-0 col-sm-1"></div>
	   		<?php endforeach ?>
	    </div>
	    <div class="col-xs-0 col-sm-1"></div>
    <?php endforeach ?>
 </section>

<?php endforeach ?>
