<?php
	
	foreach($vacantes->result() as $v): ?>
	<div class="col-sm-4 padding-tarj-new-all">
	  <div class="flip-container manual-flip">
	     <div class="sports-card">
	        <div class="front col-xs-12 col-sm-12" style="padding: 0px;">
	           <div class="cover">
	              <div class="cover2"></div>
	              <div class="col-xs-12 col-sm-12 content-tex-tittle">
	                 <h3 class="text-center h3responsive"><?= $v->titulo ?></h3>
	                 <p class="text-gray-general text-center font-weight-bold h3responsive">Estancia mínima: <?= $v->estancia_minima ?> meses</p>
	                 <div class="col-xs-6 col-sm-6" style="padding: 0;">
	                    <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Desde: <?= date("d-m-y",strtotime($v->desde)) ?></p>
	                 </div>
	                 <div class="col-xs-6 col-sm-6" style="padding: 0;">
	                    <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Hasta: <?= date("d-m-y",strtotime($v->hasta)) ?></p>
	                 </div>
	                 <p class="text-gray-general text-center" id="responsive-ciencias-title1"><?= $v->categoria ?> </p>
	              </div>
	           </div>
	           <div class="content col-sm-12 scrollbar force-overflow" id="style-3" style="padding: 8px 0px 10px 20px;">

	              <?php
	              	$this->db->select('departamentos.*, proyectos_vacantes_detalles.puestos');
	              	$this->db->join('departamentos','departamentos.id = proyectos_vacantes_detalles.departamentos_id');
	              	foreach($this->db->get_where('proyectos_vacantes_detalles',array('proyectos_vacantes_id'=>$v->id))->result() as $a):
	              ?>
		              <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
		                 <div class="col-xs-2 col-sm-2" style="padding: 0px;">
		                    <img src="<?= base_url('img/areas_laborales/'.$a->imagen) ?>" alt="<?= $a->nombre ?>" class="img-responsive centerblock img-circle img-new-tarj-cirl">
		                 </div>
		                 <div class="col-xs-10 col-sm-10">
		                    <p class="margin-title-tarj font-weight-bold text-gray-general"><?= $a->nombre ?></p>
		                    <?php foreach(explode(',',$a->puestos) as $puesto): ?>
		                    <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·<?= $puesto ?>
		                    </p>
		                	<?php endforeach ?>
		                 </div>
		              </div>
	              <?php endforeach ?>
	           </div>
	           <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
	              <button class="sports-button boton-colo-black margin-buttom-ver-mas2 icon-blue-tarj">
	              <strong>VER MÁS</strong>
	              </button>
	           </div>
	           <div class="col-xs-6 col-sm-6 boton-colo-black2">
					<button type="button" data-toggle="modal" data-target="#myModal" class="quiero sports-button margin-buttom-ver-mas color-green-tarj">
	              		<strong>QUIERO INFORMES</strong>
              		</button>
	           </div>
	        </div>
	        <div class="back col-xs-12 col-sm-12" style="padding: 0px;" id="size-card-new">
	           <div class="cover2"></div>
	           <div class="col-xs-12 col-sm-12 point-img-cover col-max-min-height-tar col-sm-12 force-overflow shadow-new-all" id="style-3">
	              <?= $v->texto ?>
	           </div>
	           <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
	              <button class="sports-button boton-colo-black margin-buttom-back">
	              <strong><i class="fa fa-reply icon-blue-tarj fa-lg" aria-hidden="true"></i></strong>
	              </button>
	           </div>
	           <div class="col-xs-6 col-sm-6 boton-colo-black2">
	              <button data-toggle="modal" data-target="#myModal" class="quiero sports-button margin-buttom-ver-mas color-green-tarj">
	              	<strong>QUIERO INFORMES</strong>
	              </button>
	           </div>
	        </div>
	     </div>
	  </div>
	</div>
<?php endforeach ?>
