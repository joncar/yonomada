<?php
$page = $this->load->view('areas_laborales',array(),TRUE,'paginas');
$categorias = $this->db->get_where('areas_laborales');
foreach($categorias->result() as $n=>$c){
	$categorias->row($n)->imagen = base_url('img/areas_laborales/'.$c->imagen);
	$categorias->row($n)->checked = $n==0?'checked':'';
	$categorias->row($n)->tipocuadro = $n==3?'padding-0-new2':'padding-0-new';
}
$page = $this->querys->fillFields($page,array('categorias'=>$categorias));
$page = str_replace('[categoriascontent]',$this->load->view('_categorias_areas',array(),TRUE),$page);
$this->load->view('read',array('page'=>$page),false,'paginas');