<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Datos Básicos</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Vacantes</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Testimonios</a></li>
    <li role="presentation"><a href="#solicitudes" aria-controls="messages" role="tab" data-toggle="tab">Solicitudes</a></li>    
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
    	<?= $output->output ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
    	<?= $output->vacantes->output ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="messages">
    	<?= $output->testimonios->output ?>    	
    </div> 
    <div role="tabpanel" class="tab-pane" id="solicitudes">
      <?= $output->solicitudes->output ?>     
    </div>      
  </div>

</div>