<?php
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/jquery.noty.js');
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js');

if (!$this->is_IE7()) {
    $this->set_js_lib('assets/grocery_crud/js/common/list.js');
}
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/cookies.js');
$this->set_js('assets/grocery_crud/themes/bootstrap3/js/flexigrid.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.form.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.printElement.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/pagination.js');
/** Fancybox */
$this->set_css('assets/grocery_crud/css/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js');

/** Jquery UI */
$this->load_js_jqueryui();
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>


<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>
<div class="row flexigrid">
    <div class="col-xs-12 col-sm-12" style="padding-bottom: 11px;">
       <div class="col-xs-12 col-sm-3">
          <p class="spanfiltros" id="hide-btn">OCULTAR FILTROS</p>
       </div>
       <div class="col-xs-12 col-sm-9">
          <!--<div class="col-xs-0 col-sm-5" style="padding-left: 0;">
             <div class="col-xs-12 col-sm-5" style="padding-left: 0;">
                <p class="text-black-title-new2">ORDENAR POR:</p>
             </div>
             <div class="col-xs-12 col-sm-7">
                <div class="custom-select" style="width:100%;top: -9px;">
                   <select>
                      <option value="0">Opción 1</option>
                      <option value="1">Opción 2</option>
                      <option value="2">Opción 3</option>
                      <option value="3">Opción 4</option>
                      <option value="4">Opción 5</option>
                      <option value="5">Opción 6</option>
                   </select>
                </div>
             </div>
          </div>
          <div class="col-xs-12 col-sm-3"></div>

          <div class="col-xs-12 col-sm-3" id="col-responsive-search">
             <div id="wrap">
                   <input type="hidden" name="search_field[]" value="titulo">
                   <input id="search" name="search_text[]" type="text" placeholder="" style="width: 100%;position: absolute;color: #5f5f5f;" ><input id="search_submit2" value="Rechercher" type="submit" class="img-pruebadd" >
             </div>
          </div>-->

       </div>
    </div>
    <div class="col-xs-12 col-sm-12">
       <div class="col-xs-12 col-sm-3" id="mielemento">
          <div class="col-xs-12 col-sm-12 contenerdo-white-1 shadow-box">
             <div class="col-xs-12 col-sm-12">
                <h2 class="title-menu-2 text-gray">FILTROS</h2>
                <div class="col-sm-6  col-xs-4 line-colo-cyan"></div>
             </div>
             <div class="col-xs-12 col-sm-12">
                <p class="text-black-filtros">Área laboral</p>
                <div class="custom-select" style="width:100%;">
                  <input type="hidden" name="search_field[]" value="view_proyectos_front.areas_laborales_id">
                  <select name="search_text[]">
                    <option value="">Selecciona una opcion</option>
                    <option value="">Ver todas</option>
                  <?php foreach(get_instance()->db->get('areas_laborales')->result() as $a): ?>
                    <option value="<?= $a->id ?>" <?= @$_GET['areas_laborales_id']==$a->id?'selected':'' ?>><?= $a->nombre ?></option>
                  <?php endforeach ?>
                  </select>
                </div>
                <p class="text-black-filtros">Departamento</p>
                <div class="custom-select" style="width:100%;">
                   <input type="hidden" name="search_field[]" value="view_proyectos_front.departamentos_id">
                   <select name="search_text[]">
                      <option value="">Selecciona una opcion</option>
                      <option value="">Ver todas</option>
                      <?php foreach(get_instance()->db->get('departamentos')->result() as $a): ?>
                        <option value="<?= $a->id ?>" <?= @$_GET['departamentos_id']==$a->id?'selected':'' ?>><?= $a->nombre ?></option>
                      <?php endforeach ?>
                   </select>
                </div>
                <p class="text-black-filtros">Carrera</p>
                <div class="custom-select" style="width:100%;">
                   <input type="hidden" name="search_field[]" value="view_proyectos_front.carreras_id">
                   <select name="search_text[]">
                      <option value="">Selecciona una opcion</option>
                      <option value="">Ver todas</option>
                      <?php foreach(get_instance()->db->get('carreras')->result() as $a): ?>
                        <option value="<?= $a->id ?>"<?= @$_GET['carreras_id']==$a->id?'selected':'' ?>><?= $a->nombre ?></option>
                      <?php endforeach ?>
                   </select>
                </div>
                <p class="text-black-filtros">Universidad</p>
                <div class="custom-select" style="width:100%;">
                   <input type="hidden" name="search_field[]" value="view_proyectos_front.universidades_id">
                   <select name="search_text[]">
                      <option value="">Selecciona una opcion</option>
                      <option value="">Ver todas</option>
                      <?php foreach(get_instance()->db->get('universidades')->result() as $a): ?>
                        <option value="<?= $a->id ?>"<?= @$_GET['universidades_id']==$a->id?'selected':'' ?>><?= $a->nombre ?></option>
                      <?php endforeach ?>
                   </select>
                </div>
                <p class="text-black-filtros">Semestre mínimo</p>
                <div class="custom-select" style="width:100%;">
                   <input type="hidden" name="search_field[]" value="view_proyectos_front.semestres_id">
                   <select name="search_text[]">
                      <option value="">Selecciona una opcion</option>
                      <option value="">Ver todas</option>
                      <?php foreach(get_instance()->db->get('semestres')->result() as $a): ?>
                        <option value="<?= $a->id ?>"<?= @$_GET['semestres_id']==$a->id?'selected':'' ?>><?= $a->nombre ?></option>
                      <?php endforeach ?>
                   </select>
                </div>
                <p class="text-black-filtros">Estancia mínima</p>
                <p class="text-black-filtros tex-range-dias">(Días)</p>
                <div class="rangeslider-wrap">
                  <input type="range" name="estancia_minima" min="1" max="100" labels="" value="1">                  
                </div>
                <input type="hidden" name="search_field[]" value="titulo">
                <input type="hidden" name="search_text[]" value="<?= !empty($_GET['titulo'])?$_GET['titulo']:'' ?>">

                <input type="hidden" name="search_field[]" value="jea8e41f2.id">
                <input type="hidden" name="search_text[]" value="<?= !empty($_GET['estados_id'])?$_GET['estados_id']:'' ?>">

                <button type="submit" class="text-black-filtros col-cleal-filtros btn-filtros-izquierda">
                    Filtrar
                </button>
                <a href="<?= base_url('proyectos') ?>" class="text-black-filtros col-cleal-filtros btn-filtros-izquierda" style="text-align: center">
                    Limpiar filtros
                </a>
             </div>
          </div>
       </div>
       <div class="col-xs-12 col-sm-9">
          <div class="col-xs-12 col-sm-12 contenerdo-white-2 shadow-box ajax_list">
             <?= $list_view ?>
          </div>
          <div class="col-xs-12 contendor-paginador">
                <nav aria-label="Page navigation" id="div1">
                      <ul class="pagination pager">                        
                      </ul>
                </nav>
          </div>
       </div>
    </div>
        <button style="display: none" title='Refrescar listado'  type="button" class="ajax_refresh_and_loading dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold" tabindex="" aria-controls="dynamic-table" data-original-title="" title="">
            <span>
                <i class="ace-icon fa fa-refresh bigger-110 grey"></i>
            </span>
        </button>
    </div>



<input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
<input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>
<?php echo form_close() ?>
