<div class="col-xs-12 col-sm-12">
    <div class="col-xs-12 col-sm-12">
       <h2 class="title-menu-tarj-2 text-gray">¡Explorar los proyectos!</h2>
    </div>

    <?php foreach ($list as $num_row =>$row): ?>

            <div class="col-xs-12 col-sm-4 new-class-padding-0-q hover-tarjetas-proyectos">
               <div class="mx-auto p-relative bg-white shadow-1 size-car-1">

                  <a href="<?= site_url('proyectos/'.toUrl($row->id.'-'.$row->titulo)) ?>">
                      <div>
                         <p class="centrado3-new"><?= $row->titulo ?></p>
                         <img src="<?= base_url() ?>img/proyectos/<?= $row->banner ?>" alt="Man with backpack" class="d-block w-full-tar">
                         <p class="centrado4-new"><?= str_replace(',',' ',$row->tags) ?></p>
                      </div>
                      <div class="px-2 py-2">
                         <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-map-marker width-30-px-new" aria-hidden="true"></i><?= $row->ubicacion ?></p>
                         <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-suitcase width-30-px-new" aria-hidden="true"></i><?= $row->carreras ?></p>
                         <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-calendar width-30-px-new" aria-hidden="true"></i>Estancia mínima:<?= $row->estancia_minima_id ?></p>
                         <p class="mb-1 mb-5-color text-posi-noticia font-size-tarje-new"><i class="fa fa-graduation-cap width-30-px-new" aria-hidden="true"></i><?= $row->carreras_id ?></p>
                      </div>
                  </a>

                  <p class="mb-1 mb-6-color22 centrar-text-tarjeta">Avalado por:</p>
                  <div class="swiper-container swiper-container3">
                     <div class="swiper-wrapper">
                        <?php foreach(explode(',',$row->avaladores) as $a): ?>
                            <div class="swiper-slide swiper-container-3">
                              <img src="<?= base_url() ?>img/avaladores/<?= trim($a) ?>" class="slider-3"/>
                            </div>
                        <?php endforeach ?>
                     </div>
                     <!-- Add Pagination -->
                     <div class="swiper-pagination bottom-gray-slider"></div>
                     <?php if(count(explode(',',$row->avaladores))>1): ?>                       
                       <!-- Add Arrows -->
                       <div class="swiper-button-next bottom-gray-slider"></div>
                       <div class="swiper-button-prev bottom-gray-slider"></div>
                     <?php endif ?>
                  </div>
                  <a href="<?= site_url('proyectos/'.toUrl($row->id.'-'.$row->titulo)) ?>" class="btn btn-default btn-buscar-color2-new general-hover">VER PROYECTO</a>
               </div>
            </div>


    <?php endforeach ?>
    <?php if(count($list)==0): ?>
        <p style="color:black;">
          No encontramos proyectos para sus criterios de búsqueda, por favor intente de nuevo.
        </p>
    <?php endif ?>
</div>
