<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
        }
        
        public function areas_laborales(){
            $this->loadView('areas_laborales');
        }

        function solicitudes(){
            $crud = new ajax_grocery_crud();
            $crud->set_table('solicitudes')
                 ->set_theme('crud')                 
                 ->unset_list()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export();
            $crud->callback_before_insert(function($post){                
                $post['vacantes'] = implode(',', $post['vacante']);
                unset($post['vacante']);
                $post['fecha'] = date("Y-m-d");
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                $this->db->select('user.*, proyectos.titulo');
                $this->db->join('user','user.id = proyectos.user_id');
                $user = $this->db->get_where('proyectos',array('proyectos.id'=>$post['proyectos_id']));
                $post['adminnombre'] = $user->row()->nombre;
                $post['adminapellido'] = $user->row()->apellido;
                $post['proyecto'] = $user->row()->titulo;
                $post['estado'] = $this->db->get_where('estados',array('id'=>$post['estados_id']))->row()->nombre;
                $post['universidad'] = $this->db->get_where('universidades',array('id'=>$post['universidades_id']))->row()->nombre;
                $post['carrera'] = $this->db->get_where('carreras',array('id'=>$post['carreras_id']))->row()->nombre;
                $post['semestre'] = $this->db->get_where('semestres',array('id'=>$post['semestres_id']))->row()->nombre;
                $post['fecha'] = date("d-m-Y",strtotime($post['fecha']));
                $this->enviarcorreo((object)$post,2,$user->row()->email);                
            });
            $crud->required_fields_array();          
            $crud = $crud->render('','application/modules/proyectos/views/');
            $this->loadView($crud);                 
        }

        function listar(){
        	$crud = new ajax_grocery_crud();
            $crud->set_table('view_proyectos_front')
                 ->set_theme('crud')
                 ->set_primary_key('id','view_proyectos_front')
                 ->group_by('id')                 
                 ->field_type('estancia_minima_id','dropdown',array('1'=>'1 dia','7'=>'1 Semana','14'=>'2 Semanas','30'=>'1 Mes','60'=>'2 meses','90'=>'3 meses','120'=>'4 meses','150'=>'5 meses','180'=>'6 meses','365'=>'1 Año'))
                 ->set_url('proyectos/frontend/listar/');;
            $crud->set_relation_n_n('avaladores','proyectos_universidades','universidades','proyectos_id','universidades_id','{logo}');
            $crud->set_relation_n_n('carreras','proyectos_areas','carreras','proyectos_id','carreras_id','{nombre}');
            $crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();   
            $crud->where('estado',1);         
            if(!empty($_POST['estancia_minima'])){
                $crud->where('estancia_minima_id >=',$_POST['estancia_minima']);         
            }
            $crud = $crud->render('','application/modules/proyectos/views/');
            $crud->view = 'list';
            $crud->title = 'Proyectos';
            $this->loadView($crud);                 
        } 

        function listarJSON(){
            $crud = new ajax_grocery_crud();
            $crud->set_table('proyectos')
                 ->set_theme('crud')
                 ->set_url('proyectos/frontend/listar/');
            $crud->set_relation_n_n('avaladores','proyectos_avaladores','avaladores','proyectos_id','avaladores_id','{logo}');
            $crud->set_relation_n_n('carreras','proyectos_areas','carreras','proyectos_id','carreras_id','{nombre}');
            $crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read(); 
            $crud->where('estado',1);
            $crud->columns('id','titulo');           
            $crud = $crud->render('','application/modules/proyectos/views/');
            $crud->view = 'list';
            $crud->title = 'Proyectos';
            $this->loadView($crud);                 
        } 

        public function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $proyectos = new Bdsource();
                $proyectos->where('id',$id);
                $proyectos->init('proyectos',TRUE);
                $this->proyectos->link = site_url('proyectos/'.toURL($this->proyectos->id.'-'.$this->proyectos->titulo));
                $this->proyectos->foto = base_url('img/proyectos/'.$this->proyectos->banner);
                
                $this->loadView(
                    array(
                        'view'=>'detail',
                        'detail'=>$this->proyectos,
                        'title'=>$this->proyectos->titulo                        
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
    }
?>
