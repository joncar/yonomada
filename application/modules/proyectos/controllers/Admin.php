<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }



        public function solicitudes(){
            $crud = $this->crud_function('','');
            $crud->columns('nombre','apellido','correo','telefono','vacantes');               
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function areas_laborales(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Areas laborales');
            $crud->set_field_upload('imagen','img/areas_laborales');          
            $crud = $crud->render();
            $crud->title = 'Areas laborales';
            $this->loadView($crud);
        }

        public function departamentos(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Departamentos de areas laborales');    
            $crud->set_field_upload('imagen','img/areas_laborales');
            $crud = $crud->render();       
            $crud->title = 'Departamentos de areas laborales'; 
            $this->loadView($crud);
        }

        public function universidades(){
            $crud = $this->crud_function('','');                 
            $crud = $crud->render();        
            $this->loadView($crud);
        }

        public function semestres(){
            $crud = $this->crud_function('','');                 
            $crud = $crud->render();        
            $this->loadView($crud);
        }

        public function carreras(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Carreras');     
            $crud->set_field_upload('imagen','img/areas_laborales');
            $crud->set_relation('departamentos_id','departamentos','nombre');
            $crud->set_relation_dependency('departamentos_id','areas_laborales_id','areas_laborales_id');
            $crud = $crud->render();        
            $crud->title = 'Carreras';
            $this->loadView($crud);
        }

        public function destinos(){
            $crud = $this->crud_function('','');                 
            $crud = $crud->render();        
            $this->loadView($crud);
        }

        public function estancia_minima(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Estancia minima');                
            $crud = $crud->render();   
            $crud->title = 'Estancia minima';     
            $this->loadView($crud);
        }

        public function avaladores(){
            $crud = $this->crud_function('','');     
            $crud->set_field_upload('logo','img/avaladores');            
            $crud = $crud->render();        
            $this->loadView($crud);
        }

        function proyectos_testimonios($x){
            $crud = $this->crud_function('','');
            $crud->set_subject('Testimonio de proyectos');
            if($crud->getParameters(false)=='list'){
                redirect('proyectos/admin/proyectos/edit/'.$x);
            }
            $crud->set_field_upload('imagen','img/testimonios_crud');      
            $crud->where('proyectos_id',$x);
            $crud->field_type('proyectos_id','hidden',$x);            
            $crud = $crud->render();
            $crud->title = 'Testimonio de proyectos';
            $this->loadView($crud);
        }

        function proyectos_vacantes($x){
            $crud = $this->crud_function('','');
            $crud->set_subject('Vacantes');
            if($crud->getParameters(false)=='list'){
                redirect('proyectos/admin/proyectos/edit/'.$x);
            }
            $crud->where('proyectos_id',$x);
            $crud->set_relation('carreras_id','carreras','nombre');
            $crud->field_type('proyectos_id','hidden',$x)
                 ->field_type('estancia_minima','dropdown',array('1'=>'1 dia','7'=>'1 Semana','14'=>'2 Semanas','30'=>'1 Mes','60'=>'2 meses','90'=>'3 meses','120'=>'4 meses','150'=>'5 meses','180'=>'6 meses','365'=>'1 Año'));  
            $crud->set_lang_string('insert_success_message','Sus datos han sido almacenados con éxito <script>document.location.href="'.base_url('proyectos/admin/proyectos_vacantes_detalles').'/{id}/add";</script>');          
            $crud->add_action('detalles','',base_url('proyectos/admin/proyectos_vacantes_detalles').'/');
            $crud = $crud->render();
            $crud->title = 'Vacantes';
            $this->loadView($crud);
        }

        function proyectos_vacantes_detalles($x){
            $crud = $this->crud_function('','');  
            $crud->set_subject('Detalle de vacantes');     
            $crud->set_relation('areas_laborales_id','areas_laborales','nombre')
                 ->set_relation('departamentos_id','departamentos','nombre')
                 ->set_relation('carreras_id','carreras','nombre')
                 ->set_relation_dependency('departamentos_id','areas_laborales_id','areas_laborales_id')
                 ->set_relation_dependency('carreras_id','departamentos_id','departamentos_id');
            $crud->where('proyectos_vacantes_id',$x);
            $crud->field_type('proyectos_vacantes_id','hidden',$x);            
            $crud->field_type('puestos','tags');            
            $crud = $crud->render();
            $crud->title = 'Detalle de las vacantes';
            $this->loadView($crud);
        }

        function testimoniosList($y){
            $this->as['proyectos'] = 'proyectos_testimonios';
            $crud = $this->crud_function('','');
            $crud->where('proyectos_id',$y);
            $crud->set_url('proyectos/admin/proyectos_testimonios/'.$y.'/');
            $crud = $crud->render(1);            
            return $crud;
        }

        function vacantesList($y){
            $this->as['proyectos'] = 'proyectos_vacantes';
            $crud = $this->crud_function('','');
            $crud->where('proyectos_id',$y);
            $crud->set_url('proyectos/admin/proyectos_vacantes/'.$y.'/');
            $crud->add_action('detalles','',base_url('proyectos/admin/proyectos_vacantes_detalles').'/');
            $crud = $crud->render(1);
            return $crud;
        }

        public function solicitudesList($y){
            $this->as['proyectos'] = 'solicitudes';
            $crud = $this->crud_function('','');  
            $crud->where('proyectos_id',$y);      
            $crud->set_url('proyectos/admin/solicitudes/'.$y.'/');  
            $crud->columns('nombre','apellido','correo','telefono','vacantes_id');   
            $crud = $crud->render(1);
            return $crud;
        }

        public function proyectos($x = '',$y = ''){
            $crud = $this->crud_function('','');  
            $crud->set_lang_string('insert_success_message','Sus datos han sido almacenados con éxito <script>document.location.href="'.base_url('proyectos/admin/proyectos/edit/').'/{id}";</script>');               
            $crud->field_type('ubicacion_mapa','map',array('width'=>'300px','height'=>'300px'));
            $crud->set_field_upload('banner','img/proyectos'); 
            $crud->field_type('tags','tags')
                 ->field_type('estancia_minima_id','dropdown',array('1'=>'1 dia','7'=>'1 Semana','14'=>'2 Semanas','30'=>'1 Mes','60'=>'2 meses','90'=>'3 meses','120'=>'4 meses','150'=>'5 meses','180'=>'6 meses','365'=>'1 Año'));
            
            $crud->set_relation_n_n('universidades','proyectos_universidades','universidades','proyectos_id','universidades_id','{nombre}');
            $crud->callback_before_insert(function($post){
                if(!empty($post['lat']) && !empty($post['lon'])){
                    $post['ubicacion_mapa'] = '('.$post['lat'].','.$post['lon'].')';
                    $post['lat'] = '';
                    $post['lon'] = '';
                }
                return $post;
            });
            $crud->callback_before_update(function($post){
                if(!empty($post['lat']) && !empty($post['lon'])){
                    $post['ubicacion_mapa'] = '('.$post['lat'].','.$post['lon'].')';
                    $post['lat'] = '';
                    $post['lon'] = '';
                }
                return $post;
            });
            $crud->columns('titulo','descripcion_corta','user_id','estado','destacado');
            if($this->user->admin!=1){
                $crud->where('proyectos.user_id',$this->user->id);
                $crud->field_type('estado','invisible');
                $crud->field_type('destacado','invisible');
                $crud->field_type('user_id','hidden',$this->user->id);

            }else{
                $crud->unset_add();
                $crud->field_type('estado','dropdown',array('-1'=>'Cancelado','0'=>'Borrador','1'=>'Publicado'));
                $crud->field_type('destacado','true_false');
            }   
            $crud->set_lang_string('insert_success_message','Sus datos se han almacenado con éxito, por favor espere. <script>setTimeout(function(){document.location.href="'.base_url('proyectos/admin/proyectos/edit/').'/{id}";},1500)</script>');      
            $crud->unset_back_to_list()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export()
                 ->display_as('ubicacion','Dirección');
            $out = $crud->render();        
            if($crud->getParameters()=='edit'){                
                $out->testimonios = $this->testimoniosList($y);
                $out->vacantes = $this->vacantesList($y);
                $out->solicitudes = $this->solicitudesList($y);
                $out->js_files = array_merge($out->js_files,$out->testimonios->js_files);
                $out->css_files = array_merge($out->css_files,$out->testimonios->css_files);
                $out->output = $this->load->view('editarproyectos',array('output'=>$out),TRUE);
            }
            $this->loadView($out);
        }



        
    }
?>
