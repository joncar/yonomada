<!-- MAIN CONTENT-->
<div class="main-content">
    <section class="page-banner blog">
        <div class="patterndestino"></div>
        <div class="container">
            <div class="page-title-wrapper">
                <div class="page-title-content">
                    <ol class="breadcrumb">
                        <li><a href="index.html" class="link home">Home</a></li>
                        <li class="active"><a href="#" class="link">Blog</a></li>
                    </ol>
                    <div class="clearfix"></div>
                    <h2 class="captions">Darreres noticies</h2></div>
            </div>
        </div>
    </section>
    <section class="page-main padding-top padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-8 main-left blog-wrapper">

                    <?php foreach ($detail->result() as $d): ?>
                        <div class="blog-post">
                            <div class="blog-image">
                                <a href="<?= $d->link ?>" class="link">
                                    <img src="<?= $d->foto ?>" alt="a car on a road" class="img-responsive">
                                </a>
                            </div>
                            <div class="blog-content">
                                <div class="col-xs-2">
                                    <div class="row">
                                        <div class="date">
                                            <h1 class="day"><?= date("d", strtotime($d->fecha)) ?></h1>
                                            <div class="month"><?= date("m", strtotime($d->fecha)) ?></div>
                                            <div class="year"><?= date("Y", strtotime($d->fecha)) ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-10 content-wrapper">
                                    <a href="<?= $d->link ?>" class="heading"><?= $d->titulo ?></a>
                                    <h5 class="meta-info">
                                        Posted By : <span><?= $d->user ?></span>
                                        <span class="sep">/</span>
                                        <span class="view-count fa-custom"><?= $d->visitas ?></span>
                                        <span class="comment-count fa-custom"><?= $d->comentarios ?></span>
                                    </h5>
                                    <p class="preview"><?= substr(strip_tags($d->texto), 0, 255) . '...' ?></p>
                                    <a href="<?= $d->link ?>" class="btn btn-gray btn-fit btn-capitalize">Llegir Més</a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                    <nav class="pagination-list margin-top70">
                        <ul class="pagination">
                            <li><a href="javascript:changePage(1)" aria-label="Previous" class="btn-pagination previous"><span aria-hidden="true" class="fa fa-angle-left"></span></a></li>
                            <?php for ($i = 1; $i <= $total_pages; $i++): ?>
                                <li><a href="javascript:changePage('<?= $i ?>')" class="btn-pagination active"><?= $i ?></a></li>
                            <?php endfor ?>
                            <li><a href="javascript:changePage(<?= !empty($_GET['page']) ? $_GET['page'] + 1 : 2 ?>)" aria-label="Next" class="btn-pagination next"><span aria-hidden="true" class="fa fa-angle-right"></span></a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-4 sidebar-widget">
                    <div class="col-2">
                        <div class="search-widget widget">
                            <form action="<?= base_url("blog") ?>">
                                <div class="input-group search-wrapper">
                                    <input type="text" placeholder="Buscar..." name="direccion" class="search-input form-control">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn submit-btn">
                                            <span class="fa fa-search"></span>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="col-1">
                            <div class="recent-post-widget widget">
                                <div class="title-widget">
                                    <div class="title">NOTICIAS RECIENTES</div>
                                </div>
                                <div class="content-widget">
                                    <div class="recent-post-list">

                                        <?php foreach ($recientes->result() as $d): ?>
                                            <div class="single-widget-item">
                                                <div class="single-recent-post-widget">
                                                    <a href="<?= $d->link ?>" class="thumb img-wrapper">
                                                        <img src="<?= $d->foto ?>" alt="recent post picture 1">
                                                    </a>
                                                    <div class="post-info">
                                                        <div class="meta-info">
                                                            <span><?= date("d-m-Y", strtotime($d->fecha)) ?></span>
                                                            <span class="sep">/</span>
                                                            <span class="fa-custom view-count"><?= $d->visitas ?></span>
                                                            <span class="fa-custom comment-count"><?= $d->comentarios ?></span>
                                                        </div>
                                                        <div class="single-rp-preview"><?= $d->titulo ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-1">
                            <div class="categories-widget widget">
                                <div class="title-widget">
                                    <div class="title">CATEGORIAS</div>
                                </div>
                                <div class="content-widget">
                                    <ul class="widget-list">
                                        <?php foreach ($categorias->result() as $d): ?>
                                            <li class="single-widget-item">
                                                <a href="<?= base_url('blog') ?>?blog_categorias_id=<?= $d->id ?>" class="link">
                                                    <span class="fa-custom category"><?= $d->blog_categorias_nombre ?></span>
                                                    <span class="count"><?= $d->cantidad ?></span>
                                                </a>
                                            </li>
                                        <?php endforeach ?>    
                                    </ul>
                                </div>
                            </div>
                            <div class="tags-widget widget">
                                <div class="title-widget">
                                    <div class="title">TAGS</div>
                                </div>
                                <div class="content-widget">
                                    <?php if ($detail->num_rows() > 0): ?>
                                        <?php foreach (explode(',', $detail->row()->tags) as $e): ?>
                                            <a href="<?= base_url('blog') ?>?direccion=<?= $e ?>" class="tag"><?= $e ?></a>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-2">                        
                        <div class="col-1">
                            <div class="gallery-widget widget">
                                <div class="title-widget">
                                    <div class="title">GALERIA</div>
                                </div>
                                <div class="content-widget">
                                    <ul class="list-unstyled list-inline">
                                        <?php $this->db->order_by('id','DESC') ?>
                                        <?php $this->db->limit(8) ?>
                                        <?php $galeria = $this->db->get('galeria'); ?>
                                            <?php foreach($galeria->result() as $p): ?>
                                                <li>                                                                                                      
                                                    <div style="width:100px; height:100px; background:url(<?= base_url() ?>img/galeria/<?= $p->foto ?>); background-size:auto 100%; background-position:center;" class="img-responsive"></div>                                                        
                                                </li>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="social-widget widget">
                                <div class="title-widget">
                                    <div class="title">SOCIAL</div>
                                </div>
                                <div class="content-widget">
                                    <ul class="list-unstyled list-inline">
                                        <li>
                                            <a href="javascript:void(0)" class="social-icon fa fa-facebook"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" class="social-icon fa fa-twitter"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" class="social-icon fa fa-instagram"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" class="social-icon fa fa-google"></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" class="social-icon fa fa-rss"></a>
                                        </li>                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Blog page --->
<script src="<?= base_url() ?>js/template/pages/sidebar.js"></script>
<script src="<?= base_url() ?>js/template/pages/blog.js"></script>
