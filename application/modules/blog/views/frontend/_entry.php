<div class="row">
    <div class="col-xs-12 col-sm-12">
       <div class="col-xs-12 col-sm-12 padding-tarje-2">
          <div class="col-xs-12col-sm-12" style="padding-bottom: 50px;padding-left: 0px;">
             <div class="col-xs-12 col-sm-8" style="margin-bottom: 40px;">
                <h2 class="title-menu-2 text-gray">¡Mantente al día!</h2>
                <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
             </div>
             <div class="col-xs-0 col-sm-2"></div>
             <div class="col-xs-12 col-sm-2" style="padding-right: 0px;">
                <div class="custom-select" style="width:95%;top: 15px;">
                   <select>
                      <option value="0">Todos los articulos</option>
                      <option value="1">Audi</option>
                      <option value="2">BMW</option>
                      <option value="3">Citroen</option>
                      <option value="4">Ford</option>
                      <option value="5">Honda</option>
                   </select>
                </div>
             </div>
          </div>

          <?php
          $this->db->limit(2);
          $this->db->order_by('fecha','DESC');
          foreach($this->db->get_where('blog')->result() as $b): ?>
              <div class="col-xs-12 col-sm-6">
                 <figure class="snip1527">
                    <div class="image">
                        <img src="<?= base_url('img/blog/'.$b->foto) ?>" alt="pr-sample25" class="img-big-blog" />}
                    </div>
                    <figcaption>
                       <div class="date">
                          <span class="day"><?= date("d",strtotime($b->fecha)) ?></span><span class="month"><?= strftime("%b",strtotime($b->fecha)) ?></span>
                       </div>
                       <h3><?= $b->titulo ?></h3>
                       <p>
                          <?= substr(strip_tags($b->texto),0,255) ?>...
                       </p>
                    </figcaption>
                    <a href="<?= site_url('blog/'.toUrl($b->id.'-'.$b->titulo)) ?>"></a>
                 </figure>
              </div>
        <?php endforeach ?>

       </div>
    </div>
 </div>
 <div class="row">
    <div class="col-xs-12 col-sm-12">
       <div class="col-xs-12 col-sm-12">

          <?php
          $this->db->limit(3,2);
          $this->db->order_by('fecha','DESC');
          foreach($this->db->get_where('blog')->result() as $b): ?>
              <div class="col-xs-12 col-sm-4 padding-tarje-2">
                 <figure class="snip1527">
                    <div class="image">
                        <img src="<?= base_url('img/blog/'.$b->foto) ?>" alt="pr-sample25" class="img-big-blog" />}
                    </div>
                    <figcaption>
                       <div class="date">
                            <span class="day"><?= date("d",strtotime($b->fecha)) ?></span><span class="month"><?= strftime("%b",strtotime($b->fecha)) ?></span>
                       </div>
                       <h3><?= $b->titulo ?></h3>
                       <p>
                          <?= substr(strip_tags($b->texto),0,155) ?>...
                       </p>
                    </figcaption>
                    <a href="<?= site_url('blog/'.toUrl($b->id.'-'.$b->titulo)) ?>"></a>
                 </figure>
              </div>
          <?php endforeach ?>
       </div>
    </div>

    <div class="col-xs-12">
        <ul class="pagination">
          <li class="paginate_button">
            <a href="<?= base_url('blog') ?>?page=1">«</a>
          </li>
          <?php for($i=1;$i<=$total_pages;$i++): ?>
              <li>
                <a href='<?= base_url('blog') ?>?page=<?= $i ?>'>
                  <span class='page-numbers <?= $i==$current_page?'current':'' ?>'><?= $i ?></span>
                </a>
              </li>
          <?php endfor ?>
          <li class="paginate_button">
            <a href="<?= base_url('blog') ?>?page=<?= $total_pages ?>">»</a>
          </li>
          </ul>
        </ul>
    </div>

 </div>
