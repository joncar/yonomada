<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Fotos extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function categorias_fotos(){
            $crud = $this->crud_function('','');                        
            $crud->add_action('<i class="fa fa-picture"></i> Adm. Fotos','',base_url('paginas/fotos/galeria/').'/');
            $crud->set_clone();
            $crud->field_type('idioma','dropdown',array('es'=>'Castellano','ca'=>'Catalán','en'=>'Ingles'));
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function galeria($x = '',$y = ''){
            $this->as['galeria'] = 'fotos';            
            $crud = $this->crud_function('','');   
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Castellano','en'=>'Ingles'));
            $crud->where('categorias_fotos_id',$x);
            $crud->field_type('categorias_fotos_id','hidden',$x);
            $crud->unset_columns('categorias_fotos_id');
            $crud->set_field_upload('foto','img/entorno');
            $crud->field_type('mostrar_en_tot','checkbox');            
            $crud = $crud->render();
            $this->loadView($crud);
            
            /*$this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('fotos')
                 ->set_url_field('foto')
                 ->set_image_path('img/entorno')
                 ->set_subject('Fotos')
                 ->set_relation_field('categorias_fotos_id')
                 ->set_title_field('titulo')
                 ->set_ordering_field('priority');
            $crud->module = 'paginas';
            $crud = $crud->render();
            $crud->title = 'Galeria fotográfica';
            $this->loadView($crud);*/
        }
    }
?>


