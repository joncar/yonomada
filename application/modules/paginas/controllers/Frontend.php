<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }       

        public function loadView($param = array('view' => 'main')) {
            if($this->router->fetch_method()!='editor'){
                $param['page'] = $this->querys->fillFields($param['page']);
            }
            parent::loadView($param);
        }
        
        function read($url){
            $theme = $this->theme;
            $params = $this->uri->segments;
            $this->load->model('querys');
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$this->load->view($theme.$url,array(),TRUE),
                    'title'=>ucfirst(str_replace('-',' ',$url))
                )
            );
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                
                //$page = file_get_contents('application/modules/paginas/views/'.$url.'.php');
                /*$page = str_replace('<?php','[?php',$page);
                $page = str_replace('<?=','[?=',$page);
                $page = str_replace('&gt;','>',$page);
                $page = str_replace('&lt;','<',$page);*/
                $page = $this->load->view($url,array(),TRUE);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){             
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('telefono','Telefono','required');
            $this->form_validation->set_rules('message','Comentario','required');            
            if($this->form_validation->run()){
                $this->load->library('recaptcha');
                /*if(!$this->recaptcha->recaptcha_check_answer($_POST['g-recaptcha-response'])){
                    $_SESSION['msj'] = $this->error('Captcha introduit incorrectament');
                }else{*/
                    //$this->enviarcorreo((object)$_POST,1,'info@yonomada.com');                
                    $_SESSION['msj'] = $this->success('Gracias por contactarnos, en breve le llamaremos');
                //}*/
            }else{                
               $_SESSION['msj'] = $this->error('Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>');               
            }             
            if(!empty($_GET['redirect'])){
                redirect($_GET['redirect']);
            }else{
                redirect(base_url().'#contacto');
            }
        }
        
        function subscribir(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            if($this->form_validation->run()){
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()==0?TRUE:FALSE;
                if($success){
                    $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                    $_SESSION['msj2'] = $this->success('Te has inscrito al newsletter');
                }else{
                    $_SESSION['msj2'] = $this->error('El correo ya se encuentra registrado');
                }
            }else{
                $_SESSION['msj2'] = $this->error($this->form_validation->error_string());
            }
            redirect(base_url().'#subscribir');
        }
        
        function unsubscribe(){
            if(empty($_POST)){
                $this->loadView('includes/template/unsubscribe');
            }else{
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()>0?TRUE:FALSE;
                if($success){
                    $this->db->delete('subscritos',array('email'=>$_POST['email']));
                    echo $this->success('Correo desafiliado al sistema de noticias');
                }            
                $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
            }
        }

        function search(){
            if(!empty($_GET['q'])){
                if(empty($_SESSION[$_GET['q']])){
                    $_SESSION[$_GET['q']] = file_get_contents('http://www.google.es/search?ei=meclXLnwCNma1fAPgbCYCA&q=site%3Amallorcaislandfestival.com+'.urlencode($_GET['q']).'&oq=site%3Amallorcaislandfestival.com+'.urlencode($_GET['q']).'&gs_l=psy-ab.3...10613.16478..16743...0.0..0.108.1753.20j3......0....1..gws-wiz.0XRgmCZL0TA');                
                }
                $result = $_SESSION[$_GET['q']];
                preg_match_all('@<div id=\"ires\">(.*)</div>@si',$result,$result);
                $resultado = $result[0][0];
                $resultado = fragmentar($resultado,'<ol>','</ol>');
                $resultado = !empty($resultado[0])?$resultado[0]:array();

                if(!empty($resultado)){
                    $resultado = str_replace('http://www.mallorcaislandfestival.com/url?q=','',$resultado);
                    $resultado = str_replace('/url?q=','',$resultado);
                    $resultado = explode('<div class="g">',$resultado);                
                    foreach($resultado as $n=>$r){
                        if(strpos($r,'/search?q=site:')){
                            unset($resultado[$n]);
                            continue;
                        }
                        $resultado[$n] = '<div class="g">'.$r;                    
                        $resultado[$n] = substr($r,0,strpos($r,'&'));
                        $pos = strpos($r,'">')+2;
                        $rr = substr($r,$pos);
                        $pos = strpos($rr,'">');
                        $rr = substr($rr,$pos);                    
                        $resultado[$n].= $rr;
                        $resultado[$n] = '<div class="g">'.$resultado[$n];
                        $resultado[$n] = utf8_encode($resultado[$n]);
                    }
                }
                
                $this->loadView(array(
                    'view'=>'read',
                    'page'=>$this->load->view($this->theme.'search',array('resultado'=>$resultado),TRUE,'paginas'),
                    'result'=>$resultado,
                    'title'=>'Resultado de busqueda'
                ));

            }
        }

        function sitemap(){
            $pages = array(
                base_url(),
                base_url().'areas-laborales',
                base_url().'proyectos',
                base_url().'galeria.html',
                base_url().'blog',
                base_url().'nosotros.html',
                base_url().'terminos-y-condiciones.html',
                base_url().'aviso-de-privacidad.html'
            );

            //Blogs
            foreach($this->db->get_where('blog')->result() as $b){
                $pages[] = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
            }
            //proyectos
            foreach($this->db->get_where('view_proyectos_front',array('estado'=>1))->result() as $b){                
                $pages[] = base_url('proyecto/'.toUrl($b->id.'-'.$b->titulo));
            }
            
            $site = '<?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
            foreach($pages as $p){
                $site.= '
                <url>
                      <loc>'.trim($p).'</loc>
                      <lastmod>'.date("Y-m-d").'T11:43:00+00:00</lastmod>
                      <priority>1.00</priority>
                </url>';
            }
            $site.= '</urlset>';
            ob_end_clean();
            ob_end_flush();
            header('Content-Type: application/xml');
            echo $site;
        }
}
