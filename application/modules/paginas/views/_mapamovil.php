<div class="col-xs-12 mapa-padding mensaje-estado-mapa-movil visible-xs">
  <img src="http://bluepixel.mx/yonomada/img/mapa.png" alt="Mapa Yo nómada" class="center-block img-responsive">
  <div class="titulo-proyectos-modal-mapa">
    <b>Proyectos:<br>

      <?= form_dropdown_from_query('estados','estados','id','nombre','','id="estadoMapaSelect"',FALSE,'estadoMapaSelect') ?>
    </b>
  </div>
  <div id="proyectosEstadoMovil">

  </div>


  <div>
      <a href="<?= base_url('proyectos') ?>" class="text-black-filtros col-cleal-filtros btn-filtros-izquierda">Ver todo<br>los proyectos</a>
  </div>
</div>

<script>
 $(document).ready(function(){
 	var divProyectoEstado = '<a href="{link}"><div class="link-mensaje-mapa"><small>Proyecto:</small><br><b>{titulo}</b></div></a>';
 	$("#estadoMapaSelect").on('change',function(){
 		$("#proyectosEstadoMovil").html('Buscando proyectos');
        $.post('<?= base_url('proyectos/frontend/listarJSON/json_list') ?>',{
          'search_text[]':$(this).val(),
          'search_field[]':'estados_id'
        },function(data){
            data = JSON.parse(data);
            if(data.length>0){
              var str = '';
              for(var i in data){
                var s = divProyectoEstado.replace('{link}','<?= base_url('proyectos') ?>/'+data[i].Id);
                s = s.replace('{titulo}',data[i].Titulo);
                str+= s;
              }

              $("#proyectosEstadoMovil").html(str);
            }else{
              $("#proyectosEstadoMovil").html('Proyectos no encontrados, intenta en otro estado');
            }
        });
 	});
  });
</script>
