      <header class="container-fluid header3" id="conteiner-fluid-0">
          <div class="container">
              <!-- Menu -->
              [menu]
          </div>
         <div class="row row-new-2">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
               <div class="col-xs-12 col-sm-12">
                  <h1 class="text-center">ÁREAS LABORALES</h1>
                  <div class="text-center-new1">¡Selecciona tu carrera o área laboral y explora los proyectos disponibles en los que puedes participar!</div>
               </div>
            </div>
         </div>
      </header>

<!-- Conteiner-->
      <div class="container-fluid fondo-gris">
         <div class="row contendor-contenido">
            <div class="col-xs-12 col-sm-12">

               <div class="tabset">
                  <!-- Tab 1 -->
                  [foreach:categorias]
                  <input type="radio" name="tabset" id="tab[id]" aria-controls="marzen" [checked]>
                  <label for="tab[id]" class="font-tab-black [tipocuadro] tamano-laber-a">
                      <div class="display-100 contenedor6 img-tabs-top">
                          <img src="[imagen]" alt="[nombre]" class="img-responsive centerblock ">
                          <span class="centrado2">[nombre]</span>
                      </div>
                  </label>
                  [/foreach]
                  <!-- Conteiner-->
                  <div class="tab-panels col-xs-12 col-sm-12" style="padding: 0px; ">
                    [categoriascontent]
                  </div>
               </div>

            </div>
         </div>
      </div>

      <!-- Contacto Footer -->
      <div class="container-fluid" id="contacto">
        [contactoForm.php]
      </div>

      <!-- Footer-->
      <footer>
        [footer.php]
      </footer>
