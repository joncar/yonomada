<!-- Galeria 02 -->
<link href="http://bluepixel.mx/yonomada/css/template/Galeria02/uikit-rtl.css" rel="stylesheet" />
<link href="http://bluepixel.mx/yonomada/css/template/Galeria02/uikit-rtl.min.css" rel="stylesheet" />
<link href="http://bluepixel.mx/yonomada/css/template/Galeria02/uikit.css" rel="stylesheet" />
<link href="http://bluepixel.mx/yonomada/css/template/Galeria02/uikit.min.css" rel="stylesheet" />
<!-- JS -->
<script src="http://bluepixel.mx/yonomada/js/Galeria02/uikit-icons.js"></script>
<script src="http://bluepixel.mx/yonomada/js/Galeria02/uikit-icons.min.js"></script>
<script src="http://bluepixel.mx/yonomada/js/Galeria02/uikit.js"></script>
<script src="http://bluepixel.mx/yonomada/js/Galeria02/uikit.min.js"></script>

<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="http://masonry.desandro.com/masonry.pkgd.js"></script>



<body class="body3">
<header class="container-fluid" id="conteiner-fluid-0 ">
	 <div class="container ">
	     [menu]
	 </div>
</header>

<!--
<div class="container-fluid">
 <div class="row">
	<div id="gallery" style="display:none;">

				[foreach:galeria]
					<a href="[enlace]">
					<img alt="[titulo]" src="[foto]" data-image="[foto]" data-description="[titulo]" style="display:none">
					</a>
				[/foreach]

	</div>
</div>
</div> -->


<div class="container-fluid">
	 <div class="contenedor-galeria">

				<div uk-lightbox class="img-galeria">
						[foreach:galeria]
							<div class="galeria">
						  		<a class="img-galeria" href="[enlace]" data-caption="[titulo]">
						  			<img src="[foto]" alt="galeria Yo nomáda" class="img-responsive center-block">
						  		</a>
							</div>
				  	[/foreach]
				</div>

		</div>
</div>


<script>
	var container = document.querySelector('.contenedor-galeria');
	var msnry = new Masonry( container, {
		// options
		itemSelector: '.galeria'
	});
</script>

<!--
<script type="text/javascript">
      jQuery(document).ready(function(){
        jQuery("#gallery").unitegallery({
          tiles_type:"nested",
          tiles_nested_optimal_tile_width: 450
        });
      });
    </script> -->
</body>
