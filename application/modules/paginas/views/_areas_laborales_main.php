<?php foreach($this->db->get_where('areas_laborales')->result() as $a): ?>
<div class="col-sm-12">
   <div class="col-sm-1"></div>
   <div class="col-sm-10 tex-align-center top-title-new-index-laborales">
      <span class="text-black"><b><?= $a->nombre ?></b></span><p class="pink-bullet"></p>
   </div>
   <div class="col-sm-1"></div>
</div>

<!--
<div class="col-xs-12 col-sm-12">
   <div class="col-sm-1"></div>

   <?php foreach($this->db->get_where('departamentos',array('areas_laborales_id'=>$a->id))->result() as $aa): ?>
       <a href="<?= base_url('proyectos') ?>?departamentos_id=<?= $a->id ?>">
          <div class="col-xs-6 col-sm-2 hover-areas-laborales-ciencias clearfix">
            <img src="<?= base_url() ?>img/areas_laborales/<?= $aa->imagen ?>" alt="Administración" class="img-responsive center-block padding-img-laborales">
            <div class="centrado"><?= $aa->nombre ?></div>
         </div>
       </a>
	 <?php endforeach ?>

   <div class="col-sm-1"></div>
</div>-->


<div class="col-xs-12 col-sm-10 col-sm-offset-1">

   <?php foreach($this->db->get_where('departamentos',array('areas_laborales_id'=>$a->id))->result() as $aa): ?>
       <a href="<?= base_url('proyectos') ?>?departamentos_id=<?= $a->id ?>">
          <div class="col-xs-6 col-sm-2 hover-areas-laborales-ciencias clearfix">
            <img src="<?= base_url() ?>img/areas_laborales/<?= $aa->imagen ?>" alt="Administración" class="img-responsive center-block padding-img-laborales">
            <div class="centrado"><?= $aa->nombre ?></div>
         </div>
       </a>
	 <?php endforeach ?>

</div>

<?php endforeach ?>
