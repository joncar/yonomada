

<?php
	$this->db->select('universidades.nombre as universidad, proyectos.titulo, proyectos_testimonios.*');
	$this->db->join('universidades','universidades.id = proyectos_testimonios.universidades_id');
	$this->db->join('proyectos','proyectos.id = proyectos_testimonios.proyectos_id');
	foreach($this->db->get_where('proyectos_testimonios')->result() as $p): ?>
	<div class="swiper-slide">
	    <div class="col-sm-6 text-center">
	        <img src="<?= base_url('img/testimonios_crud/'.$p->imagen) ?>">
	        <p>
	        <b><big><?= $p->titulo ?></big></b><br>
	        <?= strip_tags($p->texto) ?><br>
	        <small><?= $p->autor ?><br>
	        <?= $p->universidad ?></small>
	        </p>
	    </div>
	</div>
<?php endforeach ?>
