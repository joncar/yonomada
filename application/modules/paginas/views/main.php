
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<header class="container-fluid header" id="conteiner-fluid-0">
  <div class="container">
    <!-- Menu -->
    [menu]
  </div>
  <!-- Rectangulo -->
  <div class="row">
    <div class="col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
      <h1 class="text-center">DEJA TU HUELLA,</br>CREA UN IMPACTO</h1>
    </div>
  </div>
  <form action="<?= base_url('proyectos') ?>" method="get">
    <div class="row">
      <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 fondo-1">
        <div class="col-sm-12 fondo2">
          <div class="col-sm-10 col-sm-offset-1 titulo-buscador">Comienza tu aventura seleccionando uno o m&aacute;s filtros:</div>
          <div class="col-sm-12 padding0 text-align-general" id="responsive-menu-2-rec">
            <div class="col-sm-3">
              <select id="mounth" name="areas_laborales_id">
                <option value="hide">Áreas laborales</option>
                [foreach:areas_laborales]
                <option value="[id]">[nombre]</option>
                [/foreach]
              </select>
            </div>
            <div class="col-sm-3">
              <select id="mounth" name="universidades_id">
                <option value="hide">Universidad</option>
                [foreach:universidades]
                <option value="[id]">[nombre]</option>
                [/foreach]
              </select>
            </div>
            <div class="col-sm-3">
              <select id="mounth" name="estados_id">
                <option value="hide">Destino</option>
                [foreach:estados]
                <option value="[id]">[nombre]</option>
                [/foreach]
              </select>
            </div>
            <div class="col-sm-3">
              <select id="mounth" name="estancia_minima_id">
                <option value="hide">Estancia mínima</option>
                <option value="1">1 Día</option>
                <option value="7">1 Semana</option>
                <option value="14">2 Semanas</option>
                <option value="30">1 Mes</option>
                <option value="60">2 Meses</option>
                <option value="90">3 Meses</option>
                <option value="120">4 Meses</option>
                <option value="150">5 Meses</option>
                <option value="180">6 Meses</option>
                <option value="365">1 Año</option>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row row-btn">
      <div class="col-sm-12 col-md-8 col-md-offset-2">
        <div class="col-sm-4"></div>
        <div class="col-sm-4"></div>
        <div class="col-sm-4 bording-buttom-cafe">
          <div class="rectangulo33">
            <button type="submit" class="btn btn-buscar-color" style="padding-top: 0px;padding-bottom: 0px;"><span class="boton-space">BUSCAR</span></button>
          </div>
        </div>
      </div>
    </div>

    <div class="row hidden-xs">
      <section id="section06" class="demo">
        <a href="#section07"><span></span>Scroll</a>
      </section>
    </div>

  </form>
</header>




<!-- Primera seccion -->
<div class="container-fluid">
  <div class="row row-btn">
    <div class="col-sm-12">
      <div class="col-sm-10 col-sm-offset-2">
        <div class="col-xs-12 col-sm-3">
          <div class="rec-blue-ninos"></div>
          <img src="<?= base_url() ?>img/ninos.jpg" alt="ninos" class="img-responsive center-block margin-t-50">
        </div>
        <div class="col-sm-7 columna-text-gray">
          <span class="text-gray text-gray-general">Renovando el trabajo social en M&eacute;xico</span>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row row-btn">
    <div class="col-sm-12">
      <div class="col-sm-5"></div>
      <div class="col-sm-6 columna-tex-gray-littel">
        <span class="tex-gray-littel text-gray-general">Yo N&oacute;mada es una organizaci&oacute;n que acerca a estudiantes y profesionistas con quienes m&aacute;s necesitan de su experiencia y talento. Vive el trabajo social de forma diferente, emprende tu camino, transformando para siempre tu vida y la de los dem&aacute;s.</span></br></br>
        <span class="tex-bold-gray text-gray-general"><big><b>Vu&eacute;lvete un N&oacute;mada, &uacute;nete a la comunidad.</b></big></span>
      </div>
      <div class="col-sm-1"></div>
    </div>
  </div>
</div>


<div class="container">
  <div class="btn-modal" data-toggle="modal" data-target="#myModal">
      <img src="<?= base_url() ?>img/img-video.png" alt="ninos" class="img-responsive center-block">
  </div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-dialog-video">

      <!-- Modal content-->
      <div class="modal-content" style="background-color:transparent; box-shadow: 0 0px 0px rgba(0,0,0,.5);">
        <div class="modal-body">

          <div class="row">
            <div class="col-sm-12">
              <div class="video-container">
                <iframe width="auto" height="auto" src="https://www.youtube.com/embed/KuOSMUBXPJc" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>

    </div>
  </div>

</div>




<!-- Tercer seccion mapa-->
<div class="container-fluid">
  <div class="row row-mapa ">
    <div class="col-sm-12">
      <h2 class="text-gray-2 text-gray-general text-mapa-padding">
      Explora el mapa para conocer todas las oportunidades:
    </h2>


      <!-- Movil -->
      [mapamovil]


      <!-- Escritorio -->
      <div class="col-sm-2 col-md-2 col-xs-12 padding0 mapa-padding mensaje-estado-mapa hidden-xs" style="visibility:hidden" id="mapaDesktop">
          <div class="titulo-proyectos-modal-mapa text-center">
            <b>Proyectos:<br>
              <span class="text-uppercase texto-blanco" id="mapaEstado"></span>
            </b>
          </div>
          <div id="proyectosEstado" >

          </div>
          <div>
              <a href="<?= base_url('proyectos') ?>" class="text-black-filtros col-cleal-filtros btn-filtros-izquierda text-center">Ver todos<br>los proyectos</a>
          </div>
      </div>

      <div class="col-sm-10 col-md-10 col-xs-12 mapa-padding hidden-xs">
        <div id="vmap" style="width: 100%; height: 400px;"></div>
        <!--<img src="<?= base_url() ?>img/mapa.png" alt="Video Yo nomada" class="img-responsive center-block margin-t-50">
        <div class="col-sm-5 col-xs-12 ocultar-div">-->
      </div>


      <!--
      <div class="col-sm-3 col-md-2 col-xs-12 margen-check ocultar-div filtros-mapa-inicio hidden-xs">
        <div id="responsive-col-2">
          <div class="checkbox">
            <input id="box1" type="checkbox" />
            <label for="box1"><span class="text-checkbox"><i class="fa fa-user-circle-o margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i> Integración Social (1)</span></label>
          </div>
          <div class="checkbox">
            <input id="box2" type="checkbox" />
            <label for="box2"><span class="text-checkbox"><i class="fa fa-users margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Derechos humanos (3) </span></label>
          </div>
          <div class="checkbox">
            <input id="box3" type="checkbox" />
            <label for="box3"><span class="text-checkbox"><i class="fa fa-paint-brush margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Arte y cultura (2)</span></label>
          </div>
          <div class="checkbox">
            <input id="box4" type="checkbox" />
            <label for="box4"><span class="text-checkbox"><i class="fa fa-graduation-cap margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Educación (2)</span></label>
          </div>
          <div class="checkbox">
            <input id="box5" type="checkbox" />
            <label for="box5"><span class="text-checkbox"><i class="fa fa-heartbeat margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>
            Salud y nutrición  (1)</span></label>
          </div>
          <div class="checkbox">
            <input id="box6" type="checkbox" />
            <label for="box6"><span class="text-checkbox"><i class="fa fa-pagelines margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Agricultura (4)</span></label>
          </div>
          <div class="checkbox">
            <input id="box7" type="checkbox" />
            <label for="box7"><span class="text-checkbox"><i class="fa fa-male margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Tercera edad (5)</span></label>
          </div>
          <div class="checkbox">
            <input id="box8" type="checkbox" />
            <label for="box8"><span class="text-checkbox"><i class="fa fa-exclamation-circle margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Contingencias (2)</span></label>
          </div>
          <div class="checkbox">
            <input id="box9" type="checkbox" />
            <label for="box9"><span class="text-checkbox"><i class="fa fa-building margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>
            Construcción (2)</span></label>
          </div>
          <div class="checkbox">
            <input id="box10" type="checkbox" />
            <label for="box10"><span class="text-checkbox"><i class="fa fa-leaf margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Medio ambiente (4)</span></label>
          </div>
          <div class="checkbox">
            <input id="box11" type="checkbox" />
            <label for="box11"><span class="text-checkbox"><i class="fa fa-clone margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Otros (3)</span></label>
          </div>
        </div>
      </div>
      Escritorio -->

    </div>
  </div>
</div>


<!-- Cuarta seccion Areas laborales-->
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12 text-gray-3">
      <h2 class="text-gray-2 text-gray-general">Selecciona &Aacute;reas Laborales</h2>
    </div>

    <div class="col-sm-12">
    [_areas_laborales_main.php]
    </div>
  </div>
</div>


<!-- Quinta seccion Proyectos-->
<div class="container-fluid contenedor-proyectos-destacados-inicio">
  <div class="row">
    <div class="col-sm-12 text-gray-3">
      <h2 class="text-gray-2 text-gray-general ">Proyectos destacados</h2>
    </div>
    <div class="col-sm-10 col-sm-offset-1">
      [_proyectos_main.php]
    </div>
  </div>
</div>


<!--Sexta seccion Testimonios-->
<div class="container-fluid">
  <div class="row row-testimonios">
    <div class="col-sm-12 text-gray-3">
      <h2 class="text-gray-2 text-gray-general">Testimonios</h2>
    </div>

    <div class="col-sm-12">
        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">
            [_testimonios_main.php]
          </div>
          <!-- Add Pagination -->
          <div class="swiper-pagination"></div>
          <!-- Add Arrows -->
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
        </div>
    </div>

  </div>
</div>

<style>
    .swiper-container {
        width: 100%;
        height: 100%;
    }
    .swiper-slide {
        text-align: center;
        font-size: 16px;
        background: transparent;
        /* Center slide text vertically */
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }
    </style>

    <script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 30
    });
    </script>

<!-- Septima seccion suscribirme-->
<div class="container-fluid recibe-noticias-backgroud" id="subscribir">
    <div class="container">
        [newsLetter.php]
    </div>
</div>

<!-- Octova seccion Noticias-->
<div class="container-fluid">
  <div class="row row-btn contenedor-noticias-inicio">
    <div class="col-sm-12 text-gray-3">
      <h2 class="text-gray-2 text-gray-general">Noticias</h2>
    </div>

    <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
      [foreach:blog]
      <div class="col-sm-4">
        <a href="[link]">
          <div class="my-2 mx-auto p-relative bg-white shadow-1 blue-hover size-car-1">
            <img src="[foto]" alt="Man with backpack" class="d-block w-full">
            <div class="px-2 py-2">
              <h1 class="ff-serif font-weight-normal text-black-new22 card-heading mt-0 mb-1 spacing" style="line-height: 1.25;">
              [titulo]
              </h1>
              <p class="mb-1 mb-6-color">[user]</p>
              <p class="mb-1 mb-5-color text-posi-noticia-index">
                [texto]
              </p>
            </div>
            <a class="text-uppercase d-inline-block font-weight-medium lts-2px ml-2 mb-2 text-center styled-link">Ver más
            </a>
          </div>
        </a>
      </div>
      [/foreach]
    </div>

    <div class="col-xs-12 col-sm-4 col-sm-offset-4">
      <a href="blog.html" title="Ver mas noticias"> <p class="text-area-labo">Ver más noticias</p></a>
    </div>

  </div>
</div>

<!-- Contacto Footer -->
<div class="container-fluid" id="contacto">
  [contactoForm.php]
</div>
<!-- Footer-->
<footer>
  [footer.php]
</footer>

<script>
$(function() {
  $('a[href*=#]').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
  });
});
</script>
