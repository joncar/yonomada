<?php
$files = scandir('application/modules/paginas/views');
if($this->router->fetch_method()!='editor'){
	foreach($files as $f):
		if(strstr($f,'.php') && $f!=='theme.php' && $f!=='read.php'):
	    	$page = str_replace('['.$f.']',$this->load->view($f,array(),TRUE,'paginas'),$page); 
		endif;
	endforeach;
}
$page = str_replace('[menu]',$this->load->view('includes/template/menu',array(),TRUE),$page); 
$page = str_replace('[mapamovil]',$this->load->view('_mapamovil',array(),TRUE),$page); 
$this->db->order_by('orden','ASC');
$galeria = $this->db->get('galeria');
foreach($galeria->result() as $n=>$g){
	$galeria->row($n)->foto = base_url('img/galeria/'.$g->foto);
	if(strpos($g->titulo,'youtube')>-1 || strpos($g->titulo,'vimeo')>-1){
		$galeria->row($n)->enlace = $galeria->row($n)->titulo;
		$galeria->row($n)->titulo = 'Video de youtube';
	}else{
		$galeria->row($n)->enlace = $galeria->row($n)->foto;
	}
}
$this->db->limit(3);
$this->db->order_by('fecha','ASC');
$this->db->where('mostrar_en_inicio',1);
$blog = $this->db->get('blog');
foreach($blog->result() as $n=>$g){
	$blog->row($n)->foto = base_url('img/blog/'.$g->foto);
	$blog->row($n)->texto = substr(strip_tags($g->texto),0,100).'...';
	$blog->row($n)->link = site_url('blog/'.toUrl($g->id.'-'.$g->titulo));
}

$cruds = array(
	'galeria'=>$galeria,
	'blog'=>$blog,
	'areas_laborales'=>$this->db->get('areas_laborales'),
	'universidades'=>$this->db->get('universidades'),
	'departamentos'=>$this->db->get('departamentos'),
	'semestres'=>$this->db->get('semestres'),
	'destinos'=>$this->db->get('destinos'),
);
$page = $this->querys->fillFields($page,$cruds);

if(!empty($_SESSION['msj'])){
	$page = str_replace('[msj]',$_SESSION['msj'],$page);
	unset($_SESSION['msj']);
}else{
	$page = str_replace('[msj]','',$page);
}

if(!empty($_SESSION['msj2'])){
	$page = str_replace('[msj2]',$_SESSION['msj2'],$page);
	unset($_SESSION['msj2']);
}else{
	$page = str_replace('[msj2]','',$page);
}
$page = str_replace('[base_url]',base_url(),$page);
echo $page;