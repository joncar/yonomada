<?php
	$this->db->select("proyectos.*, GROUP_CONCAT(carreras.nombre SEPARATOR '/') as area");
	$this->db->join('proyectos_areas','proyectos_areas.proyectos_id = proyectos.id','LEFT');
	$this->db->join('carreras','carreras.id = proyectos_areas.carreras_id','INNER');
	$this->db->group_by('proyectos.id');
  $this->db->where('destacado',1);
	$proyectos = $this->db->get('proyectos');
	if($proyectos->num_rows()>0):
	$proyecto = $proyectos->row();
?>
<div class="col-sm-5 col-xs-12 video-recientes-inicio">
   <div class="video-container" id="video-proyectos">
       <iframe width="640" height="360" src="<?= $proyecto->video ?>" frameborder="0" allowfullscreen></iframe>
   </div>
</div>

<div class="col-sm-7 col-xs-12">
  <span class="text-black-2 text-black-general link-titulo-inicio">
			<a href="<?= site_url('proyectos/'.toUrl($proyecto->id.'-'.$proyecto->titulo)) ?>"><?= $proyecto->titulo ?></a>
	</span></br>
  <span class="text-gray-4 text-gray-general"><?= substr($proyecto->descripcion_corta,0,40) ?></span></br>
  <div id="icon-padding">
     <div class="icon-padding">
        <i class="fa fa-map-marker fa-lg icon-color icon-center" aria-hidden="true"></i>
        <span class="text-gray-4 text-gray-general"><?= $proyecto->ubicacion ?></span>
     </div>
     <div class="icon-padding">
        <i class="fa fa-suitcase fa-lg icon-color icon-center" aria-hidden="true"></i>
        <span class="text-gray-4 text-gray-general">
        	<?= $proyecto->area ?>
    	</span>
     </div>
     <div class="icon-padding">
        <i class="fa fa-key fa-lg icon-color icon-center" aria-hidden="true"></i>
        <span class="text-gray-4 text-gray-general">
        	<?= $proyecto->tags ?>
    	</span>
     </div>
     <span class="text-gray-5 text-gray-general"><small><b>¡Explora más proyectos!</b></small></span>
  </div>
  <?php
    $this->db->select("proyectos.*, GROUP_CONCAT(carreras.nombre SEPARATOR '/') as area");
    $this->db->join('proyectos_areas','proyectos_areas.proyectos_id = proyectos.id','LEFT');
    $this->db->join('carreras','carreras.id = proyectos_areas.carreras_id','INNER');
    $this->db->group_by('proyectos.id');

    $proyectos = $this->db->get('proyectos');
  ?>
  <?php if($proyectos->num_rows()>0): ?>
	  <!-- Slider -->
	  <div class="owl-carousel">
	     <?php foreach($proyectos->result() as $n=>$p): ?>
		     <div class="container-slider item item-slider-proyectos-inicio">
		        <img src="<?= base_url('img/proyectos/'.$p->banner) ?>" alt="Avatar" class="image" style="width:100%">
		        <div class="middle">
		          <div class="text-slider">
		              <?= $p->titulo ?><br><br>
		              <a href="<?= site_url('proyectos/'.toUrl($p->id.'-'.$p->titulo)) ?>"><btn class="btn-slider-proyecto">Ver Proyecto</btn></a>
		          </div>
		        </div>
		     </div>
	 	<?php endforeach; ?>
	  </div>
  <?php else: ?>
    <span style="color:black">No hay mas proyectos destacados.</span>
  <?php endif ?>
</div>
<?php else: ?>
  En estos momentos no existen proyectos destacados, intente ingresar más tarde
<?php endif ?>
