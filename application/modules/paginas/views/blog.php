<header class="container-fluid header6" id="conteiner-fluid-0">
     <div class="container">
        <!-- Menu -->
        [menu]
     </div>
     <div class="row row-new-2">
        <div class="col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
           <div class="col-sm-12">
              <h1 class="text-center">BLOG</h1>
              <div class="text-center-new1">¡Mantente al día sobre las nuevas noticias!</div>
           </div>
        </div>
     </div>
</header>

<!-- Conteiner-->
<div class="container-fluid">
         [contenido]
</div>

      <!-- Newsletter -->
      <div class="container-fluid">
        [newsletter.php]
      </div>

      <!-- Footer-->
      <footer>
        [footer.php]
      </footer>
