<header class="container-fluid header6" id="conteiner-fluid-0">
    <div class="container">
        <!-- Menu -->
        [menu]
    </div>
   <div class="row row-new-2">
      <div class="col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
         <div class="col-sm-12">
            <h1 class="text-center">BLOG</h1>
            <div class="text-center-new1">[titulo]</div>
         </div>
      </div>
   </div>
</header>
<!-- Conteiner-->
<div class="container-fluid fondo-gris">
      <div class="row contendor-contenido">
         <div class="row row-mapa margin-bottom-blog">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 padding-responsive-blo-in">
                <h2 class="title-menu-2 text-gray">[titulo]</h2>
                <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
            </div>

            <div class="col-xs-12 col-sm-10 col-sm-offset-1 padding-responsive-blo-in margin-bottom-img-nota">
                <div class="col-xs-12 col-sm-6 top-blo-2 ">
                   <div class="cfotoblogdetail center-block" style="background-image:url([foto]);"></div>
                </div>
                <div class="col-xs-12 col-sm-6 top-blo-2 ">
                  <p class="texto-fecha-noticia"><i class="fa fa-calendar"></i> [fecha]</p>
                  <p class="text-gray-general text-justify">[texto]</p>
                </div>
            </div>

            <!--
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 autorrec row-blog-inter-2 contenedor-autor-blog padding0">
                  <div class="col-xs-12 col-sm-2 img-autor-blog">
                      <img src="<?= base_url() ?>img/arquitectura.jpg" alt="Editor Yo nómada" class="img-responsive center-block">
                  </div>
                  <div class="col-xs-12 col-sm-10 padding-top-autor-new">
                    <p class="tex-blog-1">[user]</p>
                    <p class="tex-blog-2">Naturaleza + Servicio Social</p>
                    <p class="text-gray-general"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam iaculis maximus ligula. Nullam facilisis massa eget est porta facilisis.
                    </p>
                  </div>
            </div>-->

         </div>

         <div class="row">
           <div class="col-xs-12 col-sm-6 col-sm-offset-3">
              <a href="http://bluepixel.mx/yonomada/blog.html">
                 <button type="button" class="btn btn-default btn-regresar-blog"><span class="boton-space-2">REGRESAR AL BLOG</span></button>
              </a>
           </div>
         </div>

      </div>



  </div>

      <!-- Contacto Footer -->
      <div class="container-fluid" id="contacto">
        [contactoForm.php]
      </div>

      <!-- Footer-->
      <footer>
        [footer.php]
      </footer>
