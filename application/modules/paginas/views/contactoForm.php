<!-- <div class="row padding-top20">
  <div class="col-xs-12 col-sm-12 formulario-backgroud">
  <div class="col-xs-0 col-sm-1"></div>
     <div class="col-xs-12 col-sm-5 col-top-2">
        <form method="post" action="<?= base_url('paginas/frontend/contacto') ?>">
           <div class="form-group">
              <input type="text" name="nombre" class="form-control margin-formulario formulario-opacity" id="exampleFormControlInput1" placeholder="Nombre">
              <input type="email" name="email" class="form-control margin-formulario formulario-opacity" id="exampleFormControlInput1" placeholder="Correo">
              <input type="text" name="telefono" class="form-control margin-formulario formulario-opacity" id="exampleFormControlInput1" placeholder="Teléfono">
           </div>
           <div class="form-group">
              <textarea name="message" class="form-control margin-formulario formulario-opacity altura-mensaje-form" id="exampleFormControlTextarea1" rows="3" placeholder="Mensaje"></textarea>
           </div>
          <div class="col-sm-4"></div>
          <div class="col-sm-4">[msj]</div>
          <div class="col-sm-4 px-0 ">
             <button type="submit" class="btn btn-default btn-100 bottom-transparent general-hover">Enviar</button>
          </div>
        </form>
     </div>
     <div class="col-xs-0 col-sm-1 hidden-xs">
        <div class="line-white">

        </div>
     </div>
     <div class="col-xs-12 col-sm-5 col-top-2 col-margin-left">
        <div class="span-conteiner-form">
           <span class="form-title">¿TIENES ALGUNA DUDA?</span></br>
        </div>
        <div class="span-conteiner-form">
           <span class="font-lighter font-size-form">¡DÉJANOS UN MENSAJE Y NOS PONDREMOS EN CONTACTO CONTIGO!</span>
        </div>
        <div class="span-conteiner-form">
           <i class="fa fa-phone fa-lg icon-center-2" aria-hidden="true"></i><span class="font-lighter">55-55-55-55</span>
        </div>
        <div class="span-conteiner-form">
           <i class="fa fa-whatsapp fa-lg icon-center-2" aria-hidden="true"></i><span class="font-lighter">+ 521 55-55-55-55</span>
        </div>
        <div class="span-conteiner-form">
           <i class="fa fa-envelope fa-lg icon-center-2" aria-hidden="true"></i><span class="font-lighter">hola@yonomada.com</span>
        </div>
        <div class="span-conteiner-form">
           <i class="fa fa-map-marker fa-lg icon-center-2" aria-hidden="true"></i><span class="font-lighter">Dirección de la empresa</span>
        </div>
        <div class="col-xs-12 col-sm-5 icon-dudas">
           <span class="fa-stack fa-1x">
           <i class="fa fa-circle fa-stack-2x color-icon-header2"></i>
           <i class="cursor-pointer1 fa fa-instagram fa-stack-1x fa-inverse color-icon-dudas color-icon-footter-new general-hover"></i>
           </span>
           <span class="fa-stack fa-1x">
           <i class="fa fa-circle fa-stack-2x color-icon-header2"></i>
           <i class="cursor-pointer1 fa fa-facebook fa-stack-1x fa-inverse color-icon-dudas color-icon-footter-new general-hover"></i>
           </span>
           <span class="fa-stack fa-1x">
           <i class="fa fa-circle fa-stack-2x color-icon-header2"></i>
           <i class="cursor-pointer1 fa fa-linkedin fa-stack-1x fa-inverse color-icon-dudas color-icon-footter-new general-hover"></i>
           </span>
        </div>
     </div>
  </div>
  <div class="col-xs-0 col-sm-1"></div>
</div> -->



<section class="container-fluid formulario-backgroud padding0">
<div class="row padding-top20">
  <div class="col-xs-12 col-sm-8 col-sm-offset-2">

     <div class="col-xs-12 col-sm-6 col-top-2">
        <form method="post" action="<?= base_url('paginas/frontend/contacto') ?>">
           <div class="form-group">
              <input type="text" name="nombre" class="form-control margin-formulario formulario-opacity" id="exampleFormControlInput1" placeholder="Nombre">
              <input type="email" name="email" class="form-control margin-formulario formulario-opacity" id="exampleFormControlInput1" placeholder="Correo">
              <input type="text" name="telefono" class="form-control margin-formulario formulario-opacity" id="exampleFormControlInput1" placeholder="Teléfono">
           </div>
           <div class="form-group">
              <textarea name="message" class="form-control margin-formulario formulario-opacity altura-mensaje-form" id="exampleFormControlTextarea1" rows="3" placeholder="Mensaje"></textarea>
           </div>
          <div class="col-sm-4"></div>
          <div class="col-sm-4">[msj]</div>
          <div class="col-sm-4 px-0 ">
             <button type="submit" class="btn btn-default btn-100 bottom-transparent general-hover">Enviar</button>
          </div>
        </form>
     </div>

     <div class="col-xs-12 col-sm-6 col-margin-left texto-footer-contacto">
        <div class="span-conteiner-form">
           <span class="form-title">¿TIENES ALGUNA DUDA?</span></br>
        </div>
        <div class="span-conteiner-form">
           <span class="font-lighter font-size-form">¡DÉJANOS UN MENSAJE Y NOS PONDREMOS EN CONTACTO CONTIGO!</span>
        </div>
        <div class="span-conteiner-form">
           <i class="fa fa-phone fa-lg icon-center-2" aria-hidden="true"></i><span class="font-lighter">55-55-55-55</span>
        </div>
        <div class="span-conteiner-form">
           <i class="fa fa-whatsapp fa-lg icon-center-2" aria-hidden="true"></i><span class="font-lighter">+ 521 55-55-55-55</span>
        </div>
        <div class="span-conteiner-form">
           <i class="fa fa-envelope fa-lg icon-center-2" aria-hidden="true"></i><span class="font-lighter">hola@yonomada.com</span>
        </div>
        <div class="span-conteiner-form">
           <i class="fa fa-map-marker fa-lg icon-center-2" aria-hidden="true"></i><span class="font-lighter">Dirección de la empresa</span>
        </div>
        <div class="col-xs-12 col-sm-5 icon-dudas">
           <span class="fa-stack fa-1x">
           <i class="fa fa-circle fa-stack-2x color-icon-header2"></i>
           <i class="cursor-pointer1 fa fa-instagram fa-stack-1x fa-inverse color-icon-dudas color-icon-footter-new general-hover"></i>
           </span>
           <span class="fa-stack fa-1x">
           <i class="fa fa-circle fa-stack-2x color-icon-header2"></i>
           <i class="cursor-pointer1 fa fa-facebook fa-stack-1x fa-inverse color-icon-dudas color-icon-footter-new general-hover"></i>
           </span>
           <span class="fa-stack fa-1x">
           <i class="fa fa-circle fa-stack-2x color-icon-header2"></i>
           <i class="cursor-pointer1 fa fa-linkedin fa-stack-1x fa-inverse color-icon-dudas color-icon-footter-new general-hover"></i>
           </span>
        </div>
     </div>
  </div>

</div>
</section>
