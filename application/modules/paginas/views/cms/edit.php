<form onsubmit="return save()" action="" method="post">
    <div class="FrameSection" style="min-height:100vh">
            <?= $page ?>
    </div>
</form>
<script src="http://code.jquery.com/jquery-1.10.0.js"></script>     
<script src="<?= base_url() ?>assets/grocery_crud/texteditor/tiny_mce/tinymce.min.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/texteditor/tiny_mce/jquery.tinymce.js"></script>
<script>
    var SectionSelector = ".container-fluid";   
    var editableelements = '.editablesection,h1,h2,h3,h4,h5,h6,p,a,span';
    function save(){        
        $(SectionSelector).find('a').removeAttr('data-mce-href');
        $(SectionSelector).find('img').removeAttr('data-mce-href');
        $(SectionSelector).find('img').removeAttr('data-mce-src');
        $(SectionSelector).find('br').removeAttr('data-mce-bogus');
        $("body").find('input[type="hidden"]').remove();    
        $(editableelements+',img').removeAttr('data-mce-style');
        $(editableelements+',img').removeAttr('data-mce-src');
        $(editableelements+',img').removeClass('mce-content-body');      
        $(SectionSelector).removeAttr('style');
        $(editableelements).removeAttr('style');
        $(editableelements).removeAttr('contenteditable');
        $(editableelements).removeAttr('spellcheck');        
        $(".addSection").parent().remove();
        $(".rmvSection").remove();
        $(".rgtSection").remove();
        $(".lftSection").remove();
        $(".save").remove();
        update($(".FrameSection").html());        
        
        /**/
    }
    
    function update(d){        
        data = {data:d, name:'<?= $name ?>'};
        $.post('<?= base_url('paginas/admin/paginas/edit/'.$name) ?>',data,function(data){
            alert('Guardado');
            initFrameWork();
        });
    }    
    
    $(document).on('click','a.editablesection, .editablesection a',function(e){
        e.preventDefault();
    });

    $(document).on('click','a',function(e){
        e.preventDefault();
    })

    $(document).on('ready',function(){        
        initFrameWork();
    });

    $(document).on('click','.closeModal',function(){$("#modal").hide();})

    $(document).on('click','.addSection',function(){
        window.open('<?= base_url() ?>theme/components.html',"",'width=300,height=300');
    });
    $(document).on('click','.rmvSection',function(){
        if(confirm("Seguro que deseas eliminar esta seccion")){
            $(this).parents(SectionSelector).remove();
        }
    });

    $(document).on('click','.lftSection',function(){
        var container = $(this).parents(SectionSelector);
        var cont = container.clone();
        container.prev().before(cont);
        container.remove();        
    });

    $(document).on('click','.rgtSection',function(){
        var container = $(this).parents(SectionSelector);
        var cont = container.clone();
        container.next().after(cont);
        container.remove();
    });

    $(document).on('click','.save',function(){
        save();
    });

    function initFrameWork(){

        $(SectionSelector).css({'position':'relative'});
        //btnREmove
        $(SectionSelector).append('<a href="#" style="position:absolute; top:20px; left:20px;background: black;color: white;padding: 5px;border-radius: 1em;text-decoration: none;" class="rmvSection">Del</a>');
        //btnLeft
        $(SectionSelector).append('<a href="#" style="position:absolute; top:20px; left:60px;background: black;color: white;padding: 5px;border-radius: 1em;text-decoration: none;" class="lftSection"><</a>');
        //btnRight
        $(SectionSelector).append('<a href="#" style="position:absolute; top:20px; left:80px;background: black;color: white;padding: 5px;border-radius: 1em;text-decoration: none;" class="rgtSection">></a>');
        //Btn AddSection
        $(SectionSelector).last().after('<div style="text-align:center;"><button type="button" class="btn btn-success addSection">Añadir Seccion</button></div>');
        $(SectionSelector).last().after('<div style="text-align:center;"><button type="button" class="btn btn-success save">Guardar cambios</button></div>');
        tinymce.init({
            selector: editableelements,
            theme:'modern',
            image_advtab: true, 
            toolbar1: 'undo redo | formatselect fontselect fontsizeselect | bold italic underline forecolor backcolor | alignleft aligncenter alignright alignjustify | link searchreplace spellchecker | table image filemanager | fullscreen',
            plugins: "link submit code spellchecker searchreplace, table, image, imagetools, textcolor, filemanager, fullscreen",
            images_upload_base_path: '/img/uploads',
            valid_elements : "a[href|target=_blank],strong/b,div[align],br,form,span",
            menu:{
                file: {title: 'Archivo', items: 'submit'},
                format: {title: 'Formato', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
                view: {title: 'Ver', items: 'visualblocks'},
                table: {title: 'Tabla', items: 'inserttable tableprops deletetable | cell row column'},
                tools:{title:'Herramientas',items:'code spellchecker spellcheckerlanguage'}
            },
            allow_conditional_comments: true,
            allow_unsafe_link_target: true,
            relative_urls: true,
            convert_urls: false,
            cleanup_on_startup: false,
            trim_span_elements: false,
            verify_html: false,
            cleanup: false,
            inline: true,
            force_hex_style_colors:false,
            force_br_newlines:false,
            force_p_newlines:false,
            forced_root_block : ""
        });           
    }

    function addSection(obj){
        $(".addSection").parent().remove();
        $(".rmvSection").remove();
        $(".lftSection").remove();
        $(".rgtSection").remove();
        $(obj).find('img').attr('src',"<?= base_url() ?>/img/logo.png");        
        $(SectionSelector).last().after(obj);
        init();
    }
</script>
    
