<header class="container-fluid header4" id="conteiner-fluid-0">
     <div class="container">
        <!-- Menu -->
        [menu]
     </div>
     <div class="row row-new-2">
        <div class="col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
           <h1 class="text-center">¡ENCUENTRA TU PRÓXIMA AVENTURA!</h1>
        </div>
     </div>
</header>

<div class="container-fluid fondo-gris">
   <div class="row contendor-contenido">
     [output]
   </div>
</div>

<!-- Contacto Footer -->
<div class="container-fluid" id="contacto">
  [contactoForm.php]
</div>

<!-- Footer-->
<footer>
  [footer.php]
</footer>
