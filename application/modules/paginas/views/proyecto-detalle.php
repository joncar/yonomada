

<header class="container-fluid header2" id="conteiner-fluid-0" style="background:url([banner]); background-size: cover; background-position: center bottom; background-repeat: no-repeat;">
         <div class="container">
           <!-- Menu -->
           [menu]
         </div>
         <div class="row row-new-2">
            <div class="col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
               <div class="col-sm-12">
                  <h1 class="text-center">[titulo]</h1>
               </div>
            </div>
         </div>

         <div class="row row-new-2">
            <div class="col-xs-10 col-xs-offset-1 col col-sm-10 col-sm-offset-1">
               [tags]
            </div>
         </div>
         </div>
</header>


<div class="container-fluid fondo-gris">
  <div class="row contendor-contenido">
         <div class="row">
            <div class="col-xs-12 col-sm-12">
               <div class="tabset">
                  <!-- Tab 1 -->
                  <input type="radio" name="tabset" id="tab1" aria-controls="marzen" checked>
                  <label for="tab1" class="font-tab-black"><i class="fa fa-child width-icon-tab menu-proyectos" aria-hidden="true"></i>Experiencia</label>
                  <!-- Tab 2 -->
                  <input type="radio" name="tabset" id="tab2" aria-controls="rauchbier">
                  <label for="tab2" class="font-tab-black" [tabvacantes]><i class="fa fa-suitcase width-icon-tab menu-proyectos" aria-hidden="true"></i>&nbsp Vacantes</label>
                  <!-- Tab 3 -->
                  <!--<input type="radio" name="tabset" id="tab3" aria-controls="dunkles">
                  <label for="tab3" class="font-tab-black"><i class="fa fa-quote-left width-icon-tab menu-proyectos" aria-hidden="true"></i>
                  Testimonios</label>-->
                  <input type="radio" name="tabset" id="tab4" aria-controls="dunkles">
                  <label for="tab4" class="font-tab-black"><i class="fa fa-map-marker width-icon-tab menu-proyectos" aria-hidden="true"></i>Ubicación</label>
                  <div class="tab-panels col-sm-12 shadow-box" style="padding: 0px; ">

                     <section id="marzen" class="tab-panel col-sm-12 padding-30-new">

                        <div class="col-xs-12 col-sm-12 contenedor-detalle-proyecto">
                            <div class="col-xs-12 col-sm-8" style=" word-break: break-all; word-wrap: break-word;">
                               <h2 class="title-menu-2 text-gray">[descripcion_corta]</h2>
                               <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
                               <div class="col-xs-12 col-sm-12 text-gray-general tex-gray-littel padding-text-new">
                                  [texto]
                               </div>
                            </div>

                            <div class="col-xs-12 col-sm-4">
                               <iframe width="100%" height="300px" src="[video]" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12" style="background-color:#E1E1E1;">
                            <div class="col-xs-12 col-sm-4 col-sm-offset-4 logos-proyecto-detalle">
                                  Avalada por:<br>
                                 [foreach:avalada]
                                 <div class="col-xs-3 col-sm-4">
                                    <img src="[logo]" alt="[nombre]" class="img-responsive center-block margin-t-50">
                                 </div>
                                 [/foreach]
                            </div>
                        </div>

                     </section>

                     <!--VACANTES-->
                     <section id="rauchbier" class="tab-panel col-sm-12 padding-30-new">
                        <div class="col-xs-12 col-sm-12">
                        <h2 class="title-menu-2 text-gray">¡Descubre nuestras vacantes disponibles!</h2>
                        <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
                        </div>
                        <div class="col-sm-12 text-gray-general tex-gray-littel padding-text-new-tarj">

                           [vacantes]

                        </div>
                     </section>
                      <!--TESTIMONIOS-->
                     <!--<section id="dunkles" class="tab-panel col-sm-12">
                        <div class="col-sm-12 top-responsive-tes-tar">
                           <h2 class="title-menu-2 text-gray">Testimonios</h2>
                           <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
                           <div class="row">
                              <div class="col-sm-12 text-gray-3">
                              </div>
                              <div class="col-sm-12">
                                 <div class="col-sm-2"></div>
                                 <div class="col-sm-8 top-slider-responsive-tes">
                                    <!-- Slider -->
                                    <!--<div class="swiper-container">
                                       <div class="swiper-wrapper ">
                                          <div class="swiper-slide slider-transparent"><img src="<?= base_url() ?>img/testimonios-3.png" class="slider-1"/></div>
                                          <div class="swiper-slide slider-transparent"><img src="<?= base_url() ?>img/testimonios-1.png" class="slider-1"/></div>
                                          <div class="swiper-slide slider-transparent"><img src="<?= base_url() ?>img/testimonios-2.png" class="slider-1"/></div>
                                       </div>
                                       <!-- Add Pagination-->
                                       <!--<div class="swiper-pagination bottom-gray-slider"></div>
                                       <!-- Add Arrows -->
                                       <!--<div class="swiper-button-next bottom-gray-slider"></div>
                                       <div class="swiper-button-prev bottom-gray-slider"></div>
                                    </div>
                                 </div>
                                 <div class="col-sm-2"></div>
                              </div>
                              <div class="col-sm-12 centar-columna">
                                 <span class="text-black-general text-black-bold ">NOMBRE DEL PROYECTO</span>
                                 <div class="col-sm-12 font-14px">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-8 padding-tes-1">
                                       <span class="text-black-general">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis viverra faucibus mauris, vitae molestie libero pulvinar a. Donec efficitur lorem quam, ut tempus mauris efficitur nec. Curabitur placerat tristique metus, a varius leo suscipit a. Integer vel volutpat justo.</span></br>
                                       <p class="tex-formato-testi-slider">-Mariana Gómez</p>
                                       <p class="tex-formato-testi-slider ">Universidad ITAM</p>
                                    </div>
                                    <div class="col-sm-2"></div>
                                 </div>
                                 <div class="col-sm-12" style="padding-bottom: 20px;">
                                    <button type="button" class="btn btn-default btn-colo-green general-hover">BUSCAR</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>-->
                      <!--UBICACION-->
                     <section id="new" class="tab-panel col-sm-12 padding-30-new">
                        <div class="col-sm-12">
                           <h2 class="title-menu-2 text-gray">¿Cómo llegar?</h2>
                           <div class="col-sm-1 col-xs-4 line-colo-cyan"></div>
                        </div>
                        <div  class="col-sm-6 text-gray-general tex-gray-littel padding-mapa-2">
                           <!-- Back <div id="mapaubicacion" style="width:100%; height:350px;"></div> -->
                           <div id="mapaubicacion"></div>
                        </div>
                        <div class="col-sm-6 padding-mapa-2">
                           <span class="text-black-2 text-black-general">[nombre_ubicacion]</span></br></br></br>
                           <div id="col-sm-6">
                              <div class="icon-padding">
                                 <i class="fa fa-map-marker fa-lg icon-color icon-center" aria-hidden="true"></i>
                                 <span class="text-gray-4 text-gray-general">ESTADO, CIUDAD</span></br>
                                 <div class="text-gray-4 text-black-new2 margin-text-new2">[estado], [ubicacion]
                                 </div>
                              </div>
                              <div class="icon-padding">
                                 <i class="fa fa-pagelines fa-lg icon-color icon-center" aria-hidden="true"></i>
                                 <span class="text-gray-4 text-gray-general">ACERCA DE ESTE LUGAR</span></br>
                                 <div class="text-gray-4 text-black-new2 margin-text-new3">
                                    [acerca_lugar]
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>

                     <div class="col-sm-12 btn-informes-card padding0">
                        <button id="myBtn" type="button" data-toggle="modal" data-target="#myModal" class="btn btn-default btn-colo-green general-hover">SOLICITAR INFORMES</button>
                        <!-- The Modal -->
                        <div class="container-fluid"></div>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </div>


</div>
</div>


      <!-- Contacto Footer -->
      <div class="container-fluid" id="contacto">
        [contactoForm.php]
      </div>

      <!-- Footer-->
      <footer>
        [footer.php]
      </footer>
      [_solicitar_informacion.php]
