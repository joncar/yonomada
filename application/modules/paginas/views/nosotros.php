
<div class="container-fluid">

     <div class="row row-nosotro1">
         <div class="container">
             <!-- Menu -->
             [menu]
         </div>

         <div class="row row-new-2">
            <div class="col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
               <div class="col-sm-12">
                  <h1 class="text-center">ACERCA DE NOSOTROS</h1>
                  <div class="text-center-new1">¡Renovando el trabajo social!</div>
               </div>
            </div>
         </div>

        <div class="col-xs-12 col-sm-12">
            <!--
           <div class="col-xs-12 col-sm-12">
              <div class="col-xs-0 col-sm-1"></div>
               <div class="col-xs-12 col-sm-4">
                  <img src="<?= base_url() ?>img/logo_nomada_blue.png" alt="Administrac&iacute;on" class="img-responsive centerblock padding-img-laborales">
               </div>
           </div>-->

           <!--
           <div class="col-xs-12 col-sm-5 top-180-nosotros">
              <h2 class="title-blue-n">Valores</h2>
              <p class="line-h-n">Yo Nómada es una organización que acerca a estudiantes y profesioniestas con quienes más necesitan de su experiencia y talento. Vive el trabajo social de forma diferente.</p>
           </div>-->

        </div>
     </div>
</div>

<div class="container-fluid" style="padding-bottom:50px;">
    <div class="container">
       <div class="row">
          <div class="col-xs-12 col-sm-12">
                <h2 class="title-blue-n">Nuestro Objetivo</h2>
                <p class="text-gray-general line-h-n">Yo Nómada es una organización que acerca a estudiantes y
                  profesioniestas con quienes más necesitan de su experiencia y talento. Vive el trabajo social de forma diferente.</p>
          </div>
       </div>

       <div class="row">
          <div class="col-xs-12 col-sm-12">
                <h2 class="title-blue-n">Valores</h2>
                <p class="text-gray-general line-h-n">Yo Nómada es una organización que acerca a estudiantes y profesioniestas con quienes más necesitan
                  de su experiencia y talento. Vive el trabajo social de forma diferente.</p>
          </div>
       </div>

       <div class="row">
         <div class="col-xs-12 col-sm-12">
            <h2 class="title-blue-n">Áreas Laborales</h2>
            <p class="text-gray-general line-h-n">Yo Nómada es una organización que acerca a estudiantes
              y profesioniestas con quienes más necesitan de su experiencia y talento. Vive el trabajo social de forma diferente.</p>
            <div class="ocultar-responsive col-sm-12 text-center">
               <div class="text-gray-general color-area-l-no col-xs-12 col-sm-3"><div class="areas-nosortros">Ciencias<br>sociales</div></div>
               <div class="text-gray-general color-area-l-no col-xs-12 col-sm-3"><div class="areas-nosortros">Ciencias, Matemáticas e Ingenierías</div></div>
               <div class="text-gray-general color-area-l-no col-xs-12 col-sm-3"><div class="areas-nosortros">Artes, Diseño y Humanidades</div></div>
               <div class="text-gray-general color-area-l-no col-xs-12 col-sm-3"><div class="areas-nosortros">Biología, Química y Salud</div></div>
            </div>
         </div>
       </div>

       <div class="row">
           <a href="http://bluepixel.mx/yonomada/areas-laborales.html">
               <div class="col-xs-12 col-sm-4 col-sm-offset-4"><button type="button" class="btn btn-default btn-regresar-blog">Ver más</button></div>
           </a>
      </div>

    </div>
</div>

<div class="container-fluid row-nosotro2">
    <div class="container">
      <div class="row">
          <div class="col-sm-12 text-gray-3">
              <h2 class="title-blue-n">Integrantes</h2>
          </div>
      </div>

      <div class="row">

          <!-- Integrante -->
          <div class="col-lg-3 col-sm-3 text-center">
              <div class="contenedor-fundadores">
                  <img alt="Fundadoras Yo Nómada" src="http://bluepixel.mx/yonomada/img/img-perfil-fundadores-04.jpg" class="img-responsive center-block">
                  <b>Ana Fernanda Islas</b><br>
                  Fundadora y Directora<br>
                  <a class="btn btn-primary btn-twitter btn-sm" href="#"><i class="fa fa-twitter"></i></a>
                  <a class="btn btn-primary btn-sm" rel="publisher" href="#"><i class="fa fa-facebook"></i></a>
              </div>
          </div>

          <!-- Integrante -->
          <div class="col-lg-3 col-sm-3 text-center">
              <div class="contenedor-fundadores">
                  <img alt="Fundadoras Yo Nómada" src="http://bluepixel.mx/yonomada/img/img-perfil-fundadores-03.jpg" class="img-responsive center-block">
                  <b>María de Buen</b><br>
                  Fundadora y Directora Creativa<br>
                  <a class="btn btn-primary btn-twitter btn-sm" href="#"><i class="fa fa-twitter"></i></a>
                  <a class="btn btn-primary btn-sm" rel="publisher" href="#"><i class="fa fa-facebook"></i></a>
              </div>
          </div>

          <!-- Integrante -->
          <div class="col-lg-3 col-sm-3 text-center">
              <div class="contenedor-fundadores">
                  <img alt="Fundadoras Yo Nómada" src="http://bluepixel.mx/yonomada/img/img-perfil-fundadores-01.jpg" class="img-responsive center-block">
                  <b>Nombre</b><br>
                  Cofundador<br>
                  <a class="btn btn-primary btn-twitter btn-sm" href="#"><i class="fa fa-twitter"></i></a>
                  <a class="btn btn-primary btn-sm" rel="publisher" href="#"><i class="fa fa-facebook"></i></a>
              </div>
          </div>

          <!-- Integrante -->
          <div class="col-lg-3 col-sm-3 text-center">
              <div class="contenedor-fundadores">
                  <img alt="Fundadoras Yo Nómada" src="http://bluepixel.mx/yonomada/img/img-perfil-fundadores-02.jpg" class="img-responsive center-block">
                  <b>Nombre</b><br>
                  Cofundador<br>
                  <a class="btn btn-primary btn-twitter btn-sm" href="#"><i class="fa fa-twitter"></i></a>
                  <a class="btn btn-primary btn-sm" rel="publisher" href="#"><i class="fa fa-facebook"></i></a>
              </div>
          </div>

      </div>

    </div>
</div>

      <!--
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-12 col-sm-12" style="padding-bottom: 80px;">
               <div class="col-sm-5 lef-nosotros ">
                  <h2 class="title-menu-2 text-gray">Nuestro equipo</h2>
                  <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
               </div>
               <div class="col-xs-12 col-sm-12 margin-tar-nosotros-1">
                 <div class="col-xs-12 col-sm-3">
                 <figure class="snip1527">
                        <div class="image"><img src="<?= base_url() ?>img/person-1.png" alt="pr-sample23" class="img-big-blog" /></div>
                        <figcaption>

                           <h3>Nombre</h3>
                           <p>
                              You know what we need, Hobbes? We need an attitude. Yeah, you can't be cool if you don't have an attitude.
                           </p>
                        </figcaption>
                        <a href="#"></a>
                     </figure>
                 </div>
                 <div class="col-xs-12 col-sm-3">
                 <figure class="snip1527">
                        <div class="image"><img src="<?= base_url() ?>img/person-2.png" alt="pr-sample23" class="img-big-blog" /></div>
                        <figcaption>

                           <h3>Nombre</h3>
                           <p>
                              You know what we need, Hobbes? We need an attitude. Yeah, you can't be cool if you don't have an attitude.
                           </p>
                        </figcaption>
                        <a href="#"></a>
                     </figure>
                 </div>
                 <div class="col-xs-12 col-sm-3">
                   <figure class="snip1527">
                        <div class="image"><img src="<?= base_url() ?>img/person-3.png" alt="pr-sample23" class="img-big-blog" /></div>
                        <figcaption>

                           <h3>Nombre</h3>
                           <p>
                              You know what we need, Hobbes? We need an attitude. Yeah, you can't be cool if you don't have an attitude.
                           </p>
                        </figcaption>
                        <a href="#"></a>
                     </figure>
                 </div>
                 <div class="col-xs-12 col-sm-3">
                   <figure class="snip1527">
                        <div class="image"><img src="<?= base_url() ?>img/person-4.png" alt="pr-sample23" class="img-big-blog" /></div>
                        <figcaption>

                           <h3>Nombre</h3>
                           <p>
                              You know what we need, Hobbes? We need an attitude. Yeah, you can't be cool if you don't have an attitude.
                           </p>
                        </figcaption>
                        <a href="#"></a>
                     </figure>
                 </div>
               </div>
            </div>
         </div>
      </div> -->


      <!-- Contacto Footer -->
      <div class="container-fluid" id="contacto">
        [contactoForm.php]
      </div>

      <!-- Footer-->
      <footer>
        [footer.php]
      </footer>
