<div id="myModal" class="modal">
<form action="" onsubmit="return sendSolicitud(this)">
 <!-- Modal content -->
 <div class="modal-content row-modal col-sm-8 col-sm-offset-2">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="material-icons">clear</i></button>
    </div>

    <div class="titulo-modal-informes">
        <p class="p-blacl-title-modal">Solicitar informes</p>
        <p class="p-text-subtitle"><small>Llena el siguiente formulario con tus datos y te enviaremos información a tu correo</small></p>
    </div>

    <div class="col-sm-12">

       <div class="col-sm-6">
          <input id="input1" type="text" class="input-style-new-modal" placeholder="NOMBRE" name="nombre" />
          <input id="input1" type="email" class="input-style-new-modal" placeholder="CORREO" name="correo" />

          <span style="color:#000000">Estado</span>
          <div class="custom-select color-white-select" style="width:100%;margin-bottom: 23px;">
             <?= form_dropdown_from_query('estados_id','estados','id','nombre'); ?>
          </div>

          <span style="color:#000000">Área</span>
          <div class="custom-select color-white-select" style="width:100%;margin-bottom: 23px;">
             <?= form_dropdown_from_query('areas_laborales_id','areas_laborales','id','nombre'); ?>
          </div>

          <span style="color:#000000">Carrera a fin</span>
          <div class="custom-select color-white-select" style="width:100%;margin-bottom: 23px;">
             <?= form_dropdown_from_query('carreras_id','carreras','id','nombre'); ?>
          </div>
       </div>

       <div class="col-sm-6">
          <input id="input1" type="text" class="input-style-new-modal" placeholder="APELLIDO" name="apellido" />
          <input id="input1" type="text" class="input-style-new-modal" placeholder="TELÉFONO" name="telefono"/>

          <span style="color:#000000">Universidad</span>
          <div class="custom-select color-white-select" style="width:100%;margin-bottom: 23px;">
             <?= form_dropdown_from_query('universidades_id','universidades','id','nombre'); ?>
          </div>

          <span style="color:#000000">Departamento</span>
          <div class="custom-select color-white-select" style="width:100%;margin-bottom: 23px;">
             <?= form_dropdown_from_query('departamentos_id','departamentos','id','nombre'); ?>
          </div>

          <span style="color:#000000">Semestre</span>
          <div class="custom-select color-white-select" style="width:100%;margin-bottom: 23px;">
             <?= form_dropdown_from_query('semestres_id','semestres','id','nombre'); ?>
          </div>
       </div>


       <p class="text-black-filtros font-check-title-new">VACANTES QUE ME INTERESAN</p>
       <div class="col-sm-12" style="margin-left: -29px;">
          <div class="col-sm-6">
          	<?php
	          	$this->db->select('proyectos_vacantes_detalles.*, carreras.nombre as categoria');
              $this->db->join('proyectos_vacantes','proyectos_vacantes.id = proyectos_vacantes_detalles.proyectos_vacantes_id');
				      $this->db->join('carreras','carreras.id = proyectos_vacantes_detalles.carreras_id');
				foreach($this->db->get_where('proyectos_vacantes_detalles',array('proyectos_id'=>$detail->id))->result() as $v):
			?>
	             <div class="checkbox">
	                <input id="box<?= $v->id ?>" type="checkbox" value="<?= $v->titulo ?>" name="vacante[]" />
	                <label for="box<?= $v->id ?>" class="pruebaa222"><span class="text-checkbox"><?= $v->titulo ?></span>
	                </label>
	             </div>
         	<?php endforeach ?>
          </div>
       </div>

           <p class="text-black-filtros font-check-title-new">DURACIÓN DE MI ESTANCIA (Días)</p>
           <div class="rangeslider-wrap">
              <input type="range" min="1" max="100" labels="" name="duracion">
           </div>
           <input type="hidden" name="proyectos_id" value="<?= $detail->id ?>">
           <div id="response"></div>


       <button id="myBtn" type="submit" class="btn btn-default btn-colo-green general-hover" style="margin-bottom: 50px;">ENVIAR</button>
    </div>

 </div>
 </form>
</div>
<script>
	function sendSolicitud(form){
		form = new FormData(form);
		remoteConnection('proyectos/frontend/solicitudes/insert_validation',form,function(data){
			data = data.replace('<textarea>','');
			data = data.replace('</textarea>','');
			data = JSON.parse(data);
			if(data.success){
				remoteConnection('proyectos/frontend/solicitudes/insert',form,function(data){
					data = data.replace('<textarea>','');
					data = data.replace('</textarea>','');
					data = JSON.parse(data);
					if(data.success){
						$("#response").html("Su solicitud ha sido enviada con éxito").addClass('alert alert-success').removeClass('alert-danger');
					}else{
						$("#response").html(data.error_message).addClass('alert alert-danger').removeClass('alert-success');
					}
				});
			}else{
				$("#response").html(data.error_message).addClass('alert alert-danger').removeClass('alert-success');
			}

		});
		return false;
	}
</script>
