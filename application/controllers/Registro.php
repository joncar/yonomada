<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Registro extends Main {
        const IDROLUSER = 2;
        public function __construct()
        {
            parent::__construct();         
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
        }
        
        public function loadView($param = array('view'=>'main'))
        {
                if(!empty($param->output)){
                    $panel = 'panel';
                    $param->view = empty($param->view)?$panel:$param->view;
                    $param->crud = empty($param->crud)?'user':$param->crud;
                    $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                }
                if(is_string($param)){
                    $param = array('view'=>$param);
                }
                $template = 'templateadmin';
                $this->load->view($template,$param);
        }

        public function index($url = 'main',$page = 0)
        {
            if($this->router->fetch_class()=='registro'){
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('bootstrap2');
                $crud->set_table('user');
                $crud->set_subject('<span style="font-size:18px">Nou Usuari</span>');
                //Fields

                //unsets
                $crud->unset_back_to_list()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_edit()
                     ->unset_list()
                     ->unset_export()
                     ->unset_print()
                     ->field_type('fecha_registro','invisible')
                     ->field_type('fecha_actualizacion','invisible')
                     ->field_type('status','invisible')
                     ->field_type('foto','invisible')
                     ->field_type('admin','invisible')
                     ->field_type('password','password')
                     ->field_type('cedula','invisible')
                     ->field_type('apellido_materno','invisible')
                     ->field_type('tiene_android','hidden',1);
                $crud->set_lang_string('insert_success_message','Les seves dades han estat guardats amb èxit <script>setTimeout(function(){document.location.href="'.base_url('panel').'"; },2000)</script>');
                $crud->display_as('password','Contrasenya nou usuari')                 
                     ->display_as('email','Email de contacte')                 ;
                $crud->set_lang_string('form_add','');
                $crud->required_fields('password','email','nombre','apellido','centro','nro_grupo','curso');
                //Displays             
                $crud->set_lang_string('form_save','REGISTRAR');

                $crud->callback_before_insert(array($this,'binsertion'));
                $crud->callback_after_insert(array($this,'ainsertion'));
                $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
                $output = $crud->render();
                $output->view = 'registro';
                $output->crud = 'user';
                $output->title = 'REGISTRAR';
                $output->output = $output->output;
                $output->scripts = get_header_crud($output->css_files,$output->js_files,TRUE);
                $this->loadView($output);   
            }
        }              
        
        function conectar()
        {
            $this->loadView('predesign/login');
        }
        /* Callbacks */
        function binsertion($post)
        {            
            $post['status'] = 1;
            $post['admin'] = 0;
            $post['fecha_registro'] = date("Y-m-d H:i:s");
            $post['password'] = md5($post['password']);
            return $post;
        }
        
        function ainsertion($post,$primary)
        {              
            //Asignar rol
            get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::IDROLUSER));            
            get_instance()->user->login_short($primary);            
            return true;
        }
        
       
        function forget($key = '',$ajax = '')
        {
            if(empty($_POST) && empty($key)){
                $this->loadView(array('view'=>'forget'));
            }
            else
            {
                if(empty($key)){
                if(empty($_SESSION['key'])){
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    if($this->form_validation->run())
                    {
                        $user = $this->db->get_where('user',array('email'=>$this->input->post('email')));
                        if($user->num_rows()>0){
                            $_SESSION['key'] = md5(rand(0,2048));
                            $_SESSION['email'] = $this->input->post('email');
                            correo($this->input->post('email'),'Reseteo de contraseña',$this->load->view('email/forget',array('user'=>$user->row()),TRUE));
                            if(empty($ajax)){
                                $_SESSION['msj'] = $this->success('Los pasos para la restauración han sido enviados a su correo');
                                header("Location:".base_url('panel'));
                            }else{
                                echo $this->success('Los pasos para la restauración han sido enviados a su correo');
                            }
                        }
                        else{
                            if(empty($ajax)){
                                $_SESSION['msj'] = $this->error('El correo recibido no esta registrado');
                                redirect(base_url('panel'));
                            }else{
                                echo $this->error('El correo recibido no esta registrado');
                            }
                        }
                    }
                    else{
                        $_SESSION['msj'] = $this->error($this->form_validation->error_string());
                        redirect(base_url('panel'));
                    }
                }
                else
                {
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    $this->form_validation->set_rules('pass','Password','required|min_length[8]');
                    $this->form_validation->set_rules('pass2','Password2','required|min_length[8]|matches[pass]');                    
                    if($this->form_validation->run())
                    {                        
                        $this->db->update('user',array('password'=>md5($this->input->post('pass'))),array('email'=>$_SESSION['email']));
                        session_unset();
                        $_SESSION['msj'] = $this->success('Se ha restablecido su contraseña');
                        redirect(base_url('panel'));
                    }
                    else{
                        if(empty($_POST['key'])){
                            if(empty($ajax)){
                                $_SESSION['msj'] = $this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.');
                                redirect(base_url('panel'));
                            }else{
                                echo $this->traduccion->traducir($this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.'),$_SESSION['lang']);
                            }
                            session_unset();
                        }
                        else{
                            $this->loadView(array('view'=>'recover','key'=>$key,'msj'=>$this->error($this->form_validation->error_string())));
                        }
                    }
                }
                }
                else
                {
                    if(!empty($_SESSION['key']) && $key==$_SESSION['key'])
                    {
                        $this->loadView(array('view'=>'recover','key'=>$key));
                    }
                    else{
                        $_SESSION['msj'] = $this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.');
                        redirect(base_url('panel'));
                    }
                }
            }
        }          
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
