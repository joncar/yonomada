DROP VIEW IF EXISTS view_proyectos_front;
create view view_proyectos_front as SELECT
proyectos.*,
carreras.id as carreras_id,
departamentos.id as departamentos_id,
areas_laborales.id as areas_laborales_id,
universidades.id as universidades_id
FROM proyectos
LEFT JOIN proyectos_universidades ON proyectos_universidades.proyectos_id = proyectos.id
LEFT JOIN universidades ON universidades.id = proyectos_universidades.universidades_id
LEFT JOIN proyectos_vacantes ON proyectos_vacantes.proyectos_id = proyectos.id
LEFT JOIN proyectos_vacantes_detalles ON proyectos_vacantes_detalles.proyectos_vacantes_id = proyectos_vacantes.id
LEFT JOIN carreras ON carreras.id = proyectos_vacantes_detalles.carreras_id
LEFT JOIN areas_laborales ON areas_laborales.id = proyectos_vacantes_detalles.areas_laborales_id
LEFT JOIN departamentos ON departamentos.id = proyectos_vacantes_detalles.departamentos_id