<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  
<html> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <title><?= empty($title) ? 'Monalco' : $title ?></title>
  <meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
  <meta name="description" content="<?= empty($keywords) ?'': $description ?>" /> 
  <meta name="description" content="Multipurpose HTML template.">
  <link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>  
  <link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<!-- Styles   -->
	<link href="<?= base_url() ?>css/template/bootstrap.css" rel="stylesheet" />
	<link href="<?= base_url() ?>css/template/main.css" rel="stylesheet" />
	<link href="<?= base_url() ?>css/template/queries.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?= base_url() ?>css/template/swiper.min.css">
	<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.css'>
	<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css'>
	<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
	<!-- Fonts and icons   -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script src='https://code.jquery.com/jquery-1.11.1.min.js'></script>
	<!-- Slider -->
	<script src="<?= base_url() ?>js/template/swiper.min.js"></script>	
	<!-- Script -->
	<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <script type='text/javascript' src='<?= base_url() ?>theme/unitegallery/js/jquery-11.0.min.js'></script>
  <script type='text/javascript' src='<?= base_url() ?>theme/unitegallery/js/unitegallery.min.js'></script>
  <link rel='stylesheet' href='<?= base_url() ?>theme/unitegallery/css/unite-gallery.css' type='text/css' />
  <link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
  <script type='text/javascript' src='<?= base_url() ?>theme/unitegallery/themes/tiles/ug-theme-tiles.js'></script>
  <?php 
  if(!empty($css_files) && !empty($js_files)):
  foreach($css_files as $file): ?>
  <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
  <?php endforeach; ?>

  <?php foreach($js_files as $file): ?>
  <script src="<?= $file ?>"></script>
  <?php endforeach; ?>                
  <?php endif; ?>
    <link href="<?= base_url() ?>js/template/map/jqvmap.css" media="screen" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="<?= base_url() ?>js/template/map/jquery.vmap.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>js/template/map/maps/jquery.vmap.mexico.js" charset="utf-8"></script>
</head>
<body>	 
	<!-- SLIDER -->
	<?php $this->load->view($view); ?>    	
	<?php if($this->router->fetch_method()!='editor'): ?>     
      <!-- Librerias JS -->
      <script src="<?= base_url() ?>js/template/main.js"></script>
      <script src="<?= base_url() ?>js/template/main_2.js"></script>
      <!--<script src="<?= base_url() ?>js/template/slider.js"></script>      -->
      <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.js'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js'></script>
      <script  src="<?= base_url() ?>js/template/index.js"></script>


      <!-- Initialize Swiper -->
      <script>
     
      var swiper = new Swiper('.swiper-container', {
          pagination: '.swiper-pagination',
          slidesPerView: 1,
          centeredSlides: true,
          paginationClickable: true,
          spaceBetween: 30,
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
      });
      </script>

      <!-- Select Header buscador -->
      <script>
      $('#responsive-menu-2-rec select').each(function(){
      var $this = $(this), numberOfOptions = $(this).children('option').length;
      $this.addClass('select-hidden');
      $this.wrap('<div class="select"></div>');
      $this.after('<div class="select-styled"></div>');
      var $styledSelect = $this.next('div.select-styled');
      $styledSelect.text($this.children('option').eq(0).text());
      var $list = $('<ul />', {
          'class': 'select-options'
      }).insertAfter($styledSelect);
      for (var i = 0; i < numberOfOptions; i++) {
          $('<li />', {
              text: $this.children('option').eq(i).text(),
              rel: $this.children('option').eq(i).val()
          }).appendTo($list);
      }
      var $listItems = $list.children('li');
      $styledSelect.click(function(e) {
          e.stopPropagation();
          $('div.select-styled.active').not(this).each(function(){
              $(this).removeClass('active').next('ul.select-options').hide();
          });
          $(this).toggleClass('active').next('ul.select-options').toggle();
      });
      $listItems.click(function(e) {
          e.stopPropagation();
          $styledSelect.text($(this).text()).removeClass('active');
          $this.val($(this).attr('rel'));
          $list.hide();
          //console.log($this.val());
      });
      $(document).click(function() {
          $styledSelect.removeClass('active');
          $list.hide();
      });
      });
      </script>

    

    <script type="text/javascript">
      $(document).ready(function(){
          $("#hide-btn").click(function(){
          $("#hide-btn").text('MOSTRAR FILTROS');
        });
      });

      function remoteConnection(url,data,callback){        
          $.ajax({
              url: '<?= base_url() ?>'+url,
              data: data,
              context: document.body,
              cache: false,
              contentType: false,
              processData: false,
              type: 'POST',
              success:callback
          });
      };
    </script>

    <script>
      var codigos = <?php
          $codigos = $this->db->get('estados');
          $datos = array();
          foreach($codigos->result() as $c){
            $datos[] = $c;
          }

          echo json_encode($datos);
      ?>;

      var divProyectoEstado = '<a href="{link}"><div class="link-mensaje-mapa"><small>Proyecto:</small><br><b>{titulo}</b></div></a>';
      jQuery(document).ready(function () {
        var map = jQuery('#vmap').vectorMap({
          map: 'mx_en',          
          onRegionClick: function (element, code, region) {  
            $("#mapaDesktop").css('visibility','visible');
            $("#proyectosEstado").html('Buscando proyectos');          
            for(var i in codigos){
              if(codigos[i].codigo_mapa===code){
                $("#mapaEstado").html(codigos[i].nombre);
                $.post('<?= base_url('proyectos/frontend/listarJSON/json_list') ?>',{
                  'search_text[]':codigos[i].id,
                  'search_field[]':'estados_id'
                },function(data){
                    data = JSON.parse(data);
                    if(data.length>0){
                      var str = '';
                      for(var i in data){
                        var s = divProyectoEstado.replace('{link}','<?= base_url('proyectos') ?>/'+data[i].Id);
                        s = s.replace('{titulo}',data[i].Titulo);
                        str+= s;
                      }

                      $("#proyectosEstado").html(str);
                    }else{
                      $("#proyectosEstado").html('Proyectos no encontrados, intenta en otro estado');
                    }
                });
              }
            }
          }
        });

        map.setBackgroundColor('transparent');

      });

      
    </script>
  <?php endif ?>
</body>
</html>