<div class="row row-btn titulo-buscador1">
  <div class="col-xs-12 col-sm-12">
     <div class="col-sm-3 col-xs-6" id="ocultar-logo">
        <a href="<?= site_url() ?>"><img src="<?= base_url() ?>img/logo.png" alt="Logo Yo nomada" class="img-responsive center-block"></a>
     </div>
     <div class="col-sm-9 text-right" id="columna-100-p">
        <div class="col-sm-12 col-xs-5" id="icon-menu-responsive">
           <div class="col-sm-5 col-xs-5"></div>
           <div class="col-sm-3 col-xs-12" id="col-responsive">
              <span class="fa-stack fa-1x">
              <i class="fa fa-circle fa-stack-2x color-icon-header2 "></i>
              <i class="cursor-pointer1 fa fa-instagram fa-stack-1x fa-inverse color-icon-header color-icon-header-new general-hover"></i>
              </span>
              <span class="fa-stack fa-1x">
              <i class="fa fa-circle fa-stack-2x color-icon-header2"></i>
              <i class="cursor-pointer1 fa fa-facebook fa-stack-1x fa-inverse color-icon-header color-icon-header-new general-hover"></i>
              </span>
              <span class="fa-stack fa-1x">
              <i class="fa fa-circle fa-stack-2x color-icon-header2"></i>
              <i class="cursor-pointer1 fa fa-linkedin fa-stack-1x fa-inverse color-icon-header color-icon-header-new general-hover"></i>
              </span>
           </div>
           <div class="col-sm-4 col-xs-4" id="col-responsive-search">
              <div id="wrap">
                 <form action="<?= base_url('proyectos') ?>" autocomplete="on">
                    <input id="search" name="titulo" type="text" placeholder="Buscar"><input id="search_submit" value="Rechercher" type="submit">
                 </form>
              </div>
           </div>
        </div>
     </div>
     <div class="col-sm-9 top-columna-menu-2 topnav" id="mostrar-menu-prinicpal">
        <a href="<?= site_url() ?>" class="font-size-menu-ipad general-hover">Inicio</a>
        <a href="<?= site_url('areas-laborales') ?>" class="font-size-menu-ipad general-hover">&Aacute;reas Laborales</a>
        <a href="<?= site_url('proyectos') ?>" class="font-size-menu-ipad general-hover">Proyectos</a>
        <a href="<?= site_url('galeria.html') ?>" class="font-size-menu-ipad general-hover">Galer&iacute;a</a>
        <a href="<?= site_url('blog') ?>" class="font-size-menu-ipad general-hover">Blog</a>
        <a href="<?= site_url('nosotros.html') ?>" class="font-size-menu-ipad general-hover">Nosotros</a>
        <a href="<?= site_url() ?>#contacto" class="font-size-menu-ipad general-hover">Contacto</a>
        <a href="<?= base_url('panel') ?>" class="font-size-menu-ipad general-hover">Ingresar</a>
        <?php if(!empty($_SESSION['user'])): ?>
            <a href="<?= base_url('main/unlog') ?>" class="font-size-menu-ipad general-hover">Salir</a>
        <?php endif ?>
        <a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;</a>
     </div>
     <!-- Menu movil -->
     <div class="ocultar-menu-reponsive-2">
        <div id="sideNavigation" class="sidenav">
           <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
           <a href="<?= site_url() ?>">Inicio</a>
           <a href="<?= site_url('areas-laborales') ?>">&Aacute;reas Laborales</a>
           <a href="<?= site_url('proyectos') ?>">Proyectos</a>
           <a href="<?= site_url('galeria.html') ?>">Galer&iacute;a</a>
           <a href="<?= site_url('blog') ?>">Blog</a>
           <a href="<?= site_url('nosotros.html') ?>">Nosotros</a>
           <a href="<?= site_url() ?>#contacto">Contacto</a>
           <a href="<?= base_url('panel') ?>">Ingresar</a>
          <?php if(!empty($_SESSION['user'])): ?>
              <a href="<?= base_url('main/unlog') ?>">Salir</a>
          <?php endif ?>
           <a class="col-sm-4 col-xs-5 hover-cancel" id="col-responsive-search">
              <div id="wrap">
                 <form action="" autocomplete="on">
                    <input id="search" name="Buscar" type="text" placeholder="Buscar" style="width: 100%;position: absolute;" ><input id="search_submit" value="Rechercher" type="submit" >
                 </form>
              </div>
           </a>
           <div class="col-sm-3 col-xs-12 hover-cancel" id="col-responsive">
              <span class="fa-stack fa-1x">
              <i class="fa fa-circle fa-stack-2x color-icon-header2 hover-icon-hover"></i>
              <i class="cursor-pointer1 fa fa-instagram fa-stack-1x fa-inverse color-icon-header color-icon-header-new general-hover"></i>
              </span>
              <span class="fa-stack fa-1x">
              <i class="fa fa-circle fa-stack-2x color-icon-header2 hover-icon-hover"></i>
              <i class="cursor-pointer1 fa fa-facebook fa-stack-1x fa-inverse color-icon-header-new general-hover"></i>
              </span>
              <span class="fa-stack fa-1x">
              <i class="fa fa-circle fa-stack-2x color-icon-header2 hover-icon-hover"></i>
              <i class="cursor-pointer1 fa fa-linkedin fa-stack-1x fa-inverse color-icon-header-new general-hover"></i>
              </span>
           </div>
        </div>
        <nav class="topnav2">
           <a class="hover-cancel" href="#" onclick="openNav()" style="padding: 0px;">
               <img src="<?= base_url() ?>img/logo.png" alt="Logo Yo nomada" class="menu-responsive-logo-size img-responsive center-block">
              <svg width="25" height="25" id="icoOpen" class="hover-active">
                 <path d="M0,5 30,5" stroke="#ffffff" stroke-width="3"/>
                 <path d="M0,14 30,14" stroke="#ffffff" stroke-width="3"/>
                 <path d="M0,23 30,23" stroke="#ffffff" stroke-width="3"/>
              </svg>
           </a>
        </nav>
     </div>
  </div>
</div>
