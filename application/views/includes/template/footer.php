<!-- FOOTER -->
    <footer id="footer">
        <div class="container">
            <div class="footer">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <div class="f-block1">
                            <a href="./index.html"><img class="img-responsive" src="<?= base_url() ?>img/footer-logo.png" alt=""></a>
                            <ul>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <span class="specific">address</span>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-8">
                                            <p>Hotel Fontainbleu 4332 Lombard Street San Francisco, CA 10023 USA</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <span class="specific">phone</span>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-8">
                                            <p>(415) 899-3456</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <span class="specific">fax</span>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-8">
                                            <p>(415) 899-3457</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-4">
                                            <span class="specific">email</span>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-8">
                                            <a href="mailto:info@fontainbleu.com">info@fontainbleu.com</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <div class="f-block2">
                            <div class="links">
                                <h4 class="title">site links</h4>
                                <ul>
                                    <li><a href="./accommodations.html">Accommodations</a></li>
                                    <li><a href="./dining.html">Dining</a></li>
                                    <li><a href="./spa.html">Spa</a></li>
                                    <li><a href="./activities.html">Activities</a></li>
                                    <li><a href="./blog-grid.html">News</a></li>
                                    <li><a href="./index.html">Careers</a></li>
                                </ul>
                            </div>
                            <div class="tweet">
                                <h4 class="title">latest tweets</h4>
                                <div class="tweet-info">
                                    <div id="tweetcool"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <div class="f-block3">
                            <div class="date">
                                <h4 class="title">stay up to date</h4>
                                <p>Sign up for our newsletter</p>
                                <div class="input-group">
                                    <input type="email" placeholder="EMAIL" class="form-control footer-nl">
                                    <span class="input-group-btn">
                                    <button type="button" class="btn btn-default"><i class="fa fa-envelope"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="social">
                                <h4 class="title">let's socialize</h4>
                                <ul>
                                    <li><a href="https://www.twitter.com/username"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.facebook.com/username"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://www.pinterest.com/username"><i class="fa fa-pinterest"></i></a></li>
                                    <li><a href="https://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="https://www.vimeo.com"><i class="fa fa-vimeo-square"></i></a></li>
                                    <li><a href="https://www.youtube.com"><i class="fa fa-youtube"></i></a></li>
                                </ul>
                            </div>
                            <div class="enjoy">
                                <h4 class="title">did you enjoy your stay?</h4>
                                <p>Please tell us about it on <img src="<?= base_url() ?>img/footer-img1.png" alt=""></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                        <div class="f-block4">
                            <h4 class="title">contact us</h4>
                            <div class="form">
                                <form id="contactForm" action="php/contact.php" method="post">
                                    <div class="form-group">
                                        <input name="senderName" id="senderName" type="text" class="form-control" placeholder="NAME" required="required" />
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="senderEmail" id="senderEmail" class="form-control" placeholder="EMAIL ADDRESS" required="required" />
                                    </div>
                                    <div class="form-group">
                                        <textarea name="message" id="message" class="form-control" rows="3" placeholder="MESSAGE"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-default">Submit</button>
                                </form>
                                <div id="sendingMessage" class="statusMessage">
                                    <p>Sending your message. Please wait...</p>
                                </div>
                                <div id="successMessage" class="successmessage">
                                    <p>Thanks for sending your message! We'll get back to you shortly.</p>
                                </div>
                                <div id="failureMessage" class="errormessage">
                                    <p>There was a problem sending your message. Please try again.</p>
                                </div>
                                <div id="incompleteMessage" class="statusMessage">
                                    <p>Please complete all the fields in the form before sending.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 cap col-centered center-block">
                <span><img class="img-responsive" src="<?= base_url() ?>img/cap.png" alt=""></span>
            </div>
            <div class="footer-bot">
                <ul>
                    <li><a href="./index.html">Privacy Policy</a></li>
                    <li><a href="./index.html">Legal Notice</a></li>
                    <li><a href="./index.html">Sitemap</a></li>
                </ul>
                <p>&copy; Hotelier Premium  2001-2015. All rights reserved.</p>
            </div>
        </div>
    </footer>