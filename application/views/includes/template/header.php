<header class="container-fluid header" id="conteiner-fluid-0">
        <div class="container">
            <!-- Menu -->
            <?php $this->load->view('includes/template/menu') ?>
        </div>
         <!-- Rectangulo -->
         <div class="row">
            <div class="col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
                  <h1 class="text-center">DEJA TU HUELLA,</br>CREA UN IMPACTO</h1>
            </div>
         </div>

         <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 fondo-1">
               <div class="col-sm-12 fondo2">
                  <div class="col-sm-10 col-sm-offset-1 titulo-buscador">Comienza tu aventura seleccionando uno o m&aacute;s filtros:</div>
                  <div class="col-sm-12 padding0 text-align-general" id="responsive-menu-2-rec">

                      <div class="col-sm-3">
                          <select id="mounth">
                            <option value="hide">Áreas laborales</option>
                            <option value="area">Área 01</option>
                            <option value="area">Área 02</option>
                            <option value="area">Área 03</option>
                            <option value="area">Área 04</option>
                          </select>
                      </div>

                      <div class="col-sm-3">
                          <select id="mounth">
                            <option value="hide">Universidad</option>
                            <option value="universidad">Universidad 01</option>
                            <option value="universidad">Universidad 02</option>
                            <option value="universidad">Universidad 03</option>
                            <option value="universidad">Universidad 03</option>
                            <option value="universidad">Universidad 04</option>
                          </select>
                      </div>

                      <div class="col-sm-3">
                          <select id="mounth">
                            <option value="hide">Destino</option>
                            <option value="destino">Destino 01</option>
                            <option value="destino">Destino 02</option>
                            <option value="destino">Destino 03</option>
                            <option value="destino">Destino 04</option>
                            <option value="destino">Destino 05</option>
                          </select>
                      </div>

                      <div class="col-sm-3">
                          <select id="mounth">
                            <option value="hide">Estancia mínima</option>
                            <option value="estancia">Estancia 01</option>
                            <option value="estancia">Estancia 02</option>
                            <option value="estancia">Estancia 03</option>
                            <option value="estancia">Estancia 04</option>
                            <option value="estancia">Estancia 05</option>
                          </select>
                      </div>

                  </div>
               </div>
            </div>
         </div>

         <div class="row row-btn">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                 <div class="col-sm-4"></div>
                 <div class="col-sm-4"></div>
                 <div class="col-sm-4 bording-buttom-cafe">
                    <div class="rectangulo33">
                       <button type="button" class="btn btn-buscar-color" style="padding-top: 0px;padding-bottom: 0px;"><span class="boton-space">BUSCAR</span></button>
                    </div>
                 </div>
            </div>
         </div>

      </header>