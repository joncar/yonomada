
         var x, i, j, selElmnt, a, b, c;
         /*look for any elements with the class "custom-select":*/
         x = document.getElementsByClassName("custom-select");
         for (i = 0; i < x.length; i++) {
           selElmnt = x[i].getElementsByTagName("select")[0];
           /*for each element, create a new DIV that will act as the selected item:*/
           a = document.createElement("DIV");
           a.setAttribute("class", "select-selected");
           a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
           x[i].appendChild(a);
           /*for each element, create a new DIV that will contain the option list:*/
           b = document.createElement("DIV");
           b.setAttribute("class", "select-items select-hide");
           for (j = 1; j < selElmnt.length; j++) {
             /*for each option in the original select element,
             create a new DIV that will act as an option item:*/
             c = document.createElement("DIV");
             c.innerHTML = selElmnt.options[j].innerHTML;
             c.addEventListener("click", function(e) {
                 /*when an item is clicked, update the original select box,
                 and the selected item:*/
                 var i, s, h;
                 s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                 h = this.parentNode.previousSibling;
                 for (i = 0; i < s.length; i++) {
                   if (s.options[i].innerHTML == this.innerHTML) {
                     s.selectedIndex = i;
                     h.innerHTML = this.innerHTML;
                     break;
                   }
                 }
                 h.click();
             });
             b.appendChild(c);
           }
           x[i].appendChild(b);
           a.addEventListener("click", function(e) {
               /*when the select box is clicked, close any other select boxes,
               and open/close the current select box:*/
               e.stopPropagation();
               closeAllSelect(this);
               this.nextSibling.classList.toggle("select-hide");
               this.classList.toggle("select-arrow-active");
             });
         }
         function closeAllSelect(elmnt) {
           /*a function that will close all select boxes in the document,
           except the current select box:*/
           var x, y, i, arrNo = [];
           x = document.getElementsByClassName("select-items");
           y = document.getElementsByClassName("select-selected");
           for (i = 0; i < y.length; i++) {
             if (elmnt == y[i]) {
               arrNo.push(i)
             } else {
               y[i].classList.remove("select-arrow-active");
             }
           }
           for (i = 0; i < x.length; i++) {
             if (arrNo.indexOf(i)) {
               x[i].classList.add("select-hide");
             }
           }
         }
         /*if the user clicks anywhere outside the select box,
         then close all select boxes:*/
         document.addEventListener("click", closeAllSelect);

           // Get the modal
         var modal = document.getElementById('myModal');
         
         // Get the button that opens the modal
         var btn = document.getElementById("myBtn");
         
         // Get the <span> element that closes the modal
         var span = document.getElementsByClassName("close")[0];
         
         // When the user clicks the button, open the modal 
         if(typeof(btn)!=='undefined'){
           btn.onclick = function() {
               modal.style.display = "block";
           }
         }
         
         // When the user clicks on <span> (x), close the modal
         span.onclick = function() {
             modal.style.display = "none";
         }
         
         // When the user clicks anywhere outside of the modal, close it
         window.onclick = function(event) {
             if (event.target == modal) {
                 modal.style.display = "none";
             }
         }

         var appendNumber = 4;
          var prependNumber = 1;
          var swiper = new Swiper('.swiper-container', {
              pagination: '.swiper-pagination',
              nextButton: '.swiper-button-next',
              prevButton: '.swiper-button-prev',
              slidesPerView: 3,
              centeredSlides: true,
              paginationClickable: true,
              spaceBetween: 30,
          });
          document.querySelector('.prepend-2-slides').addEventListener('click', function (e) {
              e.preventDefault();
              swiper.prependSlide([
                  '<div class="swiper-slide">Slide ' + (--prependNumber) + '</div>',
                  '<div class="swiper-slide">Slide ' + (--prependNumber) + '</div>'
              ]);
          });
          document.querySelector('.prepend-slide').addEventListener('click', function (e) {
              e.preventDefault();
              swiper.prependSlide('<div class="swiper-slide">Slide ' + (--prependNumber) + '</div>');
          });
          document.querySelector('.append-slide').addEventListener('click', function (e) {
              e.preventDefault();
              swiper.appendSlide('<div class="swiper-slide">Slide ' + (++appendNumber) + '</div>');
          });
          document.querySelector('.append-2-slides').addEventListener('click', function (e) {
              e.preventDefault();
              swiper.appendSlide([
                  '<div class="swiper-slide">Slide ' + (++appendNumber) + '</div>',
                  '<div class="swiper-slide">Slide ' + (++appendNumber) + '</div>'
              ]);
          });
          function myFunction() {
          var x = document.getElementById("myTopnav");
          if (x.className === "topnav") {
              x.className += " responsive";
          } else {
              x.className = "topnav";
          }
          }
          $(document).ready(function () {
              // Muestra y oculta los menÃºs
              $('ul li:has(ul)').hover(
                      function (e)
                      {
                          $(this).find('ul').slideDown();
                      },
                      function (e)
                      {
                          $(this).find('ul').slideUp();
                      }
              );
          });
          
          //Menu Responsivo
          $("#btnMenuResp").click(function () {
              $("#bodyMenuResp").slideToggle();
          });
          
           $(".cssMenuPrincipal li").mouseover(function(){
              $(".fontHeaderOscuro").css("color","white");
          });
          
          // $(window).on('load', function() {
              //$(".cssPPLoading").fadeOut();
              //alert("a numa");
          // });
          function openNav() {
          document.getElementById("sideNavigation").style.width = "250px";
          document.getElementById("main").style.marginLeft = "250px";
          }
          
          function closeNav() {
          document.getElementById("sideNavigation").style.width = "0";
          document.getElementById("main").style.marginLeft = "0";
          }

         var x, i, j, selElmnt, a, b, c;
         /*look for any elements with the class "custom-select":*/
         x = document.getElementsByClassName("custom-select");
         for (i = 0; i < x.length; i++) {
           selElmnt = x[i].getElementsByTagName("select")[0];
           /*for each element, create a new DIV that will act as the selected item:*/
           a = document.createElement("DIV");
           a.setAttribute("class", "select-selected");
           a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
           x[i].appendChild(a);
           /*for each element, create a new DIV that will contain the option list:*/
           b = document.createElement("DIV");
           b.setAttribute("class", "select-items select-hide");
           for (j = 1; j < selElmnt.length; j++) {
             /*for each option in the original select element,
             create a new DIV that will act as an option item:*/
             c = document.createElement("DIV");
             c.innerHTML = selElmnt.options[j].innerHTML;
             c.addEventListener("click", function(e) {
                 /*when an item is clicked, update the original select box,
                 and the selected item:*/
                 var i, s, h;
                 s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                 h = this.parentNode.previousSibling;
                 for (i = 0; i < s.length; i++) {
                   if (s.options[i].innerHTML == this.innerHTML) {
                     s.selectedIndex = i;
                     h.innerHTML = this.innerHTML;
                     break;
                   }
                 }
                 h.click();
             });
             b.appendChild(c);
           }
           x[i].appendChild(b);
           a.addEventListener("click", function(e) {
               /*when the select box is clicked, close any other select boxes,
               and open/close the current select box:*/
               e.stopPropagation();
               closeAllSelect(this);
               this.nextSibling.classList.toggle("select-hide");
               this.classList.toggle("select-arrow-active");
             });
         }
         function closeAllSelect(elmnt) {
           /*a function that will close all select boxes in the document,
           except the current select box:*/
           var x, y, i, arrNo = [];
           x = document.getElementsByClassName("select-items");
           y = document.getElementsByClassName("select-selected");
           for (i = 0; i < y.length; i++) {
             if (elmnt == y[i]) {
               arrNo.push(i)
             } else {
               y[i].classList.remove("select-arrow-active");
             }
           }
           for (i = 0; i < x.length; i++) {
             if (arrNo.indexOf(i)) {
               x[i].classList.add("select-hide");
             }
           }
         }
         /*if the user clicks anywhere outside the select box,
         then close all select boxes:*/
         document.addEventListener("click", closeAllSelect);

         function sportsRotate(btn){
            var $card = $(btn).closest('.flip-container');
            //console.log($card);
            if($card.hasClass('hover')){
                $card.removeClass('hover');
            } else {
                $card.addClass('hover');
            }
         }


