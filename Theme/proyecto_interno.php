<!DOCTYPE html>
<html>
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>

   <body class="body2">
      <header class="container-fluid header2" id="conteiner-fluid-0">
         <div class="container">
           <!-- Menu -->
           <?php include('menu.php');?>
         </div>
         <div class="row row-new-2">
            <div class="col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
               <div class="col-sm-12">
                  <h1 class="text-center">CULTIVO DE CAFÉ EN CHIAPAS</h1>
               </div>
            </div>
         </div>

         <div class="row row-new-2">
            <div class="col-xs-10 col-xs-offset-1 col col-sm-10 col-sm-offset-1">
               <div class="col-xs-6 col-sm-2">
                  <button type="button" class="btn btn-default btn-colo-cu general-hover"><span>#keyword</span></button>
               </div>
               <div class="col-xs-6 col-sm-2">
                  <button type="button" class="btn btn-default btn-colo-cu general-hover">#keyword</button>
               </div>
               <div class="col-xs-6 col-sm-2">
                  <button type="button" class="btn btn-default btn-colo-cu general-hover">#keyword</button>
               </div>
               <div class="col-xs-6 col-sm-2">
                  <button type="button" class="btn btn-default btn-colo-cu general-hover">#keyword</button>
               </div>
               <div class="col-xs-6 col-sm-2">
                  <button type="button" class="btn btn-default btn-colo-cu general-hover">#keyword</button>
               </div>
               <div class="col-xs-6 col-sm-2">
                  <button type="button" class="btn btn-default btn-colo-cu general-hover">#keyword</button>
               </div>
            </div>
         </div>
         </div>
      </header>
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-12 col-sm-12" style="padding-bottom: 60px;">
               <div class="tabset">
                  <!-- Tab 1 -->
                  <input type="radio" name="tabset" id="tab1" aria-controls="marzen" checked>
                  <label for="tab1" class="font-tab-black"><i class="fa fa-child width-icon-tab" aria-hidden="true"></i>Experiencia</label>
                  <!-- Tab 2 -->
                  <input type="radio" name="tabset" id="tab2" aria-controls="rauchbier">
                  <label for="tab2" class="font-tab-black"><i class="fa fa-suitcase width-icon-tab" aria-hidden="true"></i>
                  Vacantes</label>
                  <!-- Tab 3 -->
                  <input type="radio" name="tabset" id="tab3" aria-controls="dunkles">
                  <label for="tab3" class="font-tab-black"><i class="fa fa-quote-left width-icon-tab" aria-hidden="true"></i>
                  Testimonios</label>
                  <input type="radio" name="tabset" id="tab4" aria-controls="dunkles">
                  <label for="tab4" class="font-tab-black"><i class="fa fa-map-marker width-icon-tab" aria-hidden="true"></i>Ubicación</label>
                  <div class="tab-panels col-sm-12 shadow-box" style="padding: 0px; ">
                     <section id="marzen" class="tab-panel col-sm-12 padding-30-new">
                        <div class="col-xs-12 col-sm-6">
                           <h2 class="title-menu-2 text-gray">Innovando en las cosechas de cafetales</h2>
                           <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
                           <div class="col-xs-12 col-sm-12 text-gray-general tex-gray-littel padding-text-new">
                              An elegant, malty German amber lager with a clean, rich, toasty and bready malt flavor, restrained bitterness, and a dry finish that encourages another drink. The overall malt impression is soft, elegant, and complex, with a rich aftertaste that is never cloying or heavy.</br></br>
                              As the name suggests, brewed as a stronger “March beer” in March and lagered in cold caves over the summer. Modern versions trace back to the lager developed by Spaten in 1841, contemporaneous to the development of Vienna lager. However, the Märzen name is much older than 1841; the early ones were dark brown, and in Austria the name implied a strength band (14 °P) rather than a style. The German amber lager version (in the Viennese style of the time) was first served at Oktoberfest in 1872, a tradition that lasted until 1990 when the golden Festbier was adopted as the standard festival beer.
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <iframe width="560" height="315" src="https://www.youtube.com/embed/fTNxXNTiZSM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen class="margin-t-50 padding-img-new"></iframe>
                        </div>
                        <div class="col-xs-12 col-sm-12" style="padding-bottom: 40px;">
                           <div class="rec-gray-1 col-sm-12">
                              <div class="col-xs-12 col-sm-12 text-gray-general tex-bold-gray" style="text-align: center; margin: 5px;">Avalada por:</div>
                              <div class="col-xs-12 col-sm-4"></div>
                              <div class="col-xs-12 col-sm-4" style="top: 25px;">
                                 <div class="col-xs-3 col-sm-3 paddin-logo logo-itam">
                                    <img src="img/logo-itam.png" alt="Video Yo nomada" class="img-responsive centerblock margin-t-50">
                                 </div>
                                 <div class="col-xs-3 col-sm-3 paddin-logo">
                                    <img src="img/ibero.jpg" alt="Video Yo nomada" class="img-responsive centerblock margin-t-50 logo-ibero">
                                 </div>
                                 <div class="col-xs-3 col-sm-3 paddin-logo">
                                    <img src="img/Escudo-UNAM-escalable.svg" alt="Video Yo nomada" class="img-responsive centerblock margin-t-50 width-logo-unam">
                                 </div>
                                 <div class="col-xs-3 col-sm-3 paddin-logo">
                                    <img src="img/logouam.gif" alt="Video Yo nomada" class="img-responsive centerblock margin-t-50">
                                 </div>
                              </div>
                              <div class="col-xs-4 col-sm-4"></div>
                           </div>
                        </div>
                        <div class="col-sm-12" style="padding-bottom: 20px;">
                           <button id="myBtn" type="button" class="btn btn-default btn-colo-green general-hover">SOLICITAR INFORMES</button>
                           <!-- The Modal -->
                           <div class="container-fluid">
                              <div id="myModal" class="modal">
                                 <!-- Modal content -->
                                 <div class="modal-content row-modal col-sm-8 col-sm-offset-2">
                                    <span class="close">&times;</span>
                                    <p class="p-blacl-title-modal">Solicitar informes</p>
                                    <p class="p-text-subtitle">Llena el siguiente formulario con tus datos y te enviaremos información a tu correo</p>
                                    <div class="col-sm-12">
                                       <div class="col-sm-6">
                                          <input id="input1" type="text" class="input-style-new-modal" placeholder="NOMBRE"/>
                                          <input id="input1" type="text" class="input-style-new-modal" placeholder="CORREO"/>
                                          <div class="custom-select color-white-select" style="width:100%;margin-bottom: 23px;">
                                             <select>
                                                <option value="0">ESTADO</option>
                                                <option value="1">Audi</option>
                                                <option value="2">BMW</option>
                                                <option value="3">Citroen</option>
                                                <option value="4">Ford</option>
                                                <option value="5">Honda</option>
                                             </select>
                                          </div>
                                          <div class="custom-select color-white-select" style="width:100%;margin-bottom: 23px;">
                                             <select>
                                                <option value="0">ÁREA</option>
                                                <option value="1">Audi</option>
                                                <option value="2">BMW</option>
                                                <option value="3">Citroen</option>
                                                <option value="4">Ford</option>
                                                <option value="5">Honda</option>
                                             </select>
                                          </div>
                                          <div class="custom-select color-white-select" style="width:100%;margin-bottom: 23px;">
                                             <select>
                                                <option value="0">CARRERA AFÍN</option>
                                                <option value="1">Audi</option>
                                                <option value="2">BMW</option>
                                                <option value="3">Citroen</option>
                                                <option value="4">Ford</option>
                                                <option value="5">Honda</option>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="col-sm-6">
                                          <input id="input1" type="text" class="input-style-new-modal" placeholder="APELLIDO"/>
                                          <input id="input1" type="text" class="input-style-new-modal" placeholder="TELÉFONO"/>
                                          <div class="custom-select color-white-select" style="width:100%;margin-bottom: 23px;">
                                             <select>
                                                <option value="0">UNIVERSIDAD</option>
                                                <option value="1">Audi</option>
                                                <option value="2">BMW</option>
                                                <option value="3">Citroen</option>
                                                <option value="4">Ford</option>
                                                <option value="5">Honda</option>
                                             </select>
                                          </div>
                                          <div class="custom-select color-white-select" style="width:100%;margin-bottom: 23px;">
                                             <select>
                                                <option value="0">DEPARTAMENTO</option>
                                                <option value="1">Audi</option>
                                                <option value="2">BMW</option>
                                                <option value="3">Citroen</option>
                                                <option value="4">Ford</option>
                                                <option value="5">Honda</option>
                                             </select>
                                          </div>
                                          <div class="custom-select color-white-select" style="width:100%;margin-bottom: 23px;">
                                             <select>
                                                <option value="0">SEMESTRE</option>
                                                <option value="1">Audi</option>
                                                <option value="2">BMW</option>
                                                <option value="3">Citroen</option>
                                                <option value="4">Ford</option>
                                                <option value="5">Honda</option>
                                             </select>
                                          </div>
                                       </div>
                                       <p class="text-black-filtros font-check-title-new">VACANTES QUE ME INTERESAN (MÁXIMO 3)</p>
                                       <div class="col-sm-12" style="margin-left: -29px;">
                                          <div class="col-sm-6">
                                             <div class="checkbox">
                                                <input id="box1" type="checkbox" />
                                                <label for="box1" class="pruebaa222"><span class="text-checkbox">NOMBRE DE LA VACANTE</span>
                                                </label>
                                             </div>
                                             <div class="checkbox">
                                                <input id="box2" type="checkbox" />
                                                <label for="box2" id="boxprueba"><span class="text-checkbox">NOMBRE DE LA VACANTE</span></label>
                                             </div>
                                             <div class="checkbox">
                                                <input id="box3" type="checkbox" />
                                                <label for="box3" id="boxprueba"><span class="text-checkbox">NOMBRE DE LA VACANTE</span></label>
                                             </div>
                                             <div class="checkbox">
                                                <input id="box4" type="checkbox" />
                                                <label for="box4" id="boxprueba"><span class="text-checkbox">NOMBRE DE LA VACANTE</span></label>
                                             </div>
                                          </div>
                                          <div class="col-sm-6">
                                             <div class="checkbox">
                                                <input id="box5" type="checkbox" />
                                                <label for="box5" id="boxprueba"><span class="text-checkbox">NOMBRE DE LA VACANTE</span></label>
                                             </div>
                                          </div>
                                       </div>
                                       <p class="text-black-filtros font-check-title-new">DURACIÓN DE MI ESTANCIA</p>
                                       <p class="text-black-filtros tex-range-dias">(Días)</p>
                                       <div class="rangeslider-wrap">
                                          <input type="range" min="1" max="100" labels="">
                                       </div>
                                       <button id="myBtn" type="button" class="btn btn-default btn-colo-green general-hover" style="margin-bottom: 50px;">ENVIAR</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                     <!--VACANTES-->
                     <section id="rauchbier" class="tab-panel col-sm-12 padding-30-new">
                        <div class="col-xs-12 col-sm-12">
                        <h2 class="title-menu-2 text-gray">¡Descubre nuestras vacantes disponibles!</h2>
                        <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
                        </div>
                        <div class="col-sm-12 text-gray-general tex-gray-littel padding-text-new-tarj">
                           <div class="col-sm-4 padding-tarj-new-all">
                              <div class="flip-container manual-flip">
                                 <div class="sports-card">
                                    <div class="front col-xs-12 col-sm-12" style="padding: 0px;">
                                       <div class="cover">
                                          <div class="cover2"></div>
                                          <div class="col-xs-12 col-sm-12 content-tex-tittle">
                                             <h3 class="text-center h3responsive">Lorem ipsum dolor sit amet orci aliquam</h3>
                                             <p class="text-gray-general text-center font-weight-bold h3responsive">Estancia mínima: 3 meses.</p>
                                             <div class="col-xs-6 col-sm-6" style="padding: 0;">
                                                <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Desde: Ahora</p>
                                             </div>
                                             <div class="col-xs-6 col-sm-6" style="padding: 0;">
                                                <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Hasta:20-12-18</p>
                                             </div>
                                             <p class="text-gray-general text-center" id="responsive-ciencias-title1">Ciencias, Matemáticas e Ingenierías </p>
                                          </div>
                                       </div>
                                       <div class="content col-sm-12 scrollbar force-overflow" id="style-3" style="padding: 8px 0px 10px 20px;">
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/arquitectura.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Arquitectura y urbanismo</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/tierra.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Ciencias</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ciencias ambientales
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ciencias de la tierra
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/ingenierias.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Ingenierías</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería Química
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería en Energía Renovable
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería en Sistemas biomédicos
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería Geofísica
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/matematicas.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10  col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Matemáticas</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/tecnologia.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Tecnología</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
                                          <button class="sports-button boton-colo-black margin-buttom-ver-mas2 icon-blue-tarj">
                                          <strong>VER MÁS</strong>
                                          </button>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black2">
                                          <button class="sports-button margin-buttom-ver-mas color-green-tarj">
                                          <strong>QUIERO INFORMES</strong>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="back col-xs-12 col-sm-12" style="padding: 0px;" id="size-card-new">
                                       <div class="cover2"></div>
                                       <div class="col-xs-12 col-sm-12 point-img-cover col-max-min-height-tar col-sm-12 force-overflow shadow-new-all" id="style-3">
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Descripción</h3>
                                          <div class="c">
                                             <div class="main">
                                                <p class="text-gray-general-tar">500 caracteres lorem ipsum dolor sit amet, consectetur adipisci Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                                </p>
                                             </div>
                                          </div>
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Requisitos</h3>
                                          <span class="text-gray-general-tar"><i class="fa fa-check icon-blue-tarj" aria-hidden="true" style="margin-right: 10px;"></i>Últimos dos semestres de la carrera o egresado
                                          </span></br>
                                          <span class="text-gray-general-tar"><i class="fa fa-check icon-blue-tarj" aria-hidden="true" style="margin-right: 10px;"></i>Ingles fluido
                                          </span>
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Avisos y condiciones</h3>
                                          <p class="text-gray-general-tar ">500 caracteres lorem ipsum dolor sit amet, consectetur adipisci Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                          </p>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
                                          <button class="sports-button boton-colo-black margin-buttom-back">
                                          <strong><i class="fa fa-reply icon-blue-tarj fa-lg" aria-hidden="true"></i></strong>
                                          </button>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black2">
                                          <button class="sports-button margin-buttom-ver-mas color-green-tarj">
                                          <strong>QUIERO INFORMES</strong>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-4 padding-tarj-new-all">
                              <div class="flip-container manual-flip">
                                 <div class="sports-card">
                                    <div class="col-xs-12 front col-sm-12" style="padding: 0px;">
                                       <div class="cover border-yellow">
                                          <div class="cover2-yellow"></div>
                                          <div class="col-xs-12 col-sm-12 content-tex-tittle">
                                             <h3 class="text-center h3responsive">Lorem ipsum dolor sit amet orci aliquam</h3>
                                             <p class="text-gray-general text-center font-weight-bold h3responsive">Estancia mínima: 3 meses.</p>
                                             <div class="col-xs-6 col-sm-6" style="padding: 0;">
                                                <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Desde: Ahora</p>
                                             </div>
                                             <div class="col-xs-6 col-sm-6" style="padding: 0;">
                                                <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Hasta:20-12-18</p>
                                             </div>
                                             <p class="text-gray-general text-center" id="responsive-ciencias-title1">Ciencias, Matemáticas e Ingenierías </p>
                                          </div>
                                       </div>
                                       <div class="col-xs-12 content col-sm-12 scrollbar force-overflow" id="style-4" style="padding: 8px 0px 10px 20px;">
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/arquitectura.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Arquitectura y urbanismo</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/tierra.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Ciencias</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ciencias ambientales
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ciencias de la tierra
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/ingenierias.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Ingenierías</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería Química
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería en Energía Renovable
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería en Sistemas biomédicos
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería Geofísica
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/matematicas.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Matemáticas</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/tecnologia.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Tecnología</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
                                          <button class="sports-button boton-colo-black margin-buttom-ver-mas2 icon-blue-tarj">
                                          <strong>VER MÁS</strong>
                                          </button>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black2">
                                          <button class="sports-button margin-buttom-ver-mas color-green-tarj">
                                          <strong>QUIERO INFORMES</strong>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="back col-xs-12 col-sm-12" style="padding: 0px;" id="size-card-new">
                                       <div class="cover2-yellow"></div>
                                       <div class="col-sm-12 point-img-cover-yellow col-max-min-height-tar col-sm-12 force-overflow shadow-new-all" id="style-4">
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Descripción</h3>
                                          <div class="c">
                                             <div class="main">
                                                <p class="text-gray-general-tar">500 caracteres lorem ipsum dolor sit amet, consectetur adipisci Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                                </p>
                                             </div>
                                          </div>
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Requisitos</h3>
                                          <span class="text-gray-general-tar"><i class="fa fa-check icon-blue-tarj" aria-hidden="true" style="margin-right: 10px;"></i>Últimos dos semestres de la carrera o egresado
                                          </span></br>
                                          <span class="text-gray-general-tar"><i class="fa fa-check icon-blue-tarj" aria-hidden="true" style="margin-right: 10px;"></i>Ingles fluido
                                          </span>
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Avisos y condiciones</h3>
                                          <p class="text-gray-general-tar ">500 caracteres lorem ipsum dolor sit amet, consectetur adipisci Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                          </p>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
                                          <button class="sports-button boton-colo-black margin-buttom-back">
                                          <strong><i class="fa fa-reply icon-blue-tarj fa-lg" aria-hidden="true"></i></strong>
                                          </button>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black2">
                                          <button class="sports-button margin-buttom-ver-mas color-green-tarj">
                                          <strong>QUIERO INFORMES</strong>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-4 padding-tarj-new-all">
                              <div class="flip-container manual-flip">
                                 <div class="sports-card">
                                    <div class="front col-sm-12" style="padding: 0px;">
                                       <div class="cover border-pink">
                                          <div class="cover2-pink"></div>
                                          <div class="col-xs-12 col-sm-12 content-tex-tittle">
                                             <h3 class="text-center h3responsive">Lorem ipsum dolor sit amet orci aliquam</h3>
                                             <p class="text-gray-general text-center font-weight-bold h3responsive">Estancia mínima: 3 meses.</p>
                                             <div class="col-xs-6 col-sm-6" style="padding: 0;">
                                                <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Desde: Ahora</p>
                                             </div>
                                             <div class="col-xs-6 col-sm-6" style="padding: 0;">
                                                <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Hasta:20-12-18</p>
                                             </div>
                                             <p class="text-gray-general text-center" id="responsive-ciencias-title1">Ciencias, Matemáticas e Ingenierías </p>
                                          </div>
                                       </div>
                                       <div class="content col-sm-12 scrollbar force-overflow" id="style-5" style="padding: 8px 0px 10px 20px;">
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/arquitectura.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Arquitectura y urbanismo</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/tierra.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Ciencias</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ciencias ambientales
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ciencias de la tierra
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/ingenierias.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Ingenierías</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería Química
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería en Energía Renovable
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería en Sistemas biomédicos
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería Geofísica
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/matematicas.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Matemáticas</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/tecnologia.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Tecnología</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
                                          <button class="sports-button boton-colo-black margin-buttom-ver-mas2 icon-blue-tarj">
                                          <strong>VER MÁS</strong>
                                          </button>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black2">
                                          <button class="sports-button margin-buttom-ver-mas color-green-tarj">
                                          <strong>QUIERO INFORMES</strong>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="back col-xs-12 col-sm-12" style="padding: 0px;" id="size-card-new">
                                       <div class="cover2-pink"></div>
                                       <div class="col-xs-12 col-sm-12 point-img-cover-pink col-max-min-height-tar col-sm-12 force-overflow shadow-new-all" id="style-5">
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Descripción</h3>
                                          <div class="c">
                                             <div class="main">
                                                <p class="text-gray-general-tar">500 caracteres lorem ipsum dolor sit amet, consectetur adipisci Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                                </p>
                                             </div>
                                          </div>
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Requisitos</h3>
                                          <span class="text-gray-general-tar"><i class="fa fa-check icon-blue-tarj" aria-hidden="true" style="margin-right: 10px;"></i>Últimos dos semestres de la carrera o egresado
                                          </span></br>
                                          <span class="text-gray-general-tar"><i class="fa fa-check icon-blue-tarj" aria-hidden="true" style="margin-right: 10px;"></i>Ingles fluido
                                          </span>
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Avisos y condiciones</h3>
                                          <p class="text-gray-general-tar ">500 caracteres lorem ipsum dolor sit amet, consectetur adipisci Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                          </p>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
                                          <button class="sports-button boton-colo-black margin-buttom-back">
                                          <strong><i class="fa fa-reply icon-blue-tarj fa-lg" aria-hidden="true"></i></strong>
                                          </button>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black2">
                                          <button class="sports-button margin-buttom-ver-mas color-green-tarj">
                                          <strong>QUIERO INFORMES</strong>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="text-gray-general tex-gray-littel padding-text-new-tarj tarj-margin-0-p">
                           <div class="col-sm-4 padding-tarj-new-all margin-ta-col-4-tarj">
                              <div class="flip-container manual-flip">
                                 <div class="sports-card">
                                    <div class="front col-sm-12" style="padding: 0px;">
                                       <div class="cover">
                                          <div class="cover2"></div>
                                          <div class="col-xs-12 col-sm-12 content-tex-tittle">
                                             <h3 class="text-center h3responsive">Lorem ipsum dolor sit amet orci aliquam</h3>
                                             <p class="text-gray-general text-center font-weight-bold h3responsive">Estancia mínima: 3 meses.</p>
                                             <div class="col-xs-6 col-sm-6" style="padding: 0;">
                                                <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Desde: Ahora</p>
                                             </div>
                                             <div class="col-xs-6 col-sm-6" style="padding: 0;">
                                                <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Hasta:20-12-18</p>
                                             </div>
                                             <p class="text-gray-general text-center" id="responsive-ciencias-title1">Ciencias, Matemáticas e Ingenierías </p>
                                          </div>
                                       </div>
                                       <div class="content col-xs-12 col-sm-12 scrollbar force-overflow" id="style-3" style="padding: 8px 0px 10px 20px;">
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/arquitectura.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Arquitectura y urbanismo</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/tierra.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Ciencias</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ciencias ambientales
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ciencias de la tierra
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/ingenierias.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Ingenierías</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería Química
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería en Energía Renovable
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería en Sistemas biomédicos
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería Geofísica
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/matematicas.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Matemáticas</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/tecnologia.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Tecnología</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
                                          <button class="sports-button boton-colo-black margin-buttom-ver-mas2 icon-blue-tarj">
                                          <strong>VER MÁS</strong>
                                          </button>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black2">
                                          <button class="sports-button margin-buttom-ver-mas color-green-tarj">
                                          <strong>QUIERO INFORMES</strong>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="col-xs-12 back col-sm-12" style="padding: 0px;" id="size-card-new">
                                       <div class="cover2"></div>
                                       <div class="col-xs-12 col-sm-12 point-img-cover col-max-min-height-tar col-sm-12 force-overflow shadow-new-all" id="style-3">
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Descripción</h3>
                                          <div class="c">
                                             <div class="main">
                                                <p class="text-gray-general-tar">500 caracteres lorem ipsum dolor sit amet, consectetur adipisci Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                                </p>
                                             </div>
                                          </div>
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Requisitos</h3>
                                          <span class="text-gray-general-tar"><i class="fa fa-check icon-blue-tarj" aria-hidden="true" style="margin-right: 10px;"></i>Últimos dos semestres de la carrera o egresado
                                          </span></br>
                                          <span class="text-gray-general-tar"><i class="fa fa-check icon-blue-tarj" aria-hidden="true" style="margin-right: 10px;"></i>Ingles fluido
                                          </span>
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Avisos y condiciones</h3>
                                          <p class="text-gray-general-tar ">500 caracteres lorem ipsum dolor sit amet, consectetur adipisci Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                          </p>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
                                          <button class="sports-button boton-colo-black margin-buttom-back">
                                          <strong><i class="fa fa-reply icon-blue-tarj fa-lg" aria-hidden="true"></i></strong>
                                          </button>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black2">
                                          <button class="sports-button margin-buttom-ver-mas color-green-tarj">
                                          <strong>QUIERO INFORMES</strong>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-4 padding-tarj-new-all margin-ta-col-4-tarj">
                              <div class="flip-container manual-flip">
                                 <div class="sports-card">
                                    <div class="front col-xs-12 col-sm-12" style="padding: 0px;">
                                       <div class="cover border-yellow">
                                          <div class="cover2-yellow"></div>
                                          <div class="col-xs-12 col-sm-12 content-tex-tittle">
                                             <h3 class="text-center h3responsive">Lorem ipsum dolor sit amet orci aliquam</h3>
                                             <p class="text-gray-general text-center font-weight-bold h3responsive">Estancia mínima: 3 meses.</p>
                                             <div class="col-xs-6 col-sm-6" style="padding: 0;">
                                                <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Desde: Ahora</p>
                                             </div>
                                             <div class="col-xs-6 col-sm-6" style="padding: 0;">
                                                <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Hasta:20-12-18</p>
                                             </div>
                                             <p class="text-gray-general text-center" id="responsive-ciencias-title1">Ciencias, Matemáticas e Ingenierías </p>
                                          </div>
                                       </div>
                                       <div class="content col-xs-12 col-sm-12 scrollbar force-overflow" id="style-4" style="padding: 8px 0px 10px 20px;">
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/arquitectura.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Arquitectura y urbanismo</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/tierra.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Ciencias</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ciencias ambientales
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ciencias de la tierra
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/ingenierias.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Ingenierías</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería Química
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería en Energía Renovable
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería en Sistemas biomédicos
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería Geofísica
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/matematicas.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Matemáticas</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/tecnologia.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Tecnología</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
                                          <button class="sports-button boton-colo-black margin-buttom-ver-mas2 icon-blue-tarj">
                                          <strong>VER MÁS</strong>
                                          </button>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black2">
                                          <button class="sports-button margin-buttom-ver-mas color-green-tarj">
                                          <strong>QUIERO INFORMES</strong>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="back col-xs-12 col-sm-12" style="padding: 0px;" id="size-card-new">
                                       <div class="cover2-yellow"></div>
                                       <div class="col-xs-12 col-sm-12 point-img-cover-yellow col-max-min-height-tar col-sm-12 force-overflow shadow-new-all" id="style-4">
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Descripción</h3>
                                          <div class="c">
                                             <div class="main">
                                                <p class="text-gray-general-tar">500 caracteres lorem ipsum dolor sit amet, consectetur adipisci Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                                </p>
                                             </div>
                                          </div>
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Requisitos</h3>
                                          <span class="text-gray-general-tar"><i class="fa fa-check icon-blue-tarj" aria-hidden="true" style="margin-right: 10px;"></i>Últimos dos semestres de la carrera o egresado
                                          </span></br>
                                          <span class="text-gray-general-tar"><i class="fa fa-check icon-blue-tarj" aria-hidden="true" style="margin-right: 10px;"></i>Ingles fluido
                                          </span>
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Avisos y condiciones</h3>
                                          <p class="text-gray-general-tar ">500 caracteres lorem ipsum dolor sit amet, consectetur adipisci Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                          </p>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
                                          <button class="sports-button boton-colo-black margin-buttom-back">
                                          <strong><i class="fa fa-reply icon-blue-tarj fa-lg" aria-hidden="true"></i></strong>
                                          </button>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black2">
                                          <button class="sports-button margin-buttom-ver-mas color-green-tarj">
                                          <strong>QUIERO INFORMES</strong>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-4 padding-tarj-new-all margin-ta-col-4-tarj">
                              <div class="flip-container manual-flip">
                                 <div class="sports-card">
                                    <div class="front col-xs-12 col-sm-12" style="padding: 0px;">
                                       <div class="cover border-pink">
                                          <div class="cover2-pink"></div>
                                          <div class="col-xs-12 col-sm-12 content-tex-tittle">
                                             <h3 class="text-center h3responsive">Lorem ipsum dolor sit amet orci aliquam</h3>
                                             <p class="text-gray-general text-center font-weight-bold h3responsive">Estancia mínima: 3 meses.</p>
                                             <div class="col-xs-6 col-sm-6" style="padding: 0;">
                                                <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Desde: Ahora</p>
                                             </div>
                                             <div class="col-xs-6 col-sm-6" style="padding: 0;">
                                                <p class="text-gray-general font-weight-bold text-tarj-tex-new-12">Hasta:20-12-18</p>
                                             </div>
                                             <p class="text-gray-general text-center" id="responsive-ciencias-title1">Ciencias, Matemáticas e Ingenierías </p>
                                          </div>
                                       </div>
                                       <div class="content col-xs-12 col-sm-12 scrollbar force-overflow" id="style-5" style="padding: 8px 0px 10px 20px;">
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/arquitectura.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Arquitectura y urbanismo</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/tierra.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Ciencias</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ciencias ambientales
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ciencias de la tierra
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/ingenierias.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Ingenierías</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería Química
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería en Energía Renovable
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería en Sistemas biomédicos
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Ingeniería Geofísica
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/matematicas.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Matemáticas</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 margin-top-tarjeta">
                                             <div class="col-xs-2 col-sm-2" style="padding: 0px;">
                                                <img src="img/tecnologia.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tarj-cirl">
                                             </div>
                                             <div class="col-xs-10 col-sm-10">
                                                <p class="margin-title-tarj font-weight-bold text-gray-general">Tecnología</p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Diseño urbano
                                                </p>
                                                <p class="margin-title-tarj2 text-gray-general margin-tarj-0">·Sustentabilidad
                                                </p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
                                          <button class="sports-button boton-colo-black margin-buttom-ver-mas2 icon-blue-tarj">
                                          <strong>VER MÁS</strong>
                                          </button>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black2">
                                          <button class="sports-button margin-buttom-ver-mas color-green-tarj">
                                          <strong>QUIERO INFORMES</strong>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="back col-xs-12 col-sm-12" style="padding: 0px;" id="size-card-new">
                                       <div class="cover2-pink"></div>
                                       <div class="col-xs-12 col-sm-12 point-img-cover-pink col-max-min-height-tar col-sm-12 force-overflow shadow-new-all" id="style-5">
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Descripción</h3>
                                          <div class="c">
                                             <div class="main">
                                                <p class="text-gray-general-tar">500 caracteres lorem ipsum dolor sit amet, consectetur adipisci Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                                </p>
                                             </div>
                                          </div>
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Requisitos</h3>
                                          <span class="text-gray-general-tar"><i class="fa fa-check icon-blue-tarj" aria-hidden="true" style="margin-right: 10px;"></i>Últimos dos semestres de la carrera o egresado
                                          </span></br>
                                          <span class="text-gray-general-tar"><i class="fa fa-check icon-blue-tarj" aria-hidden="true" style="margin-right: 10px;"></i>Ingles fluido
                                          </span>
                                          <h3 class="text-gray-general-tar" style="font-size: 20px;">Avisos y condiciones</h3>
                                          <p class="text-gray-general-tar ">500 caracteres lorem ipsum dolor sit amet, consectetur adipisci Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
                                          </p>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black" onclick="sportsRotate(this)">
                                          <button class="sports-button boton-colo-black margin-buttom-back">
                                          <strong><i class="fa fa-reply icon-blue-tarj fa-lg" aria-hidden="true"></i></strong>
                                          </button>
                                       </div>
                                       <div class="col-xs-6 col-sm-6 boton-colo-black2">
                                          <button class="sports-button margin-buttom-ver-mas color-green-tarj">
                                          <strong>QUIERO INFORMES</strong>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                     </section>
                      <!--TESTIMONIOS-->
                     <section id="dunkles" class="tab-panel col-sm-12">
                        <div class="col-sm-12 top-responsive-tes-tar">
                           <h2 class="title-menu-2 text-gray">Testimonios</h2>
                           <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
                           <div class="row">
                              <div class="col-sm-12 text-gray-3">
                              </div>
                              <div class="col-sm-12">
                                 <div class="col-sm-2"></div>
                                 <div class="col-sm-8 top-slider-responsive-tes">
                                    <!-- Slider -->
                                    <div class="swiper-container">
                                       <div class="swiper-wrapper ">
                                          <div class="swiper-slide slider-transparent"><img src="img/testimonios-3.png" class="slider-1"/></div>
                                          <div class="swiper-slide slider-transparent"><img src="img/testimonios-1.png" class="slider-1"/></div>
                                          <div class="swiper-slide slider-transparent"><img src="img/testimonios-2.png" class="slider-1"/></div>
                                       </div>
                                       <!-- Add Pagination-->
                                       <div class="swiper-pagination bottom-gray-slider"></div>
                                       <!-- Add Arrows -->
                                       <div class="swiper-button-next bottom-gray-slider"></div>
                                       <div class="swiper-button-prev bottom-gray-slider"></div>
                                    </div>
                                 </div>
                                 <div class="col-sm-2"></div>
                              </div>
                              <div class="col-sm-12 centar-columna">
                                 <span class="text-black-general text-black-bold ">NOMBRE DEL PROYECTO</span>
                                 <div class="col-sm-12 font-14px">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-8 padding-tes-1">
                                       <span class="text-black-general">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis viverra faucibus mauris, vitae molestie libero pulvinar a. Donec efficitur lorem quam, ut tempus mauris efficitur nec. Curabitur placerat tristique metus, a varius leo suscipit a. Integer vel volutpat justo.</span></br>
                                       <p class="tex-formato-testi-slider">-Mariana Gómez</p>
                                       <p class="tex-formato-testi-slider ">Universidad ITAM</p>
                                    </div>
                                    <div class="col-sm-2"></div>
                                 </div>
                                 <div class="col-sm-12" style="padding-bottom: 20px;">
                                    <button type="button" class="btn btn-default btn-colo-green general-hover">BUSCAR</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                      <!--UBICACION-->
                     <section id="new" class="tab-panel col-sm-12 padding-30-new">
                        <div class="col-sm-12">
                           <h2 class="title-menu-2 text-gray">¿Cómo llegar?</h2>
                           <div class="col-sm-1 col-xs-4 line-colo-cyan"></div>
                        </div>
                        <div class="col-sm-6 text-gray-general tex-gray-littel padding-mapa-2">
                           <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14980710.007984804!2d-102.62050004999999!3d23.5540767!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2smx!4v1522769860376" class="border-blue-map" width="500" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                        <div class="col-sm-6 padding-mapa-2">
                           <span class="text-black-2 text-black-general">NOMBRE DE LA FUNDACIÓN O LUGAR</span></br></br></br>
                           <div id="col-sm-6">
                              <div class="icon-padding">
                                 <i class="fa fa-map-marker fa-lg icon-color icon-center" aria-hidden="true"></i>
                                 <span class="text-gray-4 text-gray-general">ESTADO, CIUDAD</span></br>
                                 <div class="text-gray-4 text-black-new2 margin-text-new2">Calle, número exterior, número interior, Colonia, DELEGACIÓN, C.P.
                                 </div>
                              </div>
                              <div class="icon-padding">
                                 <i class="fa fa-pagelines fa-lg icon-color icon-center" aria-hidden="true"></i>
                                 <span class="text-gray-4 text-gray-general">ACERCA DE ESTE LUGAR</span></br>
                                 <div class="text-gray-4 text-black-new2 margin-text-new3">
                                    Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años.
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-12" style="padding-bottom: 20px;">
                           <button type="button" class="btn btn-default btn-colo-green general-hover">SOLICITAR INFORMES</button>
                        </div>
                     </section>
                  </div>
               </div>
            </div>
         </div>
      </div>


      <!-- Contacto Footer -->
      <div class="container-fluid" id="contacto">
        <?php include('formulario.php');?>
      </div>

      <!-- Footer-->
      <footer>
        <?php include('footer.php');?>
      </footer>

      <!-- Librerias JS -->
      <script src="js/main.js"></script>
      <script src="js/main_2.js"></script>
      <script src="js/slider.js"></script>
   </body>
</html>
