<!DOCTYPE html>
<html>
<head>
<!-- Librerias -->
<?php include('head.php');?>
    <!-- FALTARÍA SCRIPT DE CAMBIAR CLASE DE LAS TABS A ACTIVE O NO ACTIVE -->
</head>

   <body class="body2">
      <header class="container-fluid header3" id="conteiner-fluid-0">
          <div class="container">
              <!-- Menu -->
              <?php include('menu.php');?>
          </div>
         <div class="row row-new-2">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
               <div class="col-xs-12 col-sm-12">
                  <h1 class="text-center">ÁREAS LABORALES</h1>
                  <div class="text-center-new1">¡Selecciona tu carrera o área laboral y explora los proyectos disponibles en los que puedes participar!</div>
               </div>
            </div>
         </div>
      </header>
      <!-- Conteiner-->
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-12 col-sm-12" style="padding-bottom: 60px;">
               <div class="tabset">
                  <!-- Tab 1 -->
                  <input type="radio" name="tabset" id="tab1" aria-controls="marzen" checked>
                  <label for="tab1" class="font-tab-black padding-0-new tamano-laber-a">
                     <img src="img/ciencias-tab_2.jpg" alt="Video Yo nomada" class="img-responsive centerblock display-100 contenedor6 tab_active">
                     <div class="centrado2 display-100 contenedor6" style="display:none;">CIENCIAS SOCIALES</div>
                  </label>
                  <!-- Tab 2 -->
                  <input type="radio" name="tabset" id="tab2" aria-controls="rauchbier">
                  <label for="tab2" class="font-tab-black padding-0-new tamano-laber-a">
                     <img src="img/matematicas-tab_2.jpg" alt="Video Yo nomada" class="img-responsive centerblock display-100 contenedor6">
                     <div class="centrado2" style="display:none;">CIENCIAS, MATEMÁTICAS E INGENIERÍAS</div>
                  </label>
                  <!-- Tab 3 -->
                  <input type="radio" name="tabset" id="tab3" aria-controls="dunkles">
                  <label for="tab3" class="font-tab-black padding-0-new tamano-laber-a">
                     <img src="img/arte-tab_2.jpg" alt="Video Yo nomada" class="img-responsive centerblock display-100 contenedor6">
                     <div class="centrado2" style="display:none;">ARTE, DISEÑO Y HUMANIDADES</div>
                  </label>
                  <input type="radio" name="tabset" id="tab4" aria-controls="dunkles">
                  <label for="tab4" class="font-tab-black padding-0-new2 tamano-laber-a">
                     <img src="img/biologia2-tab_2.jpg" alt="Video Yo nomada" class="img-responsive centerblock display-100 contenedor6">
                     <div class="centrado2" style="display:none;">BIOLOGÍA, QUÍMICA Y SALUD</div>
                  </label>
                  <!-- Conteiner-->
                  <div class="tab-panels col-xs-12 col-sm-12 shadow-box" style="padding: 0px; ">
                  <!-- Conteiner1 C.sociales-->
                     <section id="marzen" class="tab-panel col-xs-12 col-sm-12 padding-30-new">
                        <div class="col-xs-12 col-sm-12">
                           <h2 class="title-menu-2 text-gray">Ciencias Sociales</h2>
                           <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/administracion.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              ADMINISTRACIÓN Y ECONOMÍA
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                               <a href="index.html"> <p class="text-area-labo">Área Laboral</p></a>
                               <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                               <a href="index.html"> <p class="text-area-labo">Área Laboral</p></a>
                               <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                               <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                               <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                               <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                               <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                               <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                               <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                               <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                               <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/comunicacion.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              COMUNICACIÓN
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/derecho.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-3 col-sm-11 padding-tittle-img-tab title-blue-n">
                              DERECHO
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/relaciones.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              RELACIONES INTERNACIONALES
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/sociales.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              SOCIOLOGÍA
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                     </section>
                     <!-- Conteiner2 C.Mate-->
                     <section id="rauchbier" class="tab-panel col-xs-12 col-sm-12 padding-30-new">
                        <div class="col-xs-12 col-sm-12">
                           <h2 class="title-menu-2 text-gray">Ciencias, Matemáticas e Ingenierías</h2>
                           <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/arquitectura.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              ARQUITECTURA Y URBANISMO
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/tierra.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              CIENCIAS DE LA TIERRA
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/ingenierias.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              INGENIERÍAS
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-0 col-sm-1">
                              <img src="img/matematicas.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              MATEMÁTICAS
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-6 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/tecnologia.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              TECNOLOGÍA
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                     </section>
                     <!-- Conteiner3 ArteD-->
                     <section id="dunkles" class="tab-panel col-xs-12 col-sm-12 padding-30-new">
                        <div class="col-xs-12 col-sm-12">
                           <h2 class="title-menu-2 text-gray">Arte, Diseño y Humanidades</h2>
                           <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/arte.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              ARTE Y DISEÑO
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/literatura.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              LENGUAS Y LITERATURA
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/filosofia.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              FILOSOFÍA
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/historia.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              HISTORIA
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/pedagogia.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              PEDAGOGÍA
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                     </section>
                     <!-- Conteiner4 Biologia-->
                     <section id="new" class="tab-panel col-xs-12 col-sm-12 padding-30-new">
                        <div class="col-xs-12 col-sm-12">
                           <h2 class="title-menu-2 text-gray">Biología, Química y Salud</h2>
                           <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/medicina.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              MEDICINA
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/pedagogia.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              PSICOLOGÍA
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/agronomia.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              AGRONOMÍA Y ALIMENTOS
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/quimica.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              QUÍMICA
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-row-new">
                           <div class="col-xs-3 col-sm-1">
                              <img src="img/biologia.jpg" alt="Video Yo nomada" class="img-responsive centerblock img-circle img-new-tab2">
                           </div>
                           <div class="col-xs-9 col-sm-11 padding-tittle-img-tab title-blue-n">
                              BIOLOGÍA Y MEDIO AMBIENTE
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 padding-40-tab">
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                           <div class="col-xs-6 col-sm-2 new-padding-responsive">
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                              <a href="index.html"><p class="text-area-labo">Área Laboral</p></a>
                           </div>
                           <div class="col-xs-0 col-sm-1"></div>
                        </div>
                     </section>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Contacto Footer -->
      <div class="container-fluid" id="contacto">
        <?php include('formulario.php');?>
      </div>

      <!-- Footer-->
      <footer>
        <?php include('footer.php');?>
      </footer>

      <!-- Librerias JS -->
      <script src="js/main.js"></script>
      <script src="js/main_2.js"></script>
      <script src="js/slider.js"></script>
    </body>
</html>
