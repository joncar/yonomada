<!-- Footer-->
<div class="container-fluid">
  <div class="row row-btn">
     <div class="col-sm-12" style="margin-bottom: 40px;">
        <div class="col-xs-12 col-sm-1"></div>
        <div class="col-xs-10 col-xs-offset-1 col-sm-3 padding-top-footer">
           <a href="index.php"><img src="img/logo_nomada.png" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales"></a>
           <span class="font-black-footer text-footer-copy" id="footer-responsive1"><i class="fa fa-copyright" aria-hidden="true"></i>2018 TODOS LOS DERECHOS RESERVADOS</span>
        </div>
        <div class="col-xs-0 col-sm-2"></div>
        <div class="col-xs-6 col-sm-2 padding-top-footer2">
           <span class="text-black-bold titulo-footer-site">YO NÓMADA</span></br>
           <a href="index.php"><span class="font-black-footer">Inicio</span></a></br>
           <a href="proyectos.php"><span class="font-black-footer">Especialidades</span></a></br>
           <a href="proyectos.php"><span class="font-black-footer">Proyectos</span></a></br>
           <a href="galeria.php"><span class="font-black-footer">Galería</span></a></br>
           <a href="blog.php"><span class="font-black-footer">Blog</span></a></br>
           <a href="nosotros.php"><span class="font-black-footer">Nosotros</span></a></br>
           <a href="contacto.php"><span class="font-black-footer">Contacto</span></a></br>
        </div>
        <div class="col-xs-6 col-sm-2 padding-top-footer2">
           <span class="text-black-bold titulo-footer-site">LEGAL</span></br>
           <a href="index.html"><span class="font-black-footer"><span class="font-black-footer">Términos y condiciones</span></a></br>
           <a href="index.html"><span class="font-black-footer"><span class="font-black-footer">Aviso de privacidad</span></a></br>
        </div>
        <div class="col-xs-6 col-sm-2 padding-top-footer2">
           <span class="fa-stack fa-1x">
           <i class="fa fa-circle fa-stack-2x color-icon-footer"></i>
           <i class="cursor-pointer1 fa fa-instagram fa-stack-1x fa-inverse color-icon-footter-new general-hover"></i>
           </span>
           <span class="fa-stack fa-1x">
           <i class="fa fa-circle fa-stack-2x color-icon-footer"></i>
           <i class="cursor-pointer1 fa fa-facebook fa-stack-1x fa-inverse color-icon-footter-new general-hover"></i>
           </span>
           <span class="fa-stack fa-1x">
           <i class="fa fa-circle fa-stack-2x color-icon-footer"></i>
           <i class="cursor-pointer1 fa fa-linkedin fa-stack-1x fa-inverse color-icon-footter-new general-hover"></i>
           </span>
        </div>
     </div>
  </div>
</div>
