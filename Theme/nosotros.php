<!DOCTYPE html>
<html>
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>

   <body>
      <header class="container-fluid header5" id="conteiner-fluid-0">
          <div class="container">
              <!-- Menu -->
              <?php include('menu.php');?>
          </div>
         <div class="row row-new-2">
            <div class="col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
               <div class="col-sm-12">
                  <h1 class="text-center">ACERCA DE NOSOTROS</h1>
                  <div class="text-center-new1">¡Renovando el trabajo social!</div>
               </div>
            </div>
         </div>
         </div>
      </header>
      <div class="container-fluid">
         <div class="row row-nosotro1">
            <div class="col-xs-12 col-sm-12">
               <div class="col-xs-12 col-sm-12">
                  <div class="col-xs-0 col-sm-1"></div>
                   <div class="col-xs-12 col-sm-4">
                      <img src="img/logo_nomada_blue.png" alt="Administrac&iacute;on" class="img-responsive centerblock padding-img-laborales">
                   </div>
               </div>
               <div class="col-xs-12 col-sm-5 top-180-nosotros font-georgia">
                  <h2 class="title-blue-n">Valores</h2>
                  <p class="line-h-n">Yo Nómada es una organización que acerca a estudiantes y profesioniestas con quienes más necesitan de su experiencia y talento. Vive el trabajo social de forma diferente.</p>
               </div>
               <div class="col-xs-0 col-sm-2"></div>
               <div class="col-xs-12 col-sm-5 top-400-nosotros font-georgia">
                  <h2 class="title-blue-n2">Áreas Laborales</h2>
                  <p class="tex-right-new2 text-gray-general line-h-n">Yo Nómada es una organización que acerca a estudiantes y profesioniestas con quienes más necesitan de su experiencia y talento. Vive el trabajo social de forma diferente.</p>
                  <div class="ocultar-responsive">
                     <p class="tex-right-new2 text-gray-general color-area-l-no">Ciencias sociales</p>
                     <p class="tex-right-new2 text-gray-general color-area-l-no">Ciencias, Matemáticas e Ingenierías</p>
                     <p class="tex-right-new2 text-gray-general color-area-l-no">Artes, Diseño y Humanidades</p>
                     <p class="tex-right-new2 text-gray-general color-area-l-no">Biología, Química y Salud</p>
                  </div>
                     <div class="col-xs-0 col-sm-4"></div>
                       <div class="col-xs-0 col-sm-4"></div>
                      <div class="col-xs-12 col-sm-4">
                     <button type="button" class="tex-right-new2 family-font-robot btn btn-default btn-100 btn-color-blue general-hover">Ver más</button>
                     </div>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid padding-50-n-row">
         <div class="row row-nosotro2">
            <div class="col-xs-12 col-sm-12">
               <div class="col-xs-12 col-sm-5 top-180-nuestro font-georgia">
                  <h2 class="title-blue-n">Nuestro Objetivo</h2>
                  <p class="text-gray-general line-h-n">Yo Nómada es una organización que acerca a estudiantes y profesioniestas con quienes más necesitan de su experiencia y talento. Vive el trabajo social de forma diferente.</p>
               </div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-12 col-sm-12" style="padding-bottom: 80px;">
               <div class="col-sm-5 lef-nosotros ">
                  <h2 class="title-menu-2 text-gray">Nuestro equipo</h2>
                  <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
               </div>
               <div class="col-xs-12 col-sm-12 margin-tar-nosotros-1">
                 <div class="col-xs-12 col-sm-3">
                 <figure class="snip1527">
                        <div class="image"><img src="img/person-1.png" alt="pr-sample23" class="img-big-blog" /></div>
                        <figcaption>

                           <h3>Nombre</h3>
                           <p>
                              You know what we need, Hobbes? We need an attitude. Yeah, you can't be cool if you don't have an attitude.
                           </p>
                        </figcaption>
                        <a href="#"></a>
                     </figure>
                 </div>
                 <div class="col-xs-12 col-sm-3">
                 <figure class="snip1527">
                        <div class="image"><img src="img/person-2.png" alt="pr-sample23" class="img-big-blog" /></div>
                        <figcaption>

                           <h3>Nombre</h3>
                           <p>
                              You know what we need, Hobbes? We need an attitude. Yeah, you can't be cool if you don't have an attitude.
                           </p>
                        </figcaption>
                        <a href="#"></a>
                     </figure>
                 </div>
                 <div class="col-xs-12 col-sm-3">
                   <figure class="snip1527">
                        <div class="image"><img src="img/person-3.png" alt="pr-sample23" class="img-big-blog" /></div>
                        <figcaption>

                           <h3>Nombre</h3>
                           <p>
                              You know what we need, Hobbes? We need an attitude. Yeah, you can't be cool if you don't have an attitude.
                           </p>
                        </figcaption>
                        <a href="#"></a>
                     </figure>
                 </div>
                 <div class="col-xs-12 col-sm-3">
                   <figure class="snip1527">
                        <div class="image"><img src="img/person-4.png" alt="pr-sample23" class="img-big-blog" /></div>
                        <figcaption>

                           <h3>Nombre</h3>
                           <p>
                              You know what we need, Hobbes? We need an attitude. Yeah, you can't be cool if you don't have an attitude.
                           </p>
                        </figcaption>
                        <a href="#"></a>
                     </figure>
                 </div>
               </div>
            </div>
         </div>
      </div>


      <!-- Contacto Footer -->
      <div class="container-fluid" id="contacto">
        <?php include('formulario.php');?>
      </div>

      <!-- Footer-->
      <footer>
        <?php include('footer.php');?>
      </footer>

      <!-- Librerias JS -->
      <script src="js/main.js"></script>
      <script src="js/main_2.js"></script>
      <script src="js/slider.js"></script>
   </body>
</html>
