<div class="row row-btn">
  <div class="col-sm-12">
     <div class="col-sm-1"></div>
     <div class="col-sm-10 col-xs-12 text-white-title-col">
        <span class="text-white-title-center">¡Recibe noticias de las mejores oportunidades!</span>
        <div class="col-sm-12 col-md-5 col-pos-top">
           <input type="email" class="input-white margen-ingresar-correo-newsletter" name="useremail" placeholder="Ingrese correo">
        </div>
        <div class="col-sm-12 col-md-4 col-pos-top-2">
           <button type="button" class="btn btn-default btn-buscar-color "><span class="boton-space-2">SUSCRIBIRME</span></button>
        </div>
        <div class="col-sm-2"></div>
     </div>
     <div class="col-sm-1"></div>
  </div>
</div>
