<!DOCTYPE html>
<html>
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>

   <body>
      <header class="container-fluid header" id="conteiner-fluid-0">
        <div class="container">
            <!-- Menu -->
            <?php include('menu.php');?>
        </div>
         <!-- Rectangulo -->
         <div class="row">
            <div class="col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
                  <h1 class="text-center">DEJA TU HUELLA,</br>CREA UN IMPACTO</h1>
            </div>
         </div>

         <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 fondo-1">
               <div class="col-sm-12 fondo2">
                  <div class="col-sm-10 col-sm-offset-1 titulo-buscador">Comienza tu aventura seleccionando uno o m&aacute;s filtros:</div>
                  <div class="col-sm-12 padding0 text-align-general" id="responsive-menu-2-rec">

                      <div class="col-sm-3">
                          <select id="mounth">
                            <option value="hide">Áreas laborales</option>
                            <option value="area">Área 01</option>
                            <option value="area">Área 02</option>
                            <option value="area">Área 03</option>
                            <option value="area">Área 04</option>
                          </select>
                      </div>

                      <div class="col-sm-3">
                          <select id="mounth">
                            <option value="hide">Universidad</option>
                            <option value="universidad">Universidad 01</option>
                            <option value="universidad">Universidad 02</option>
                            <option value="universidad">Universidad 03</option>
                            <option value="universidad">Universidad 03</option>
                            <option value="universidad">Universidad 04</option>
                          </select>
                      </div>

                      <div class="col-sm-3">
                          <select id="mounth">
                            <option value="hide">Destino</option>
                            <option value="destino">Destino 01</option>
                            <option value="destino">Destino 02</option>
                            <option value="destino">Destino 03</option>
                            <option value="destino">Destino 04</option>
                            <option value="destino">Destino 05</option>
                          </select>
                      </div>

                      <div class="col-sm-3">
                          <select id="mounth">
                            <option value="hide">Estancia mínima</option>
                            <option value="estancia">Estancia 01</option>
                            <option value="estancia">Estancia 02</option>
                            <option value="estancia">Estancia 03</option>
                            <option value="estancia">Estancia 04</option>
                            <option value="estancia">Estancia 05</option>
                          </select>
                      </div>

                  </div>
               </div>
            </div>
         </div>

         <div class="row row-btn">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                 <div class="col-sm-4"></div>
                 <div class="col-sm-4"></div>
                 <div class="col-sm-4 bording-buttom-cafe">
                    <div class="rectangulo33">
                       <button type="button" class="btn btn-buscar-color" style="padding-top: 0px;padding-bottom: 0px;"><span class="boton-space">BUSCAR</span></button>
                    </div>
                 </div>
            </div>
         </div>

      </header>

      <!-- Primera seccion -->
      <div class="container-fluid">
         <div class="row row-btn">
            <div class="col-sm-12">
               <div class="col-sm-10 col-sm-offset-2">
                  <div class="col-xs-12 col-sm-3">
                     <div class="rec-blue-ninos"></div>
                     <img src="img/ninos.jpg" alt="ninos" class="img-responsive center-block margin-t-50">
                  </div>
                  <div class="col-sm-2"></div>
                  <div class="col-sm-7 columna-text-gray">
                     <span class="text-gray text-gray-general">Renovando el trabajo social en M&eacute;xico</span>
                     <div class="col-sm-4  col-xs-4 line-colo-cyan"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="container-fluid">
         <div class="row row-btn">
            <div class="col-sm-12">
               <div class="col-sm-5"></div>
               <div class="col-sm-6 columna-tex-gray-littel">
                  <span class="tex-gray-littel text-gray-general">Yo N&oacute;mada es una organizaci&oacute;n que acerca a estudiantes y profesionistas con quienes m&aacute;s necesitan de su experiencia y talento. Vive el trabajo social de forma diferente, emprende tu camino, transformando para siempre tu vida y la de los dem&aacute;s.</span></br></br>
                  <span class="tex-bold-gray text-gray-general"><big><b>Vu&eacute;lvete un N&oacute;mada, &uacute;nete a la comunidad.</b></big></span>
               </div>
               <div class="col-sm-1"></div>
            </div>
         </div>
      </div>

      <!-- Video -->
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="video-container">
                    <iframe width="640" height="360" src="https://www.youtube.com/embed/KuOSMUBXPJc" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
         </div>
      </div>

      <!-- Tercer seccion mapa-->
      <div class="container-fluid">
         <div class="row row-mapa ">
            <div class="col-sm-12">
               <h2 class="text-gray-2 text-gray-general text-mapa-padding">Explora el mapa para conocer todas las oportunidades:</h2>
               <div class="col-sm-9 mapa-padding">
                  <img src="img/mapa.png" alt="Video Yo nomada" class="img-responsive center-block margin-t-50">
                  <div class="col-sm-5 col-xs-12 ocultar-div">
                     <div class="rectangulo-mapa">
                        <div class="spanrectangulo">
                           <i class="fa fa-heartbeat fa-lg width-icon-rec" aria-hidden="true"></i><span class="padding-rec-span">SALUD ENTRE HERMANOS</span>
                        </div>
                        <div class="spanrectangulo">
                           <i class="fa fa-map-marker fa-lg width-icon-rec" aria-hidden="true"></i><span class="padding-rec-span">Aguascalientes, Aguscalientes</span>
                        </div>
                        <div class="spanrectangulo">
                           <i class="fa fa-suitcase fa-lg" aria-hidden="true"></i><span class="padding-rec-span">· Biología,química y salud</span>
                        </div>
                        <div class="spanrectangulo">
                           <i class="fa fa fa-flask fa-lg" aria-hidden="true"></i><span class="padding-rec-span">· Ciencias, matemáticas e ingenierías</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 col-xs-12 margen-check ocultar-div">
                  <div id="responsive-col-2">
                     <div class="checkbox">
                        <input id="box1" type="checkbox" />
                        <label for="box1"><span class="text-checkbox"><i class="fa fa-user-circle-o margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i> Integración Social (1)</span></label>
                     </div>
                     <div class="checkbox">
                        <input id="box2" type="checkbox" />
                        <label for="box2"><span class="text-checkbox"><i class="fa fa-users margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Derechos humanos (3) </span></label>
                     </div>
                     <div class="checkbox">
                        <input id="box3" type="checkbox" />
                        <label for="box3"><span class="text-checkbox"><i class="fa fa-paint-brush margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Arte y cultura (2)</span></label>
                     </div>
                     <div class="checkbox">
                        <input id="box4" type="checkbox" />
                        <label for="box4"><span class="text-checkbox"><i class="fa fa-graduation-cap margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Educación (2)</span></label>
                     </div>
                     <div class="checkbox">
                        <input id="box5" type="checkbox" />
                        <label for="box5"><span class="text-checkbox"><i class="fa fa-heartbeat margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>
                        Salud y nutrición  (1)</span></label>
                     </div>
                     <div class="checkbox">
                        <input id="box6" type="checkbox" />
                        <label for="box6"><span class="text-checkbox"><i class="fa fa-pagelines margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Agricultura (4)</span></label>
                     </div>
                     <div class="checkbox">
                        <input id="box7" type="checkbox" />
                        <label for="box7"><span class="text-checkbox"><i class="fa fa-male margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Tercera edad (5)</span></label>
                     </div>
                     <div class="checkbox">
                        <input id="box8" type="checkbox" />
                        <label for="box8"><span class="text-checkbox"><i class="fa fa-exclamation-circle margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Contingencias (2)</span></label>
                     </div>
                     <div class="checkbox">
                        <input id="box9" type="checkbox" />
                        <label for="box9"><span class="text-checkbox"><i class="fa fa-building margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>
                        Construcción (2)</span></label>
                     </div>
                     <div class="checkbox">
                        <input id="box10" type="checkbox" />
                        <label for="box10"><span class="text-checkbox"><i class="fa fa-leaf margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Medio ambiente (4)</span></label>
                     </div>
                     <div class="checkbox">
                        <input id="box11" type="checkbox" />
                        <label for="box11"><span class="text-checkbox"><i class="fa fa-clone margin-left-icon-check-new iconos-mapa" aria-hidden="true"></i>Otros (3)</span></label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Cuarta seccion Areas laborales-->
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12 text-gray-3">
               <h2 class="text-gray-2 text-gray-general">&Aacute;reas Laborales</h2>
            </div>

            <div class="col-sm-12">
               <div class="col-sm-1"></div>
               <div class="col-sm-10 tex-align-center top-title-new-index-laborales">
                  <span class="text-black"><b>CIENCIAS SOCIALES</b></span>
               </div>
               <div class="col-sm-1"></div>
            </div>

            <div class="col-xs-12 col-sm-12">
               <div class="col-sm-1"></div>


               <div class="col-xs-6 col-sm-2">
                    <a href="#">
                        <img src="img/administracion.jpg" alt="Administración" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                        <div class="centrado">ADMINISTRACI&Oacute;N Y ECONOM&Iacute;A</div>
                    </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                    <a href="#">
                        <img src="img/comunicacion.jpg" alt="Comuniación" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                        <div class="centrado">COMUNICACI&Oacute;N</div>
                    </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                    <a href="#">
                        <img src="img/derecho.jpg" alt="Derecho" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                        <div class="centrado">DERECHO</div>
                    </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                    <a href="#">
                        <img src="img/relaciones.jpg" alt="Relaciones internaciobales" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                        <div class="centrado">RELACIONES INTERNACIONALES</div>
                    </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                    <a href="#">
                        <img src="img/sociales.jpg" alt="Sociología" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                        <div class="centrado">SOCIOLOG&Iacute;A</div>
                    </a>
               </div>

               <div class="col-sm-1"></div>
            </div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row row-btn">

            <div class="col-sm-12">
               <div class="col-sm-1"></div>
               <div class="col-sm-10 tex-align-center top-title-new-index-laborales">
                  <span class="text-black "><b>CIENCIAS, MATEM&Aacute;TICAS E INGENIER&Iacute;AS</b></span>
               </div>
               <div class="col-sm-1"></div>
            </div>

            <div class="col-xs-12 col-sm-12">
               <div class="col-sm-1"></div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/arquitectura.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <p class="centrado">ARQUITECTURA Y URBANISMO</p>
                  </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/tierra.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">CIENCIAS DE LA TIERRA</div>
                  </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/ingenierias.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">INGENIER&Iacute;AS</div>
                  </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/matematicas.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">MATEM&Aacute;TICAS</div>
                  </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/tecnologia.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">TECNOLOG&Iacute;A</div>
                  </a>
               </div>

               <div class="col-sm-1"></div>
            </div>

         </div>
      </div>
      <div class="container-fluid">
         <div class="row row-btn">
            <div class="col-xs-12 col-sm-12">
               <div class="col-sm-1"></div>
               <div class="col-sm-10 tex-align-center top-title-new-index-laborales">
                  <span class="text-black"><b>ARTES, DISEÑO Y HUMANIDADES</b></span>
               </div>
               <div class="col-sm-1"></div>
            </div>

            <div class="col-sm-12">
               <div class="col-sm-1"></div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/arte.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">ARTE Y DISEÑO</div>
                  </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/literatura.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">LENGUAS Y LITERATURA</div>
                  </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/filosofia.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">FILOSOF&Iacute;A</div>
                  </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/historia.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">HISTOR&Iacute;A</div>
                  </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/pedagogia.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">PEDAGOG&Iacute;A</div>
                  </a>
               </div>

               <div class="col-sm-1"></div>
            </div>

         </div>
      </div>
      <div class="container-fluid">
         <div class="row row-btn">
            <div class="col-xs-12 col-sm-12">
               <div class="col-sm-1"></div>
               <div class="col-sm-10 tex-align-center top-title-new-index-laborales">
                  <span class="text-black text-black-general"><b>BIOLOG&Iacute;A, QU&Iacute;MICA Y SALUD</b></span>
               </div>
               <div class="col-sm-1"></div>
            </div>

            <div class="col-sm-12">
               <div class="col-sm-1"></div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/medicina.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">MEDICINA</div>
                  </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/psicologia.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">PSICOLOG&Iacute;A</div>
                  </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/agronomia.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">AGRONOM&Iacute;A Y ALIMENTOS</div>
                  </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/quimica.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">QU&Iacute;MICA</div>
                  </a>
               </div>

               <div class="col-xs-6 col-sm-2">
                  <a href="#">
                      <img src="img/biologia.jpg" alt="Administrac&iacute;on" class="img-responsive center-block padding-img-laborales hover-areas-laborales">
                      <div class="centrado">BIOLOG&Iacute;A Y MEDIO AMBIENTE</div>
                  </a>
               </div>

               <div class="col-sm-1"></div>
            </div>

         </div>
      </div>
      <!-- Quinta seccion Proyectos-->
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12 text-gray-3">
               <h2 class="text-gray-2 text-gray-general ">Proyectos destacados</h2>
            </div>
            <div class="col-sm-10 col-sm-offset-1">

               <div class="col-sm-5 col-xs-12">
                   <div class="video-container" id="video-proyectos">
                       <iframe width="640" height="360" src="https://www.youtube.com/embed/KuOSMUBXPJc" frameborder="0" allowfullscreen></iframe>
                   </div>
               </div>

               <div class="col-sm-7 col-xs-12">
                  <span class="text-black-2 text-black-general">NOMBRE DEL PROYECTO</span></br>
                  <span class="text-gray-4 text-gray-general">Subt&iacute;tulo del proyecto (40 caracteres max.)</span></br></br>
                  <div id="icon-padding">
                     <div class="icon-padding">
                        <i class="fa fa-map-marker fa-lg icon-color icon-center" aria-hidden="true"></i>
                        <span class="text-gray-4 text-gray-general">Palenque, Chiapas</span>
                     </div>
                     <div class="icon-padding">
                        <i class="fa fa-suitcase fa-lg icon-color icon-center" aria-hidden="true"></i>
                        <span class="text-gray-4 text-gray-general">Artes, diseño y humanidades / Biolog&iacute;a, qu&iacute;mica y salud</span>
                     </div>
                     <div class="icon-padding">
                        <i class="fa fa-key fa-lg icon-color icon-center" aria-hidden="true"></i>
                        <span class="text-gray-4 text-gray-general">#cultivos  #educaci&oacute;n  #niños  #alimentos  #desnutrici&oacute;n</span>
                     </div>
                     </br>
                     <span class="text-gray-5 text-gray-general">¡Explora m&aacute;s proyectos!</span>
                  </div>

                  <!-- Slider -->
                  <div class="owl-carousel">
                     <div class="container-slider item">
                        <img src="img/slider-2.jpg" alt="Avatar" class="image" style="width:100%">
                        <div class="middle">
                          <div class="text-slider">
                              ENSEÑANDO A TRAVÉS DEL DEPORTE<br><br>
                              <a href="#"><btn class="btn-slider-proyecto">Ver Proyecto</btn></a>
                          </div>
                        </div>
                     </div>

                     <div class="container-slider item">
                        <img src="img/slider-3.jpg" alt="Avatar" class="image" style="width:100%">
                        <div class="middle">
                          <div class="text-slider">
                              ENSEÑANDO A TRAVÉS DEL DEPORTE<br><br>
                              <a href="#"><btn class="btn-slider-proyecto">Ver Proyecto</btn></a>
                          </div>
                        </div>
                     </div>

                     <div class="container-slider item">
                        <img src="img/slider-1.jpg" alt="Avatar" class="image" style="width:100%">
                        <div class="middle">
                          <div class="text-slider">
                              ENSEÑANDO A TRAVÉS DEL DEPORTE<br><br>
                              <a href="#"><btn class="btn-slider-proyecto">Ver Proyecto</btn></a>
                          </div>
                        </div>
                     </div>

                     <div class="container-slider item">
                        <img src="img/slider-2.jpg" alt="Avatar" class="image" style="width:100%">
                        <div class="middle">
                          <div class="text-slider">
                              ENSEÑANDO A TRAVÉS DEL DEPORTE<br><br>
                              <a href="#"><btn class="btn-slider-proyecto">Ver Proyecto</btn></a>
                          </div>
                        </div>
                     </div>

                     <div class="container-slider item">
                        <img src="img/slider-2.jpg" alt="Avatar" class="image" style="width:100%">
                        <div class="middle">
                          <div class="text-slider">
                              ENSEÑANDO A TRAVÉS DEL DEPORTE<br><br>
                              <a href="#"><btn class="btn-slider-proyecto">Ver Proyecto</btn></a>
                          </div>
                        </div>
                     </div>

                  </div>

               </div>
            </div>
         </div>
      </div>

      <!--Sexta seccion Testimonios-->
      <div class="container-fluid">
         <div class="row row-testimonios">
            <div class="col-sm-12 text-gray-3">
               <h2 class="text-gray-2 text-gray-general">Testimonios</h2>
            </div>








            <!-- Swiper -->
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="col-sm-12 text-center">
                            <img src="img/Testimonios/testimonios-1.png">
                            <p>
                            <b><big>NOMBRE DEL PROYECTO</big></b><br>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis viverra faucibus mauris, vitae molestie libero pulvinar a.
                            Donec efficitur lorem quam, ut tempus mauris efficitur nec. Curabitur placerat tristique metus, a varius leo suscipit a.
                            Integer vel volutpat justo.<br>
                            <small>Mariana Gómez<br>
                            Universidad ITAM</small>
                            </p>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="col-sm-12 text-center">
                            <img src="img/Testimonios/testimonios-2.png">
                            <p>
                            <b><big>NOMBRE DEL PROYECTO</big></b><br>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis viverra faucibus mauris, vitae molestie libero pulvinar a.
                            Donec efficitur lorem quam, ut tempus mauris efficitur nec. Curabitur placerat tristique metus, a varius leo suscipit a.
                            Integer vel volutpat justo.<br>
                            <small>Mariana Gómez<br>
                            Universidad ITAM</small>
                            </p>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="col-sm-12 text-center">
                            <img src="img/Testimonios/testimonios-3.png">
                            <p>
                            <b><big>NOMBRE DEL PROYECTO</big></b><br>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis viverra faucibus mauris, vitae molestie libero pulvinar a.
                            Donec efficitur lorem quam, ut tempus mauris efficitur nec. Curabitur placerat tristique metus, a varius leo suscipit a.
                            Integer vel volutpat justo.<br>
                            <small>Mariana Gómez<br>
                            Universidad ITAM</small>
                            </p>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="col-sm-12 text-center">
                            <img src="img/Testimonios/testimonios-1.png">
                            <p>
                            <b><big>NOMBRE DEL PROYECTO</big></b><br>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis viverra faucibus mauris, vitae molestie libero pulvinar a.
                            Donec efficitur lorem quam, ut tempus mauris efficitur nec. Curabitur placerat tristique metus, a varius leo suscipit a.
                            Integer vel volutpat justo.<br>
                            <small>Mariana Gómez<br>
                            Universidad ITAM</small>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>

         </div>
      </div>

      <!-- Septima seccion suscribirme-->
      <div class="container-fluid recibe-noticias-backgroud">
        <?php include('newsletter.php');?>
      </div>
      <!-- Octova seccion Noticias-->
      <div class="container-fluid">
         <div class="row row-btn">
            <div class="col-sm-12 text-gray-3">
               <h2 class="text-gray-2 text-gray-general">Noticias</h2>
            </div>
            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1">

               <a href="blog_interno.php">
                   <div class="col-sm-4">
                      <div class="my-2 mx-auto p-relative bg-white shadow-1 blue-hover size-car-1">
                         <img src="img/noticias-1.jpg" alt="Man with backpack" class="d-block w-full">
                         <div class="px-2 py-2">
                            <h1 class="ff-serif font-weight-normal text-black-new22 card-heading mt-0 mb-1 spacing titulo-blog-inicio" style="line-height: 1.25;">
                               Título de la noticia
                            </h1>
                            <p class="mb-1 mb-6-color">Autor</p>
                            <p class="mb-1 mb-5-color text-posi-noticia">
                               Summer is coming to a close just around the corner. But it's not too late to squeeze in another weekend trip &hellip;
                            </p>
                         </div>
                         <a href="#0" class="text-uppercase d-inline-block font-weight-medium lts-2px ml-2 mb-2 text-center styled-link">Categoría</a>
                      </div>
                   </div>
               </a>

               <a href="blog_interno.php">
                   <div class="col-sm-4">
                      <div class="my-2 mx-auto p-relative bg-white shadow-1 blue-hover size-car-1">
                         <img src="img/noticias-2.jpg" alt="Man with backpack" class="d-block w-full">
                         <div class="px-2 py-2">
                            <h1 class="ff-serif font-weight-normal text-black-new22 card-heading mt-0 mb-1 spacing titulo-blog-inicio" style="line-height: 1.25;">
                               Título de la noticia
                            </h1>
                            <p class="mb-1 mb-6-color">Autor</p>
                            <p class="mb-1 mb-5-color text-posi-noticia">
                               Summer is coming to a close just around the corner. But it's not too late to squeeze in another weekend trip &hellip;
                            </p>
                         </div>
                         <a href="#0" class="text-uppercase d-inline-block font-weight-medium lts-2px ml-2 mb-2 text-center styled-link">Categoría
                         </a>
                      </div>
                   </div>
               </a>

               <a href="blog_interno.php">
                   <div class="col-sm-4">
                      <div class="my-2 mx-auto p-relative bg-white shadow-1 blue-hover">
                         <img src="img/noticias-3.jpg" alt="Man with backpack" class="d-block w-full">
                         <div class="px-2 py-2">
                            <h1 class="ff-serif font-weight-normal text-black-new22 card-heading mt-0 mb-1 spacing titulo-blog-inicio" style="line-height: 1.25;">
                               Título de la noticia
                            </h1>
                            <p class="mb-1 mb-6-color">Autor</p>
                            <p class="mb-1 mb-5-color text-posi-noticia">
                               Summer is coming to a close just around the corner. But it's not too late to squeeze in another weekend trip &hellip;
                            </p>
                         </div>
                         <a href="#0" class="text-uppercase d-inline-block font-weight-medium lts-2px ml-2 mb-2 text-center styled-link">Categoría
                         </a>
                      </div>
                      <div class="col-sm-6"></div>
                      <div class="col-sm-6 px-0 ">
                          <a href="blog.php">
                              <button type="button" class="btn btn-default btn-100 btn-color-blue general-hover">Ver más noticias</button>
                          </a>
                      </div>
                   </div>
               </a>

            </div>
         </div>
      </div>

      <!-- Contacto Footer -->
      <div class="container-fluid" id="contacto">
        <?php include('formulario.php');?>
      </div>

      <!-- Footer-->
      <footer>
        <?php include('footer.php');?>
      </footer>

      <!-- Librerias JS -->
      <script src="js/main.js"></script>
      <script src="js/main_2.js"></script>
      <script src='https://code.jquery.com/jquery-1.11.1.min.js'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.js'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js'></script>
      <script  src="js/index.js"></script>


      <!-- Initialize Swiper -->
      <script>
      var swiper = new Swiper('.swiper-container', {
          pagination: '.swiper-pagination',
          slidesPerView: 4,
          centeredSlides: true,
          paginationClickable: true,
          spaceBetween: 30
      });
      </script>

      <!-- Select Header buscador -->
      <script>
      $('select').each(function(){
      var $this = $(this), numberOfOptions = $(this).children('option').length;
      $this.addClass('select-hidden');
      $this.wrap('<div class="select"></div>');
      $this.after('<div class="select-styled"></div>');
      var $styledSelect = $this.next('div.select-styled');
      $styledSelect.text($this.children('option').eq(0).text());
      var $list = $('<ul />', {
          'class': 'select-options'
      }).insertAfter($styledSelect);
      for (var i = 0; i < numberOfOptions; i++) {
          $('<li />', {
              text: $this.children('option').eq(i).text(),
              rel: $this.children('option').eq(i).val()
          }).appendTo($list);
      }
      var $listItems = $list.children('li');
      $styledSelect.click(function(e) {
          e.stopPropagation();
          $('div.select-styled.active').not(this).each(function(){
              $(this).removeClass('active').next('ul.select-options').hide();
          });
          $(this).toggleClass('active').next('ul.select-options').toggle();
      });
      $listItems.click(function(e) {
          e.stopPropagation();
          $styledSelect.text($(this).text()).removeClass('active');
          $this.val($(this).attr('rel'));
          $list.hide();
          //console.log($this.val());
      });
      $(document).click(function() {
          $styledSelect.removeClass('active');
          $list.hide();
      });
      });
      </script>
   </body>
</html>
