<div class="row row-btn titulo-buscador1">
    <div class="col-xs-12 col-sm-12">
        <div class="col-sm-3 col-xs-6" id="ocultar-logo">
            <a href="index.php"><img src="img/logo.png" alt="Logo Yo nomada" class="img-responsive center-block"></a>
        </div>
        <div class="col-sm-9 text-right" id="columna-100-p">
            <div class="col-sm-12 col-xs-5" id="icon-menu-responsive">
                <div class="col-sm-5 col-xs-5"></div>

                <div class="col-sm-3 col-xs-12" id="col-responsive">
                    <span class="fa-stack fa-1x">
                        <i class="fa fa-circle fa-stack-2x color-icon-header2"></i>
                        <i class="cursor-pointer1 fa fa-instagram fa-stack-1x fa-inverse color-icon-header color-icon-header-new general-hover"></i>
                    </span>
                    <span class="fa-stack fa-1x">
                        <i class="fa fa-circle fa-stack-2x color-icon-header2"></i>
                        <i class="cursor-pointer1 fa fa-facebook fa-stack-1x fa-inverse color-icon-header color-icon-header-new general-hover"></i>
                    </span>
                    <span class="fa-stack fa-1x">
                        <i class="fa fa-circle fa-stack-2x color-icon-header2"></i>
                        <i class="cursor-pointer1 fa fa-linkedin fa-stack-1x fa-inverse color-icon-header color-icon-header-new general-hover"></i>
                    </span>
                </div>
           
                <div class="col-sm-4 col-xs-4" id="col-responsive-search">
                    <div id="wrap">
                        <form action="" autocomplete="on">
                            <input id="search" name="search" type="text" placeholder="Buscar"><input id="search_submit" value="Rechercher" type="submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-9 top-columna-menu-2 topnav" id="mostrar-menu-prinicpal">
            <a href="index.php" class="font-size-menu-ipad general-hover">Inicio</a>
            <a href="areas_laborales.php" class="font-size-menu-ipad general-hover">&Aacute;reas Laborales</a>
            <a href="proyectos.php" class="font-size-menu-ipad general-hover">Proyectos</a>
            <a href="galeria.php" class="font-size-menu-ipad general-hover">Galer&iacute;a</a>
            <a href="blog.php" class="font-size-menu-ipad general-hover">Blog</a>
            <a href="nosotros.php" class="font-size-menu-ipad general-hover">Nosotros</a>
            <a href="index.php#contacto" class="font-size-menu-ipad general-hover">Contacto</a>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;</a>
        </div>
        <!-- Menu movil -->
        <div class="ocultar-menu-reponsive-2">
            <div id="sideNavigation" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <a href="index.php">Inicio</a>
                <a href="areas_laborales.php">&Aacute;reas Laborales</a>
                <a href="proyectos.php">Proyectos</a>
                <a href="galeria.php">Galer&iacute;a</a>
                <a href="blog.php">Blog</a>
                <a href="nosotros.php">Nosotros</a>
                <a href="index.php#contacto">Contacto</a>
                <a class="col-sm-4 col-xs-5 hover-cancel" id="col-responsive-search">
                    <div id="wrap">
                        <form action="" autocomplete="on">
                            <input id="search" name="Buscar" type="text" placeholder="Buscar" style="width: 100%;position: absolute;" ><input id="search_submit" value="Rechercher" type="submit" >
                        </form>
                    </div>
                </a>
                <div class="col-sm-3 col-xs-12 hover-cancel" id="col-responsive">
                    <span class="fa-stack fa-1x">
                        <i class="fa fa-circle fa-stack-2x color-icon-header2 hover-icon-hover"></i>
                        <i class="cursor-pointer1 fa fa-instagram fa-stack-1x fa-inverse color-icon-header color-icon-header-new general-hover"></i>
                    </span>
                    <span class="fa-stack fa-1x">
                        <i class="fa fa-circle fa-stack-2x color-icon-header2 hover-icon-hover"></i>
                        <i class="cursor-pointer1 fa fa-facebook fa-stack-1x fa-inverse color-icon-header-new general-hover"></i>
                    </span>
                    <span class="fa-stack fa-1x">
                        <i class="fa fa-circle fa-stack-2x color-icon-header2 hover-icon-hover"></i>
                        <i class="cursor-pointer1 fa fa-linkedin fa-stack-1x fa-inverse color-icon-header-new general-hover"></i>
                    </span>
                </div>
            </div>
            <nav class="topnav2">
                <a class="hover-cancel" href="#" onclick="openNav()" style="padding: 0px;">
                    <img src="img/logo.png" alt="Logo Yo nomada" class="menu-responsive-logo-size img-responsive center-block">
                    <svg width="25" height="25" id="icoOpen" class="hover-active">
                        <path d="M0,5 30,5" stroke="#ffffff" stroke-width="3"/>
                        <path d="M0,14 30,14" stroke="#ffffff" stroke-width="3"/>
                        <path d="M0,23 30,23" stroke="#ffffff" stroke-width="3"/>
                    </svg>
                </a>
            </nav>
        </div>
    </div>
</div>
