<!DOCTYPE html>
<html>
    <head>
    <!-- Librerias -->
    <?php include('head.php');?>
    </head>
    
    <body class="body2">
        <header class="container-fluid header4" id="conteiner-fluid-0">
            <div class="container">
                <!-- Menu -->
                <?php include('menu.php');?>
            </div>
            <div class="row row-new-2">
                <div class="col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
                    <div class="col-sm-12">
                        <h1 class="text-center">¡ENCUENTRA TU PRÓXIMA AVENTURA!</h1>
                    </div>
                </div>
            </div>
        </header>
        <div class="conteiner-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12" style="padding-bottom: 11px;">
                    <div class="col-xs-12 col-sm-3">
                        <p class="spanfiltros" id="hide-btn">OCULTAR FILTROS</p>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="col-xs-0 col-sm-5" style="padding-left: 0;">
                            <div class="col-xs-12 col-sm-5" style="padding-left: 0;">
                                <p class="text-black-title-new2">ORDENAR POR:</p>
                            </div>
                            <div class="col-xs-12 col-sm-7">
                                <div class="custom-select" style="width:100%;top: -9px;">
                                    <select>
                                        <option value="0">Recientes</option>
                                        <option value="1">Opción 1</option>
                                        <option value="2">Opción 2</option>
                                        <option value="3">Opción 3</option>
                                        <option value="4">Opción 4</option>
                                        <option value="5">Opción 5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3"></div>
                        <div class="col-xs-12 col-sm-3" id="">
                            <div id="wrap">
                                <form action="" autocomplete="on">
                                    <input id="search" name="Buscar" type="text" placeholder="Buscar..." style="width: 100%;position: absolute;color: #5f5f5f;" ><input id="search_submit2" value="Rechercher" type="submit" class="img-pruebadd" >
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12">
                    <!-- FILTROS -->
                    <div class="col-xs-12 col-sm-3" id="mielemento">
                        <div class="col-xs-12 col-sm-12 contenerdo-white-1 shadow-box">
                            <div class="col-xs-12 col-sm-12">
                                <h2 class="title-menu-2 text-gray">FILTROS</h2>
                                <div class="col-sm-6  col-xs-4 line-colo-cyan"></div>
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                <p class="text-black-filtros">Área laboral</p>
                                <div class="custom-select" style="width:100%;">
                                    <select>
                                        <option value="0">Selecciona una opcion</option>
                                        <option value="1">Opción 1</option>
                                        <option value="2">Opción 2</option>
                                        <option value="3">Opción 3</option>
                                        <option value="4">Opción 4</option>
                                        <option value="5">Opción 5</option>
                                    </select>
                                </div>
                                <p class="text-black-filtros">Departamento</p>
                                <div class="custom-select" style="width:100%;">
                                    <select>
                                        <option value="0">Selecciona una opcion</option>
                                        <option value="1">Opción 1</option>
                                        <option value="2">Opción 2</option>
                                        <option value="3">Opción 3</option>
                                        <option value="4">Opción 4</option>
                                        <option value="5">Opción 5</option>
                                    </select>
                                </div>
                                <p class="text-black-filtros">Carrera</p>
                                <div class="custom-select" style="width:100%;">
                                    <select>
                                        <option value="0">Selecciona una opcion</option>
                                        <option value="1">Opción 1</option>
                                        <option value="2">Opción 2</option>
                                        <option value="3">Opción 3</option>
                                        <option value="4">Opción 4</option>
                                        <option value="5">Opción 5</option>
                                    </select>
                                </div>
                                <p class="text-black-filtros">Universidad</p>
                                <div class="custom-select" style="width:100%;">
                                    <select>
                                        <option value="0">Selecciona una opcion</option>
                                        <option value="1">Opción 1</option>
                                        <option value="2">Opción 2</option>
                                        <option value="3">Opción 3</option>
                                        <option value="4">Opción 4</option>
                                        <option value="5">Opción 5</option>
                                    </select>
                                </div>
                                <p class="text-black-filtros">Semestre mínimo</p>
                                <div class="custom-select" style="width:100%;">
                                    <select>
                                        <option value="0">Selecciona una opcion</option>
                                        <option value="1">Opción 1</option>
                                        <option value="2">Opción 2</option>
                                        <option value="3">Opción 3</option>
                                        <option value="4">Opción 4</option>
                                        <option value="5">Opción 5</option>
                                    </select>
                                </div>
                                <p class="text-black-filtros">Estancia mínima</p>
                                <p class="text-black-filtros tex-range-dias">(Días)</p>
                                <div class="rangeslider-wrap">
                                    <input type="range" min="1" max="100" labels="">
                                </div>
                                <p class="text-black-filtros col-cleal-filtros" style="margin-bottom: 20px;">Limpiar filtros</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-9">
                        <div class="col-xs-12 col-sm-12 contenerdo-white-2 shadow-box">
                            <div class="col-xs-12 col-sm-12">
                                <div class="col-xs-12 col-sm-12">
                                    <h2 class="title-menu-tarj-2 text-gray">¡Explorar los proyectos!</h2>
                                </div>

                                <div class="col-xs-12 col-sm-3 new-class-padding-0-q explorar_proyecto">
                                    <div class="my-2 mx-auto p-relative bg-white shadow-1 size-car-1">
                                        <div>
                                            <p class="centrado3-new">UN MEJOR ECOSISTEMA PARA LAS TORTUGAS</p>
                                            <img src="img/slider-1.jpg" alt="Man with backpack" class="d-block w-full-tar">
                                            <p class="centrado4-new">#tortugas #biodiversidad #ecología #mar</p>
                                        </div>
                                        <div class="px-2 py-2">
                                            <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-map-marker width-30-px-new" aria-hidden="true"></i>Riviera Maya, Quintana Roo</p>
                                            <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-suitcase width-30-px-new" aria-hidden="true"></i>Biología, química y salud/Área laboral adicional(hasta 3)</p>
                                            <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-calendar width-30-px-new" aria-hidden="true"></i>Estancia mínima:30 días</p>
                                            <p class="mb-1 mb-5-color text-posi-noticia font-size-tarje-new"><i class="fa fa-graduation-cap width-30-px-new" aria-hidden="true"></i>5to semestre en adelante</p>
                                        </div>
                                        <p class="mb-1 mb-6-color22 centrar-text-tarjeta">Avalado por:</p>
                                        <div class="swiper-container swiper-container3">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide swiper-container-3"><img src="img/ibero.jpg" class="slider-3"/></div>
                                                <div class="swiper-slide swiper-container-3"><img src="img/logo-itam.png" class="slider-3"/></div>
                                                <div class="swiper-slide swiper-container-3"><img src="img/logouam.gif" class="slider-3"/></div>
                                            </div>
                                            <!-- Add Pagination -->
                                            <div class="swiper-pagination bottom-gray-slider"></div>
                                            <!-- Add Arrows -->
                                            <div class="swiper-button-next bottom-gray-slider"></div>
                                            <div class="swiper-button-prev bottom-gray-slider"></div>
                                        </div>
                                        <button type="button" class="btn btn-default btn-buscar-color2-new general-hover">VER PROYECTO</button>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-3 new-class-padding-0-q explorar_proyecto">
                                    <div class="my-2 mx-auto p-relative bg-white shadow-1 size-car-1">
                                        <div>
                                            <p class="centrado3-new">CULTIVO DE CAFÉ EN CHIAPAS</p>
                                            <img src="img/cultivo-tarj.jpg" alt="Man with backpack" class="d-block w-full-tar">
                                            <p class="centrado4-new">#hashtag #hashtag #hashtag #hashtag </p>
                                        </div>
                                        <div class="px-2 py-2">
                                            <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-map-marker width-30-px-new" aria-hidden="true"></i>Riviera Maya, Quintana Roo</p>
                                            <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-suitcase width-30-px-new" aria-hidden="true"></i>Biología, química y salud/Área laboral adicional(hasta 3)</p>
                                            <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-calendar width-30-px-new" aria-hidden="true"></i>Estancia mínima:30 días</p>
                                            <p class="mb-1 mb-5-color text-posi-noticia font-size-tarje-new"><i class="fa fa-graduation-cap width-30-px-new" aria-hidden="true"></i>5to semestre en adelante</p>
                                        </div>
                                        <p class="mb-1 mb-6-color22 centrar-text-tarjeta">Avalado por:</p>
                                        <div class="swiper-container swiper-container3">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide swiper-container-3"><img src="img/ibero.jpg" class="slider-3"/></div>
                                                <div class="swiper-slide swiper-container-3"><img src="img/logo-itam.png" class="slider-3"/></div>
                                                <div class="swiper-slide swiper-container-3"><img src="img/logouam.gif" class="slider-3"/></div>
                                            </div>
                                            <!-- Add Pagination -->
                                            <div class="swiper-pagination bottom-gray-slider"></div>
                                            <!-- Add Arrows -->
                                            <div class="swiper-button-next bottom-gray-slider"></div>
                                            <div class="swiper-button-prev bottom-gray-slider"></div>
                                        </div>
                                        <button type="button" class="btn btn-default btn-buscar-color2-new general-hover">VER PROYECTO</button>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-3 new-class-padding-0-q explorar_proyecto">
                                    <div class="my-2 mx-auto p-relative bg-white shadow-1 size-car-1">
                                        <div>
                                            <p class="centrado3-new">CULTIVO DE CAFÉ EN CHIAPAS</p>
                                            <img src="img/cultivo-tarj.jpg" alt="Man with backpack" class="d-block w-full-tar">
                                            <p class="centrado4-new">#hashtag #hashtag #hashtag #hashtag </p>
                                        </div>
                                        <div class="px-2 py-2">
                                            <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-map-marker width-30-px-new" aria-hidden="true"></i>Riviera Maya, Quintana Roo</p>
                                            <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-suitcase width-30-px-new" aria-hidden="true"></i>Biología, química y salud/Área laboral adicional(hasta 3)</p>
                                            <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-calendar width-30-px-new" aria-hidden="true"></i>Estancia mínima:30 días</p>
                                            <p class="mb-1 mb-5-color text-posi-noticia font-size-tarje-new"><i class="fa fa-graduation-cap width-30-px-new" aria-hidden="true"></i>5to semestre en adelante</p>
                                        </div>
                                        <p class="mb-1 mb-6-color22 centrar-text-tarjeta">Avalado por:</p>
                                        <div class="swiper-container swiper-container3">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide swiper-container-3"><img src="img/ibero.jpg" class="slider-3"/></div>
                                                <div class="swiper-slide swiper-container-3"><img src="img/logo-itam.png" class="slider-3"/></div>
                                                <div class="swiper-slide swiper-container-3"><img src="img/logouam.gif" class="slider-3"/></div>
                                            </div>
                                            <!-- Add Pagination -->
                                            <div class="swiper-pagination bottom-gray-slider"></div>
                                            <!-- Add Arrows -->
                                            <div class="swiper-button-next bottom-gray-slider"></div>
                                            <div class="swiper-button-prev bottom-gray-slider"></div>
                                        </div>
                                        <button type="button" class="btn btn-default btn-buscar-color2-new general-hover">VER PROYECTO</button>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-3 new-class-padding-0-q explorar_proyecto">
                                    <div class="my-2 mx-auto p-relative bg-white shadow-1 size-car-1">
                                        <div>
                                            <p class="centrado3-new">CULTIVO DE CAFÉ EN CHIAPAS</p>
                                            <img src="img/cultivo-tarj.jpg" alt="Man with backpack" class="d-block w-full-tar">
                                            <p class="centrado4-new">#hashtag #hashtag #hashtag #hashtag </p>
                                        </div>
                                        <div class="px-2 py-2">
                                            <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-map-marker width-30-px-new" aria-hidden="true"></i>Riviera Maya, Quintana Roo</p>
                                            <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-suitcase width-30-px-new" aria-hidden="true"></i>Biología, química y salud/Área laboral adicional(hasta 3)</p>
                                            <p class="mb-1 mb-6-color22 font-size-tarje-new"><i class="fa fa-calendar width-30-px-new" aria-hidden="true"></i>Estancia mínima:30 días</p>
                                            <p class="mb-1 mb-5-color text-posi-noticia font-size-tarje-new"><i class="fa fa-graduation-cap width-30-px-new" aria-hidden="true"></i>5to semestre en adelante</p>
                                        </div>
                                        <p class="mb-1 mb-6-color22 centrar-text-tarjeta">Avalado por:</p>
                                        <div class="swiper-container swiper-container3">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide swiper-container-3"><img src="img/ibero.jpg" class="slider-3"/></div>
                                                <div class="swiper-slide swiper-container-3"><img src="img/logo-itam.png" class="slider-3"/></div>
                                                <div class="swiper-slide swiper-container-3"><img src="img/logouam.gif" class="slider-3"/></div>
                                            </div>
                                            <!-- Add Pagination -->
                                            <div class="swiper-pagination bottom-gray-slider"></div>
                                            <!-- Add Arrows -->
                                            <div class="swiper-button-next bottom-gray-slider"></div>
                                            <div class="swiper-button-prev bottom-gray-slider"></div>
                                        </div>
                                        <button type="button" class="btn btn-default btn-buscar-color2-new general-hover">VER PROYECTO</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Contacto Footer -->
        <div class="container-fluid" id="contacto">
            <?php include('formulario.php');?>
        </div>

        <!-- Footer-->
        <footer>
            <?php include('footer.php');?>
        </footer>

        <!-- Librerias JS -->
        <script src="js/main.js"></script>
        <script src="js/main_2.js"></script>
        <script src="js/slider.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#hide-btn").click(function(){
                    $("#hide-btn").text('MOSTRAR FILTROS');
                });
            });
        </script>
    </body>
</html>
