<!DOCTYPE html>
<html>
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>

   <body>
      <header class="container-fluid header6" id="conteiner-fluid-0">
         <div class="container">
              <!-- Menu -->
              <?php include('menu.php');?>
         </div>
         <div class="row row-new-2">
            <div class="col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
               <div class="col-sm-12">
                  <h1 class="text-center">BLOG</h1>
                  <div class="text-center-new1">¡Mantente al día sobre todos nuestros nuevos proyectos!</div>
               </div>
            </div>
         </div>
      </header>

      <!-- Conteiner-->
      <div class="container-fluid">

         <div class="row">
            <div class="col-xs-12 col-sm-12">
               <div class="col-xs-12 col-sm-12 padding-tarje-2">

                  <div class="col-xs-12 col-sm-12" style="padding-bottom: 50px;padding-left: 0px;">
                     <div class="col-xs-12 col-sm-8">
                        <h2 class="title-menu-2 text-gray">¡Mantente al día!</h2>
                        <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
                     </div>
                     <div class="col-xs-0 col-sm-2"></div>
                     <div class="col-xs-12 col-sm-2" style="padding-right: 0px;">
                        <div class="custom-select" style="width:95%;top: 15px;">
                           <select>
                              <option value="0">Todos los articulos</option>
                              <option value="1">Opción 1</option>
                              <option value="2">Opción 2</option>
                              <option value="3">Opción 3</option>
                              <option value="4">Opción 4</option>
                              <option value="5">Opción 5</option>
                           </select>
                        </div>
                     </div>
                  </div>

                  <div class="col-xs-12 col-sm-4">
                     <figure class="snip1527">
                        <div class="image"><img src="img/psicologia.jpg" alt="pr-sample25" class="img-big-blog" /></div>
                        <figcaption>
                           <div class="date"><span class="day">01</span><span class="month">Dec</span></div>
                           <h3>Down with this sort of thing</h3>
                           <p>I don't need to compromise my principles, because they don't have the slightest bearing on what happens to me anyway.</p>
                        </figcaption>
                        <a href="blog_interno.php"></a>
                     </figure>
                  </div>

                  <div class="col-xs-12 col-sm-4">
                     <figure class="snip1527">
                        <div class="image"><img src="img/tierra.jpg" alt="pr-sample25" class="img-big-blog" /></div>
                        <figcaption>
                           <div class="date"><span class="day">01</span><span class="month">Dec</span></div>
                           <h3>Down with this sort of thing</h3>
                           <p>I don't need to compromise my principles, because they don't have the slightest bearing on what happens to me anyway.</p>
                        </figcaption>
                        <a href="blog_interno.php"></a>
                     </figure>
                  </div>

                  <div class="col-xs-12 col-sm-4">
                     <figure class="snip1527">
                        <div class="image"><img src="img/tierra.jpg" alt="pr-sample25" class="img-big-blog" /></div>
                        <figcaption>
                           <div class="date"><span class="day">01</span><span class="month">Dec</span></div>
                           <h3>Down with this sort of thing</h3>
                           <p>I don't need to compromise my principles, because they don't have the slightest bearing on what happens to me anyway.</p>
                        </figcaption>
                        <a href="blog_interno.php"></a>
                     </figure>
                  </div>

               </div>
            </div>
         </div>

         <div class="row">
            <div class="col-xs-12 col-sm-12">
               <div class="col-xs-12 col-sm-12">

                  <div class="col-xs-12 col-sm-3 padding-tarje-2">
                     <figure class="snip1527">
                        <div class="image"><img src="img/tecnologia.jpg" alt="pr-sample23" class="margin-right"></div>
                        <figcaption>
                           <div class="date"><span class="day">28</span><span class="month">Oct</span></div>
                           <h3>The World Ended Yesterday</h3>
                           <p>You know what we need, Hobbes? We need an attitude. Yeah, you can't be cool if you don't have an attitude.</p>
                        </figcaption>
                        <a href="blog_interno.php"></a>
                     </figure>
                  </div>

                  <div class="col-xs-12 col-sm-3 padding-tarje-2">
                     <figure class="snip1527">
                        <div class="image"><img src="img/biologia.jpg" alt="pr-sample23" class="margin-right"></div>
                        <figcaption>
                           <div class="date"><span class="day">28</span><span class="month">Oct</span></div>
                           <h3>The World Ended Yesterday</h3>
                           <p>You know what we need, Hobbes? We need an attitude. Yeah, you can't be cool if you don't have an attitude.</p>
                        </figcaption>
                        <a href="blog_interno.php"></a>
                     </figure>
                  </div>

                  <div class="col-xs-12 col-sm-3 padding-tarje-2">
                     <figure class="snip1527">
                        <div class="image"><img src="img/slider-2.jpg" alt="pr-sample25" class="margin-right"></div>
                        <figcaption>
                           <div class="date"><span class="day">01</span><span class="month">Dec</span></div>
                           <h3>Down with this sort of thing</h3>
                           <p>I don't need to compromise my principles, because they don't have the slightest bearing on what happens to me anyway.</p>
                        </figcaption>
                        <a href="blog_interno.php"></a>
                     </figure>
                  </div>

                  <div class="col-xs-12 col-sm-3 padding-tarje-2">
                     <figure class="snip1527">
                        <div class="image"><img src="img/slider-2.jpg" alt="pr-sample25" class="margin-right"></div>
                        <figcaption>
                           <div class="date"><span class="day">01</span><span class="month">Dec</span></div>
                           <h3>Down with this sort of thing</h3>
                           <p>I don't need to compromise my principles, because they don't have the slightest bearing on what happens to me anyway.</p>
                        </figcaption>
                        <a href="blog_interno.php"></a>
                     </figure>
                  </div>

               </div>
            </div>
         </div>

         <div class="row contenedor-paginador-blog">
               <ul class="pager text-uppercase">
                   <li><a href="#">Anterior</a></li>
                   <li><a href="#">Siguiente</a></li>
               </ul>
               <nav aria-label="Page navigation" id="div1">
                   <ul class="pager">
                     <li><a href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
                     <li><a href="#">1</a></li>
                     <li><a href="#">2</a></li>
                     <li><a href="#">3</a></li>
                     <li><a href="#">4</a></li>
                     <li><a href="#">5</a></li>
                     <li><a href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                   </ul>
               </nav>
         </div>

      </div>

      <!-- Footer-->
      <footer>
        <?php include('footer.php');?>
      </footer>

      <!-- Librerias JS -->
      <script src="js/main.js"></script>
      <script src="js/main_2.js"></script>
      <script src="js/slider.js"></script>
   </body>
</html>
