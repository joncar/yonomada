<!DOCTYPE html>
<html>
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>

   <body>
      <header class="container-fluid header6" id="conteiner-fluid-0">
          <div class="container">
              <!-- Menu -->
              <?php include('menu.php');?>
          </div>
         <div class="row row-new-2">
            <div class="col-sm-8 col-sm-offset-2" id="top-row-segundo-principal">
               <div class="col-sm-12">
                  <h1 class="text-center">BLOG</h1>
                  <div class="text-center-new1">¡Mantente al día sobre todos nuestros nuevos proyectos!</div>
               </div>
            </div>
         </div>
      </header>
      <!-- Conteiner-->
      <div class="container-fluid ">
         <div class="row row-mapa ">
              <div class="col-xs-12 col-sm-10 col-sm-offset-1 padding-responsive-blo-in padding0">
                <h2 class="title-menu-2 text-gray">¡Mantente al día!</h2>
                  <div class="col-sm-3  col-xs-4 line-colo-cyan"></div>
              </div>
              <div class="col-xs-12 col-sm-10 col-sm-offset-1 padding-responsive-blo-in">
              <div class="col-xs-12 col-sm-4 top-blo-2 ">
                 <div class="cfotoblogdetail" style="background-image:url(img/slider-1.jpg);">
                 </div>
              </div>
              <div class="col-xs-12 col-sm-8 top-blo-2 ">
                  <h2 class="titulo-blog-interno">Título de la nota</h2>
                  <p class="text-gray-general text-justify">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam iaculis maximus ligula. Nullam facilisis massa eget est porta facilisis. In auctor lorem sit amet lorem scelerisque laoreet. Proin quis dictum dolor, ac feugiat arcu. Donec quis odio accumsan, congue massa eget, bibendum velit. Pellentesque rhoncus ex ligula, quis commodo ex suscipit eget. Sed eleifend elit velit, consectetur scelerisque est semper eu. Ut pulvinar facilisis blandit. Nullam posuere ligula at libero tempor, ac interdum diam placerat. Praesent vel nunc sed lacus placerat pulvinar id ac tellus. Integer ultricies accumsan mauris eget faucibus.
                    Etiam sit amet mi turpis. Mauris vel ornare turpis. Cras et urna condimentum, mattis nisi nec, tempus augue. Nulla libero eros, dapibus in sem at, iaculis rhoncus enim. Nulla lectus lacus, blandit eu tincidunt nec, vehicula eu purus. Duis molestie id mi eget efficitur. Curabitur at lorem vulputate, vestibulum orci at, fermentum enim. Cras pellentesque pharetra magna at euismod. Morbi bibendum, tellus ut pretium rutrum, arcu augue volutpat felis, non suscipit metus ante non arcu. Duis ac felis fermentum, tempor elit sed, pretium nibh. Nullam vitae massa erat. Nulla massa risus, rhoncus at quam et, eleifend eleifend nibh. Pellentesque quis sapien pulvinar, imperdiet lectus eu, laoreet leo. Nullam at consectetur tortor.
                  </p>
              </div>
              </div>
         </div>
      </div>

      <div class="container-fluid">
        <div class="row row-blog-inter-2">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <div class="autorrec">
              <div class="col-xs-12 col-sm-2">
              <img src="img/arquitectura.jpg" alt="Administrac&iacute;on" class="img-responsive center-block img-circle img-cir-blog-autor">
              </div>
              <div class="col-xs-12 col-sm-10 padding-top-autor-new">
                <p class="tex-blog-1">Nombre del autor</p>
                <p class="tex-blog-2">Naturaleza + Servicio Social</p>
                <p class="text-gray-general"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam iaculis maximus ligula. Nullam facilisis massa eget est porta facilisis.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>


      <!-- Contacto Footer -->
      <div class="container-fluid" id="contacto">
        <?php include('formulario.php');?>
      </div>

      <!-- Footer-->
      <footer>
        <?php include('footer.php');?>
      </footer>

      <!-- Librerias JS -->
      <script src="js/main.js"></script>
      <script src="js/main_2.js"></script>
      <script src="js/slider.js"></script>
   </body>
</html>
